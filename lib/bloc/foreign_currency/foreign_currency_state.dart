part of 'foreign_currency_bloc.dart';

class ForeignCurrencyState extends Equatable {
  final String isCurrency;

  const ForeignCurrencyState({required this.isCurrency});

  factory ForeignCurrencyState.initial(){
    return const ForeignCurrencyState(isCurrency: "profile");
  }

  @override
  // TODO: implement props
  List<Object?> get props => [isCurrency];

  @override
  String toString() {
    return 'ForeignCurrencyState{isCurrency: $isCurrency}';
  }

  ForeignCurrencyState copyWith({
    String? isCurrency,
  }) {
    return ForeignCurrencyState(isCurrency: isCurrency ?? this.isCurrency);
  }
}
