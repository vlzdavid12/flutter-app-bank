import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:xisfo_app/models/models.dart';

part 'investor_event.dart';
part 'investor_state.dart';

class InvestorBloc extends Bloc<InvestorEvent, InvestorState> {
  InvestorBloc() : super(InvestorState.initial()) {
    on<AddInvestorEvent>(_addInvestor);
    on<RemoveInvestorEvent>(_deleteInvestor);
  }
  void _addInvestor(AddInvestorEvent event, Emitter<InvestorState> emit){
    final newInvestor = ListInvestor(investor: event.investor, dateExpedition: event.dateExpedition, stake: event.stake, numberIdentification: event.numberIdentification, peps: event.peps);
    final newInvestors = [...state.investors, newInvestor];
    emit(state.copyWith(investors: newInvestors));
  }

  void _deleteInvestor(RemoveInvestorEvent event, Emitter<InvestorState> emit){
    final newInvestor =  state.investors.where((element) => element.id != event.investor.id).toList();
    emit(state.copyWith(investors: newInvestor));
  }
}

