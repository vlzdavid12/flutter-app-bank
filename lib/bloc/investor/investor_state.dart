part of 'investor_bloc.dart';

class InvestorState extends Equatable {
  final List<ListInvestor> investors;

  const InvestorState({required this.investors});

  factory InvestorState.initial() => const InvestorState(investors: []);

  @override
  List<Object> get props => [investors];

  @override
  String toString() => 'InvestorState(investors: $investors)';

  InvestorState copyWith({
    List<ListInvestor>? investors,
  }) {
    return InvestorState(investors: investors ?? this.investors);
  }
}
