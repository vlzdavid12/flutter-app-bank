part of 'password_bloc.dart';

abstract class PasswordEvent extends Equatable {
  const PasswordEvent();

  @override
  List<Object> get props => [];
}

class SetPasswordChangeEvent extends PasswordEvent {
  final bool newVisiblePass;

  const SetPasswordChangeEvent({required this.newVisiblePass});

  @override
  String toString() {
    return 'PasswordChangeEvent{visiblePass: $newVisiblePass}';
  }

  @override
  List<Object> get props => [newVisiblePass];
}
