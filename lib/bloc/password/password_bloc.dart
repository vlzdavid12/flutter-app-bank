import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'password_event.dart';
part 'password_state.dart';

class PasswordBloc extends Bloc<PasswordEvent, PasswordState> {
  PasswordBloc() : super(PasswordState.initial()) {
    on<SetPasswordChangeEvent>((event, emit) {
      emit(state.copyWith(visiblePass: event.newVisiblePass));
    });
  }
}
