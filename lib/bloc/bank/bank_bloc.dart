import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../models/list_bank.model.dart';

part 'bank_event.dart';
part 'bank_state.dart';

class BankBloc extends Bloc<BankEvent, BankState> {
  BankBloc() : super(BankState.initial()) {
    on<AddBankEvent>(_addBank);
    on<RemoveBankEvent>(_deleteBank);
  }
  void _addBank(AddBankEvent event,  Emitter<BankState> emit){
    final newBank =  ListBank(nameBank: event.nameBank, typeAccount: event.typeAccount, numberAccountBank: event.numberAccount);
    final  newBanks = [...state.banks, newBank];
    emit(state.copyWith(banks: newBanks));
  }

  void _deleteBank(RemoveBankEvent event, Emitter<BankState> emit){
    final newBanks =  state.banks.where((element) => element.id != event.bank.id).toList();
    emit(state.copyWith(banks: newBanks));
  }
}
