part of 'bank_bloc.dart';

class BankState extends Equatable {
  final List<ListBank> banks;

  BankState({required this.banks});

  factory BankState.initial() {
    return BankState(banks: []);
  }

  @override
  List<Object?> get props => [banks];

  @override
  String toString() {
    return 'BankState{banks: $banks}';
  }

  BankState copyWith({List<ListBank>? banks}) {
    return BankState(banks: banks ?? this.banks);
  }
}
