part of 'bank_bloc.dart';

abstract class BankEvent extends Equatable {
  const BankEvent();
}

class AddBankEvent extends BankEvent {
  final String nameBank;
  final String typeAccount;
  final String numberAccount;
  AddBankEvent({required this.nameBank, required this.typeAccount, required this.numberAccount});

  @override
  List<Object?> get props => [nameBank, typeAccount, numberAccount];

  @override
  String toString() {
    return 'AddBankEvent{nameBank: $nameBank, typeAccount: $typeAccount, numberAccount: $numberAccount}';
  }
}

class RemoveBankEvent extends BankEvent {
  final ListBank bank;
  RemoveBankEvent({required this.bank});

  @override
  List<Object> get props => [bank];

  @override
  String toString() {
    return 'DeleteBankEvent{bank: $bank}';
  }
}



