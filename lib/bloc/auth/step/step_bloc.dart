import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'step_event.dart';
part 'step_state.dart';

class StepBloc extends Bloc<StepEvent, StepState> {
  StepBloc() : super(StepState.initial()) {
    on<SetChangeStepEvent>((event, emit) {
      emit(state.copyWith(step: event.newStep));
    });
  }
}
