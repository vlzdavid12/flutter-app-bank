part of 'step_bloc.dart';

class StepState extends Equatable {
  final bool step;
  const StepState({required this.step});

  factory StepState.initial(){
    return const StepState(step: true);
  }

  @override
  List<Object> get props => [step];

  @override
  String toString() {
    return 'StepState{step: $step}';
  }

  StepState copyWith({
    bool? step
  }){
    return StepState(step: step ?? this.step);
  }
}