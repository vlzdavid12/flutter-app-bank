// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(total) => "Participation totale: ${total}";

  static String m1(name) => "${name}, Les bons comptes font les bons amis";

  static String m2(peso) => "Les documents doivent faire maximum ${peso}";

  static String m3(name) =>
      "¡Ne vous inquiétez pas, ${name} ! Effectuez vos paiements groupés ici";

  static String m4(value) =>
      "*Valeur dispersion (\\\$${value} + TVA entre portefeuilles Xisfo)";

  static String m5(phone) => "Tu as demandé à ${phone}";

  static String m6(Cafer) => "${Cafer}, voici votre argent";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Pin": MessageLookupByLibrary.simpleMessage("Pin"),
        "account": MessageLookupByLibrary.simpleMessage("Compte"),
        "accountBank":
            MessageLookupByLibrary.simpleMessage("Comptes Bancaires"),
        "activityEconomic":
            MessageLookupByLibrary.simpleMessage("Activité Economique"),
        "addArchive": MessageLookupByLibrary.simpleMessage("Joindre Archive"),
        "addText": MessageLookupByLibrary.simpleMessage("Attachez"),
        "alertInvestorConfirm": MessageLookupByLibrary.simpleMessage(
            "Dans la liste des champs de participation, le 100% doit être atteint.."),
        "appInvestorTotal": m0,
        "ask": MessageLookupByLibrary.simpleMessage("Demande"),
        "asks": MessageLookupByLibrary.simpleMessage("Commander"),
        "asksFriends": MessageLookupByLibrary.simpleMessage(
            "!Demande de l\'argent à ce pote qui te doit!"),
        "asksTitle": m1,
        "authData":
            MessageLookupByLibrary.simpleMessage("Authenticatión de données"),
        "available": MessageLookupByLibrary.simpleMessage("Disponible"),
        "back": MessageLookupByLibrary.simpleMessage("Retour"),
        "changeEmail": MessageLookupByLibrary.simpleMessage("Changez l\'Email"),
        "changePin":
            MessageLookupByLibrary.simpleMessage("Changez le code PIN"),
        "changeTextPin": MessageLookupByLibrary.simpleMessage(
            "Rappellez-vous que le PIN est secret, ne le communiquez à d\'autres personnes!"),
        "confirmTextPin":
            MessageLookupByLibrary.simpleMessage("Confirmez le code PIN"),
        "conventions": MessageLookupByLibrary.simpleMessage("Conventions"),
        "createNewAccount":
            MessageLookupByLibrary.simpleMessage("Créez votre compte"),
        "createPin": MessageLookupByLibrary.simpleMessage("Créez votre PIN"),
        "declarationOrigenFound": MessageLookupByLibrary.simpleMessage(
            "En cliquant ici, je déclare l\'origine de mes fonds."),
        "deleteTextAlert": MessageLookupByLibrary.simpleMessage(
            "Souhaitez-vous supprimer cette notification?"),
        "deleteTextAlert2": MessageLookupByLibrary.simpleMessage(
            "si vous l\'effacez, vous ne pourrez plus le recupérer."),
        "descriptionThinkClick": MessageLookupByLibrary.simpleMessage(
            "Pour vous faciliter la tâche, nous avons développé les paiements de masse. Désormais, il suffit d\'un clic pour répartir les salaires, payer les fournisseurs et envoyer de l\'argent à plusieurs personnes en même temps !"),
        "desireText": MessageLookupByLibrary.simpleMessage(
            "Je veux m\'enregistrer en tant que:"),
        "detailsAccount":
            MessageLookupByLibrary.simpleMessage("Details du compte"),
        "detailsEconomicActivity": MessageLookupByLibrary.simpleMessage(
            "Introduisez les détails de votre acitivité économique."),
        "downLoadTemplate": MessageLookupByLibrary.simpleMessage(
            "Téléchargez le modèle d\'envoi"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "endMoneyInternational": MessageLookupByLibrary.simpleMessage(
            "Finalisez Monnaie Etrangère."),
        "endRegister":
            MessageLookupByLibrary.simpleMessage("Finalisez l\'Enregistrement"),
        "endStatementFound": MessageLookupByLibrary.simpleMessage(
            "Finalisez la déclaration de fonds."),
        "enterCallNumber":
            MessageLookupByLibrary.simpleMessage("Entrez le numero du mobile"),
        "enterPinPassword": MessageLookupByLibrary.simpleMessage(
            "Entrez le PIN du mot de passe"),
        "filter": MessageLookupByLibrary.simpleMessage("Filtrer"),
        "fintech": MessageLookupByLibrary.simpleMessage("fintech"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Mot de passe oublié"),
        "forgotPin": MessageLookupByLibrary.simpleMessage("PIN Oublié?"),
        "getInto": MessageLookupByLibrary.simpleMessage("Entrez"),
        "hello": MessageLookupByLibrary.simpleMessage("Salut !"),
        "help": MessageLookupByLibrary.simpleMessage("Aide"),
        "historyMovements":
            MessageLookupByLibrary.simpleMessage("Historique de Mouvements"),
        "home": MessageLookupByLibrary.simpleMessage("Maison"),
        "ifWish": MessageLookupByLibrary.simpleMessage("Oui, je le veux!"),
        "infoGeneral":
            MessageLookupByLibrary.simpleMessage("Information Générale"),
        "investor": MessageLookupByLibrary.simpleMessage("Inversionistes"),
        "investorSubtitle": MessageLookupByLibrary.simpleMessage(
            "Listez les actionnaires détenant une participation supérieure à 5 %.."),
        "leyPoliData": MessageLookupByLibrary.simpleMessage(
            "En cliquant ici et conformément à la loi 1581 de 2012, j\'accepte la politique de traitement des données personnelles."),
        "logout": MessageLookupByLibrary.simpleMessage("Fermer Session"),
        "massiveShipments":
            MessageLookupByLibrary.simpleMessage("Envoies massifs"),
        "maxWeightArchive": m2,
        "mobil": MessageLookupByLibrary.simpleMessage("Mobile"),
        "mobilNotValidate": MessageLookupByLibrary.simpleMessage(
            "Le numero du mobile n\'est pas valable"),
        "movements": MessageLookupByLibrary.simpleMessage("Mouvements"),
        "myAccount": MessageLookupByLibrary.simpleMessage("Mon Compte"),
        "myData": MessageLookupByLibrary.simpleMessage("Mes Données"),
        "nameArchiveNomina": MessageLookupByLibrary.simpleMessage(
            "Vous devez nommer le fichier Payroll_(Mark)"),
        "newFintechOur": MessageLookupByLibrary.simpleMessage(
            "Vous êtes nouveau dans notre fintech"),
        "newPayment": MessageLookupByLibrary.simpleMessage("Nouveau Paiement."),
        "newPaymentSubtitle":
            MessageLookupByLibrary.simpleMessage("Il y a un nouveau paiement"),
        "nextContinue": MessageLookupByLibrary.simpleMessage("Continuez"),
        "noIdentification":
            MessageLookupByLibrary.simpleMessage("No. IDENTIFICATION"),
        "notCompliedPayment": m3,
        "notRed":
            MessageLookupByLibrary.simpleMessage("Il n\'y a pas de reseau..."),
        "notWant": MessageLookupByLibrary.simpleMessage("Non, Je ne veux pas"),
        "noteDispersion": m4,
        "numberMobil": MessageLookupByLibrary.simpleMessage("Numero de Mobile"),
        "numberPhone":
            MessageLookupByLibrary.simpleMessage("Numéro de Téléphone"),
        "password": MessageLookupByLibrary.simpleMessage("Mot de passe"),
        "payment": MessageLookupByLibrary.simpleMessage("Paiement"),
        "paymentHave": m5,
        "paymentsMassive":
            MessageLookupByLibrary.simpleMessage("Paiements de masse"),
        "paymentsRecharges":
            MessageLookupByLibrary.simpleMessage("Paiements et Recharges"),
        "personLegal": MessageLookupByLibrary.simpleMessage("Entreprise"),
        "personNature": MessageLookupByLibrary.simpleMessage("Personne"),
        "persons": MessageLookupByLibrary.simpleMessage("Personne"),
        "processRejected":
            MessageLookupByLibrary.simpleMessage("Transaction refusée !"),
        "processRejectedText": MessageLookupByLibrary.simpleMessage(
            "Votre transaction a échoué, veuillez contacter notre administrateur Xisfo."),
        "profile": MessageLookupByLibrary.simpleMessage("Profil"),
        "pymes": MessageLookupByLibrary.simpleMessage("PMEs"),
        "recoverAccount":
            MessageLookupByLibrary.simpleMessage("Recuperez le compte"),
        "registerAs": MessageLookupByLibrary.simpleMessage(
            "enregistrez-vous en tant que"),
        "registerHere": MessageLookupByLibrary.simpleMessage("Enregistrez Ici"),
        "representLegal":
            MessageLookupByLibrary.simpleMessage("Représentant Légal"),
        "request": MessageLookupByLibrary.simpleMessage("Requête"),
        "requestCredit":
            MessageLookupByLibrary.simpleMessage("Demandez un credit"),
        "send": MessageLookupByLibrary.simpleMessage("Envoyez"),
        "sendFriends":
            MessageLookupByLibrary.simpleMessage("Envoie à un pote!"),
        "sendPaymentsMassive": MessageLookupByLibrary.simpleMessage(
            "ENVOYEZ DES PAIEMENTS EN MASSE"),
        "services": MessageLookupByLibrary.simpleMessage("Services"),
        "signContract":
            MessageLookupByLibrary.simpleMessage("signez le Contrat"),
        "solicitudExitosa":
            MessageLookupByLibrary.simpleMessage("Demande acceptée."),
        "startDetailAccount": MessageLookupByLibrary.simpleMessage(
            "Saisie des données du compte."),
        "textDanger": MessageLookupByLibrary.simpleMessage(
            "N\'a pas pu être enregistré."),
        "textDistrust": MessageLookupByLibrary.simpleMessage(
            "Nous croyons que c\'est vous, nous voulons juste nous assurer."),
        "textFavorite":
            MessageLookupByLibrary.simpleMessage("Destinataire Préféré:"),
        "textMoney": MessageLookupByLibrary.simpleMessage(
            "Desirez-vous activer les monnaies étrangères."),
        "textRecovery": MessageLookupByLibrary.simpleMessage(
            "Entrez l\'email d\'entreprise et nous vous enverrons les instructions pour restaurer le mot de passe"),
        "textRegister": MessageLookupByLibrary.simpleMessage(
            "Avoir votre enregistrement en tant que"),
        "textSegmentsPersons": MessageLookupByLibrary.simpleMessage(
            "ÊTES-VOUS UNE PME OU UNE PERSONNE ?"),
        "textSlideMore": MessageLookupByLibrary.simpleMessage(
            "Glissez pour en savoir plus ..."),
        "textSuccessFully":
            MessageLookupByLibrary.simpleMessage("Enregistrement réussi."),
        "typeDocument":
            MessageLookupByLibrary.simpleMessage("Type de document"),
        "update": MessageLookupByLibrary.simpleMessage("Actualisez"),
        "updateEmail":
            MessageLookupByLibrary.simpleMessage("Actualisez l\'Email"),
        "updatePassword": MessageLookupByLibrary.simpleMessage(
            "Actualisez votre mot de passe"),
        "uploadYourSupport":
            MessageLookupByLibrary.simpleMessage("téléchargez votre support"),
        "valueTotal": MessageLookupByLibrary.simpleMessage("Valeur Totale"),
        "wallet": MessageLookupByLibrary.simpleMessage("Portefeuille"),
        "weLoveAgain":
            MessageLookupByLibrary.simpleMessage("Au plaisir de vous revoir"),
        "welcome": MessageLookupByLibrary.simpleMessage("Bienvenue"),
        "welcomeDashboard": m6,
        "welcomeFirstText": MessageLookupByLibrary.simpleMessage("Bienvenue à"),
        "welcomeSecondText":
            MessageLookupByLibrary.simpleMessage("Notre temps"),
        "welcomeSubtitleText": MessageLookupByLibrary.simpleMessage(
            "Il y a de la place pour tous!"),
        "welcomeXisfo":
            MessageLookupByLibrary.simpleMessage("Bienvenue à Xisfo."),
        "withDraw": MessageLookupByLibrary.simpleMessage("Retirez"),
        "withdrawal": MessageLookupByLibrary.simpleMessage("Retrait"),
        "yesWant": MessageLookupByLibrary.simpleMessage("Oui, je le veux"),
        "youBusiness":
            MessageLookupByLibrary.simpleMessage("de votre compagnie")
      };
}
