// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static String m0(total) => "Total de participación: ${total}";

  static String m1(name) =>
      "${name}, Las Cuentas  Claras y el chocolate espeso";

  static String m7(name) => "¡Legaliza tu pago! Sube tus soportes, ${name}";

  static String m2(peso) => "Los documentos deben pesar máximo ${peso}";

  static String m3(name) =>
      "¡Despreocúpate, ${name}! Realiza tus pagos masivos aquí";

  static String m4(value) =>
      "*Valor dispersión (\$${value} + IVA entre billeteras Xisfo)";

  static String m5(phone) => "Le pediste a ${phone}";

  static String m8(name) => "${name}, registra tus cuentas bancarias";

  static String m9(name) =>
      "${name}, porque somos transparentes, conoce tus tarifas.";

  static String m6(Cafer) => "¡${Cafer}, aquí está tu plata!";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Pin": MessageLookupByLibrary.simpleMessage("Ingresa tu PIN actual"),
        "acceptTermCondition": MessageLookupByLibrary.simpleMessage(
            "Al hacer click aquí aceptas los términos y condiciones del contrato."),
        "account": MessageLookupByLibrary.simpleMessage("Cuenta"),
        "accountBank":
            MessageLookupByLibrary.simpleMessage("Cuentas Bancarias"),
        "activeForeignCurrency":
            MessageLookupByLibrary.simpleMessage("Activar Moneda Extranjera"),
        "activePartner":
            MessageLookupByLibrary.simpleMessage("Activar Business Partner"),
        "activityEconomic":
            MessageLookupByLibrary.simpleMessage("Actividad Ecónomica"),
        "addAccount": MessageLookupByLibrary.simpleMessage("Añadir Cuenta"),
        "addAccountBank":
            MessageLookupByLibrary.simpleMessage("Añadir Cuenta Bancaria"),
        "addArchive": MessageLookupByLibrary.simpleMessage("Adjuntar Archivo"),
        "addShareHolder":
            MessageLookupByLibrary.simpleMessage("Agregar Accionista"),
        "addText": MessageLookupByLibrary.simpleMessage("Adjuntar"),
        "alertInvestorConfirm": MessageLookupByLibrary.simpleMessage(
            "En el campo de participacipación deben tener el total 100% de paticipación a cumplir su listado de inversión."),
        "appInvestorTotal": m0,
        "ask": MessageLookupByLibrary.simpleMessage("Pide"),
        "asks": MessageLookupByLibrary.simpleMessage("Pide"),
        "asksFriends": MessageLookupByLibrary.simpleMessage(
            "!Pídele ese parcero que debe!"),
        "asksTitle": m1,
        "authData":
            MessageLookupByLibrary.simpleMessage("Autenticación de datos"),
        "available": MessageLookupByLibrary.simpleMessage("Disponible"),
        "back": MessageLookupByLibrary.simpleMessage("Atrás"),
        "bank": MessageLookupByLibrary.simpleMessage("Banco"),
        "btnChangePIN": MessageLookupByLibrary.simpleMessage("Cambiar PIN"),
        "changeEmail": MessageLookupByLibrary.simpleMessage(
            "Ingresa tu nuevo correo electrónico"),
        "changePin": MessageLookupByLibrary.simpleMessage("Cambiar PIN"),
        "changeTextPin": MessageLookupByLibrary.simpleMessage(
            "Recuerda que tu PIN es secreto, ¡no lo compartas con nadie!"),
        "changeUpdate": MessageLookupByLibrary.simpleMessage("Actualizar"),
        "confirmTextPin":
            MessageLookupByLibrary.simpleMessage("Ingresa tu nuevo PIN"),
        "confirmVerificationTransferBank": MessageLookupByLibrary.simpleMessage(
            "Al aceptar confirmas que todos tus datos son verídicos, recuerda que si tus datos no están bien, tus transferencias no llegarán."),
        "contractMoney":
            MessageLookupByLibrary.simpleMessage("Contrato Moneda Extranjera."),
        "conventions": MessageLookupByLibrary.simpleMessage("Convenios"),
        "createNewAccount":
            MessageLookupByLibrary.simpleMessage("Crea una cuenta"),
        "createPin": MessageLookupByLibrary.simpleMessage("Crea tu PIN"),
        "declarationOrigenFound": MessageLookupByLibrary.simpleMessage(
            "Al hacer click aquí Declaro el origen de mis fondos."),
        "deleteTextAlert": MessageLookupByLibrary.simpleMessage(
            "Deseas eliminar esta notificación?"),
        "deleteTextAlert2": MessageLookupByLibrary.simpleMessage(
            "Si lo eliminas no podrás recuperarlo."),
        "descriptionThinkClick": MessageLookupByLibrary.simpleMessage(
            "Pensando en tu comodidad desarrollamos pagos masivos. Ahora dispersar nómina, pagar a proveedores y enviar dinero a varias personas al tiempo, está al alcance de un solo Click!"),
        "desireText":
            MessageLookupByLibrary.simpleMessage("Deseo inscribirme como:"),
        "detailActivityEconomic": MessageLookupByLibrary.simpleMessage(
            "Ingresa los detalles de tu actividad económica"),
        "detailsAccount":
            MessageLookupByLibrary.simpleMessage("Detalles de la cuenta"),
        "detailsEconomicActivity": MessageLookupByLibrary.simpleMessage(
            "Ingresa los detalles de tu actividad económica."),
        "downLoadTemplate": MessageLookupByLibrary.simpleMessage(
            "Descarga la plantilla de envíos"),
        "downloadFormat":
            MessageLookupByLibrary.simpleMessage("Descargar Formato"),
        "email": MessageLookupByLibrary.simpleMessage("Correo electrónico"),
        "endMoneyInternational": MessageLookupByLibrary.simpleMessage(
            "Finalizar Moneda Extranjera."),
        "endRegister":
            MessageLookupByLibrary.simpleMessage("Finaliza tu registro"),
        "endStatementFound": MessageLookupByLibrary.simpleMessage(
            "Finalizar Declaración de Fondos."),
        "enterCallNumber":
            MessageLookupByLibrary.simpleMessage("Ingresa el numero celular"),
        "enterPinPassword": MessageLookupByLibrary.simpleMessage(
            "Ingresa el PIN o la Contraseña"),
        "filter": MessageLookupByLibrary.simpleMessage("Filtrar"),
        "fintech": MessageLookupByLibrary.simpleMessage("Fintech"),
        "foreignCurrency": MessageLookupByLibrary.simpleMessage("Extranjera"),
        "foreignCurrencyMoney":
            MessageLookupByLibrary.simpleMessage("Moneda extranjera"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Olvidaste tu PIN"),
        "forgotPin": MessageLookupByLibrary.simpleMessage("¿Olvidaste tu PIN?"),
        "getInto": MessageLookupByLibrary.simpleMessage("Ingresar"),
        "hello": MessageLookupByLibrary.simpleMessage("¡Hola!"),
        "help": MessageLookupByLibrary.simpleMessage("Ayuda"),
        "helpTitle": MessageLookupByLibrary.simpleMessage(
            "Queremos ayudarte Cafer, aclara tus dudas"),
        "historyMovements":
            MessageLookupByLibrary.simpleMessage("Historial Movimientos"),
        "historyPayments":
            MessageLookupByLibrary.simpleMessage("Historial de pagos"),
        "home": MessageLookupByLibrary.simpleMessage("Inicio"),
        "ifWish": MessageLookupByLibrary.simpleMessage("Si Deseo"),
        "infoGeneral":
            MessageLookupByLibrary.simpleMessage("Información básica"),
        "information": MessageLookupByLibrary.simpleMessage("Información"),
        "investor": MessageLookupByLibrary.simpleMessage("Accionistas"),
        "investorSubtitle": MessageLookupByLibrary.simpleMessage(
            "A continuación relacione los accionistas que poseen más del 5% de participación."),
        "legalPaymentSupport": m7,
        "leyPoliData": MessageLookupByLibrary.simpleMessage(
            "Al hacer click aquí y de acuerdo a la ley 1581 del 2012, acepto la Política de tratamiento de datos personales"),
        "logout": MessageLookupByLibrary.simpleMessage("Cerrar Seción"),
        "massiveShipments":
            MessageLookupByLibrary.simpleMessage("Envios masivos"),
        "maxWeightArchive": m2,
        "mobil": MessageLookupByLibrary.simpleMessage("Celular"),
        "mobilNotValidate":
            MessageLookupByLibrary.simpleMessage("El célular no es valido"),
        "mountDollar": MessageLookupByLibrary.simpleMessage("Monto USD"),
        "movements": MessageLookupByLibrary.simpleMessage("Movimientos"),
        "myAccount": MessageLookupByLibrary.simpleMessage("Tu cuenta"),
        "myData": MessageLookupByLibrary.simpleMessage("Tus datos"),
        "nameArchiveNomina": MessageLookupByLibrary.simpleMessage(
            "Debes nombrar el archivo Nómina_(Marca)"),
        "newFintechOur": MessageLookupByLibrary.simpleMessage(
            "¿Eres nuevo en nuestra Fintech?"),
        "newPayment": MessageLookupByLibrary.simpleMessage("Nuevo Pago."),
        "newPaymentSubtitle":
            MessageLookupByLibrary.simpleMessage("Hay un nuevo pago"),
        "nextContinue": MessageLookupByLibrary.simpleMessage("Continuar"),
        "nextContinuo": MessageLookupByLibrary.simpleMessage("Continuar"),
        "noIdentification":
            MessageLookupByLibrary.simpleMessage("No. Identificación"),
        "notCompliedPayment": m3,
        "notRed": MessageLookupByLibrary.simpleMessage("No tienes red..."),
        "notWant": MessageLookupByLibrary.simpleMessage("No, Sedeo"),
        "noteDispersion": m4,
        "nowVerificar": MessageLookupByLibrary.simpleMessage("Empezar ahora"),
        "numberAccountBank":
            MessageLookupByLibrary.simpleMessage("Número de cuenta"),
        "numberFacture": MessageLookupByLibrary.simpleMessage("Factura No."),
        "numberMobil":
            MessageLookupByLibrary.simpleMessage("Ingresa tu número celular"),
        "numberPhone": MessageLookupByLibrary.simpleMessage("Número Celular"),
        "partner": MessageLookupByLibrary.simpleMessage("Partner"),
        "password": MessageLookupByLibrary.simpleMessage("Ingresa tu PIN"),
        "payment": MessageLookupByLibrary.simpleMessage("Paga"),
        "paymentApply":
            MessageLookupByLibrary.simpleMessage("Solicitud de pago"),
        "paymentHave": m5,
        "paymentsMassive":
            MessageLookupByLibrary.simpleMessage("Pagos Masivos"),
        "paymentsRecharges":
            MessageLookupByLibrary.simpleMessage("Pagos y recargas"),
        "personLegal": MessageLookupByLibrary.simpleMessage("Persona jurídica"),
        "personNature": MessageLookupByLibrary.simpleMessage("Persona natural"),
        "persons": MessageLookupByLibrary.simpleMessage("Personas"),
        "processRejected":
            MessageLookupByLibrary.simpleMessage("¡Transacción Rechazada!"),
        "processRejectedText": MessageLookupByLibrary.simpleMessage(
            "Tu transacción falló, contacta con nuestro administrador Xisfo."),
        "profile": MessageLookupByLibrary.simpleMessage("Perfil"),
        "pymes": MessageLookupByLibrary.simpleMessage("Pymes"),
        "rates": MessageLookupByLibrary.simpleMessage("Tarifas"),
        "recordBankText": MessageLookupByLibrary.simpleMessage(
            "Recuerda que las cuentas bancarias asociadas a tu billetera deben ser de tu  propiedad, de lo contrario la transacción  será rechazada."),
        "recoverAccount":
            MessageLookupByLibrary.simpleMessage("Recuperar Cuenta"),
        "registerAccountBank": m8,
        "registerAs":
            MessageLookupByLibrary.simpleMessage("Haz tu registro como"),
        "registerHere": MessageLookupByLibrary.simpleMessage("Regístrate Aquí"),
        "representLegal":
            MessageLookupByLibrary.simpleMessage("Representante Legal"),
        "request": MessageLookupByLibrary.simpleMessage("Pedido"),
        "requestCredit":
            MessageLookupByLibrary.simpleMessage("Solicita tu crédito"),
        "search": MessageLookupByLibrary.simpleMessage("Buscar..."),
        "selectPlatform":
            MessageLookupByLibrary.simpleMessage("Selecciona la plataforma"),
        "send": MessageLookupByLibrary.simpleMessage("Enviar"),
        "sendFriends":
            MessageLookupByLibrary.simpleMessage("¡Envía a un parcero!"),
        "sendPaymentsMassive":
            MessageLookupByLibrary.simpleMessage("ENVIAR PAGOS MASIVOS"),
        "sendTwo": MessageLookupByLibrary.simpleMessage("Envía"),
        "services": MessageLookupByLibrary.simpleMessage("Servicios"),
        "signContract": MessageLookupByLibrary.simpleMessage("Firma Contrato"),
        "solicitudExitosa":
            MessageLookupByLibrary.simpleMessage("¡Transacción Exitosa!"),
        "startDetailAccount": MessageLookupByLibrary.simpleMessage(
            "Ingresa los detalles de tu cuenta"),
        "subHelpTitle": MessageLookupByLibrary.simpleMessage(
            "En esta categoría las preguntas más frecuentes son:"),
        "sureAccountAdd": MessageLookupByLibrary.simpleMessage(
            "¿Estás seguro de que esta cuenta que estás agregando te pertenece?  Recuerda que solo puedes retirar a cuentas propias."),
        "textAllyCommercial": MessageLookupByLibrary.simpleMessage(
            "Para ser nuestro aliado comercial"),
        "textBusinessRegister": MessageLookupByLibrary.simpleMessage(
            "Si tienes una empresa registrada con cámara de comercio es aquí donde te registras"),
        "textDanger":
            MessageLookupByLibrary.simpleMessage("No se pudo registrar."),
        "textDistrust": MessageLookupByLibrary.simpleMessage(
            "No desconfiamos de ti, solo queremos confirmar que eres tú"),
        "textFavorite":
            MessageLookupByLibrary.simpleMessage("Destinatario  Favoritos:"),
        "textMoney": MessageLookupByLibrary.simpleMessage(
            "Si vas a realizar un proceso de monetización con nosotros debes activar moneda extranjera aquí"),
        "textPersonNatureRegister": MessageLookupByLibrary.simpleMessage(
            "Si obras como persona natural es aquí donde te registras"),
        "textProcessMoney": MessageLookupByLibrary.simpleMessage(
            "Para procesos de monetización"),
        "textRecovery": MessageLookupByLibrary.simpleMessage(
            "Introduce tu correo electrónico y te enviaremos las instrucciones para restablecer tu PIN"),
        "textRegister":
            MessageLookupByLibrary.simpleMessage("Haz tu registro como"),
        "textSegmentsPersons": MessageLookupByLibrary.simpleMessage(
            "¿ERES UNA PYME O PERTENECES AL SEGMENTO PERSONAS?"),
        "textSlideMore":
            MessageLookupByLibrary.simpleMessage("Desliza para conocer más"),
        "textSuccessFully": MessageLookupByLibrary.simpleMessage(
            "Se ha registrado correctamente."),
        "transferKnow": m9,
        "typeAccountBank":
            MessageLookupByLibrary.simpleMessage("Tipo de cuenta"),
        "typeDocument": MessageLookupByLibrary.simpleMessage("Tipo documento"),
        "update": MessageLookupByLibrary.simpleMessage("Modificar"),
        "updateEmail":
            MessageLookupByLibrary.simpleMessage("Modifica tu correo"),
        "updatePassword": MessageLookupByLibrary.simpleMessage("Cambia tu PIN"),
        "uploadSupport": MessageLookupByLibrary.simpleMessage("Subir Soporte"),
        "uploadYourSupport":
            MessageLookupByLibrary.simpleMessage("Sube tu soporte"),
        "valueTotal": MessageLookupByLibrary.simpleMessage("Cuánto"),
        "verifiquemosID":
            MessageLookupByLibrary.simpleMessage("Verifica tu identidad"),
        "verifyDataLegal": MessageLookupByLibrary.simpleMessage(
            "Comprobemos que tus datos son legítimos"),
        "wallet": MessageLookupByLibrary.simpleMessage("Billetera"),
        "walletBonus": MessageLookupByLibrary.simpleMessage("Billetera Abono"),
        "weLoveAgain":
            MessageLookupByLibrary.simpleMessage("Nos encanta verte de nuevo"),
        "welcome": MessageLookupByLibrary.simpleMessage("¡Bienvenido!"),
        "welcomeDashboard": m6,
        "welcomeFirstText":
            MessageLookupByLibrary.simpleMessage("Bienvenido a"),
        "welcomeSecondText":
            MessageLookupByLibrary.simpleMessage("nuestra era"),
        "welcomeSubtitleText":
            MessageLookupByLibrary.simpleMessage("¡Aquí cabemos todos!"),
        "welcomeXisfo":
            MessageLookupByLibrary.simpleMessage("Bienvenido a Xisfo"),
        "withDraw": MessageLookupByLibrary.simpleMessage("Retira"),
        "withDrawMount": MessageLookupByLibrary.simpleMessage("Retirar Monto"),
        "withdrawal": MessageLookupByLibrary.simpleMessage("Retiro"),
        "xisfoPersons": MessageLookupByLibrary.simpleMessage("Xisfo Personas"),
        "xisfoPymes": MessageLookupByLibrary.simpleMessage("Xisfo Pymes"),
        "yesWant": MessageLookupByLibrary.simpleMessage("Sì, Deseo"),
        "youBusiness": MessageLookupByLibrary.simpleMessage("de tu empresa"),
        "youCanAlso": MessageLookupByLibrary.simpleMessage("También puedes:"),
        "yourWallet": MessageLookupByLibrary.simpleMessage("Tú billetera")
      };
}
