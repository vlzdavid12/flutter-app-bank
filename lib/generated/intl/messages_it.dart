// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a it locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'it';

  static String m0(total) => "Partecipazione totale: ${total}";

  static String m1(name) => "${name}, Le cose in chiaro";

  static String m2(peso) => "I documenti non devono pesare più di ${peso}";

  static String m3(name) =>
      "Non preoccuparti, ${name}! Effettua i tuoi pagamenti in blocco qui";

  static String m4(value) =>
      "*Valore di dispersione (\\\$${value} + IVA tra i portafogli Xisfo)";

  static String m5(phone) => "Hai chiesto ${phone}";

  static String m6(Cafer) => "${Cafer}, ecco i tuoi soldi";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Pin": MessageLookupByLibrary.simpleMessage("Pin"),
        "account": MessageLookupByLibrary.simpleMessage("Conto"),
        "accountBank": MessageLookupByLibrary.simpleMessage("Conti Bancari"),
        "activityEconomic":
            MessageLookupByLibrary.simpleMessage("Attività economica"),
        "addArchive": MessageLookupByLibrary.simpleMessage("Allegare il file"),
        "addText": MessageLookupByLibrary.simpleMessage("Allegare"),
        "alertInvestorConfirm": MessageLookupByLibrary.simpleMessage(
            "Nell\'elenco dei campi di partecipazione, è necessario raggiungere il 100%."),
        "appInvestorTotal": m0,
        "ask": MessageLookupByLibrary.simpleMessage("Domanda"),
        "asks": MessageLookupByLibrary.simpleMessage("Chiedere"),
        "asksFriends": MessageLookupByLibrary.simpleMessage(
            "Chiedete a quell\'amico che è in debito con voi!"),
        "asksTitle": m1,
        "authData":
            MessageLookupByLibrary.simpleMessage("Autenticazione dei dati"),
        "available": MessageLookupByLibrary.simpleMessage("Disponibile"),
        "back": MessageLookupByLibrary.simpleMessage("Indietro"),
        "changeEmail": MessageLookupByLibrary.simpleMessage("Cambia email"),
        "changePin":
            MessageLookupByLibrary.simpleMessage("Modificare il codice PIN"),
        "changeTextPin": MessageLookupByLibrary.simpleMessage(
            "Ricorda che il PIN è segreto, non condividerlo con altre persone!"),
        "confirmTextPin":
            MessageLookupByLibrary.simpleMessage("Confermare il codice PIN"),
        "conventions": MessageLookupByLibrary.simpleMessage("Accordi"),
        "createNewAccount":
            MessageLookupByLibrary.simpleMessage("Crea il tuo account"),
        "createPin": MessageLookupByLibrary.simpleMessage("Crea il tuo PIN"),
        "declarationOrigenFound": MessageLookupByLibrary.simpleMessage(
            "Facendo clic qui, dichiaro l\'origine dei miei fondi."),
        "deleteTextAlert": MessageLookupByLibrary.simpleMessage(
            "Vuoi eliminare questa notifica?"),
        "deleteTextAlert2": MessageLookupByLibrary.simpleMessage(
            "Se lo inizializzi, non sarai in grado di recuperarlo."),
        "descriptionThinkClick": MessageLookupByLibrary.simpleMessage(
            "Per la vostra comodità, abbiamo sviluppato i pagamenti di massa. Ora è sufficiente un clic per distribuire le buste paga, pagare i fornitori e inviare denaro a più persone contemporaneamente!"),
        "desireText":
            MessageLookupByLibrary.simpleMessage("Voglio registrarmi come:"),
        "detailsAccount":
            MessageLookupByLibrary.simpleMessage("Dettagli del conto"),
        "detailsEconomicActivity": MessageLookupByLibrary.simpleMessage(
            "Inserite i dettagli della vostra attività economica."),
        "downLoadTemplate": MessageLookupByLibrary.simpleMessage(
            "Scarica il modello di spedizione"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "endMoneyInternational":
            MessageLookupByLibrary.simpleMessage("Terminare la valuta estera."),
        "endRegister":
            MessageLookupByLibrary.simpleMessage("Fine della registrazione"),
        "endStatementFound": MessageLookupByLibrary.simpleMessage(
            "Completare la dichiarazione dei fondi."),
        "enterCallNumber": MessageLookupByLibrary.simpleMessage(
            "Inserisci il numero di cellulare"),
        "enterPinPassword": MessageLookupByLibrary.simpleMessage(
            "Inserisci il PIN della password"),
        "filter": MessageLookupByLibrary.simpleMessage("Filtro"),
        "fintech": MessageLookupByLibrary.simpleMessage("fintech"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Password Dimenticata"),
        "forgotPin": MessageLookupByLibrary.simpleMessage("PIN Dimenticato?"),
        "getInto": MessageLookupByLibrary.simpleMessage("Avanti"),
        "hello": MessageLookupByLibrary.simpleMessage("Ciao !"),
        "help": MessageLookupByLibrary.simpleMessage("Aiuto"),
        "historyMovements":
            MessageLookupByLibrary.simpleMessage("Storia Movimenti"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "ifWish": MessageLookupByLibrary.simpleMessage("Sì, lo voglio!"),
        "infoGeneral":
            MessageLookupByLibrary.simpleMessage("Informazioni generali"),
        "investor": MessageLookupByLibrary.simpleMessage("Investitori"),
        "investorSubtitle": MessageLookupByLibrary.simpleMessage(
            "Di seguito sono elencati gli azionisti che detengono una partecipazione superiore al 5%."),
        "leyPoliData": MessageLookupByLibrary.simpleMessage(
            "Facendo clic qui e in conformità con la legge 1581 del 2012, accetto l\'Informativa sul trattamento dei dati personali."),
        "logout": MessageLookupByLibrary.simpleMessage("Disconnessione"),
        "massiveShipments":
            MessageLookupByLibrary.simpleMessage("Invii di massa"),
        "maxWeightArchive": m2,
        "mobil": MessageLookupByLibrary.simpleMessage("Cellulare"),
        "mobilNotValidate": MessageLookupByLibrary.simpleMessage(
            "Il numero du cellulare non è valido"),
        "movements": MessageLookupByLibrary.simpleMessage("Mouvimenti"),
        "myAccount": MessageLookupByLibrary.simpleMessage("Il mio conto"),
        "myData": MessageLookupByLibrary.simpleMessage("I miei dati"),
        "nameArchiveNomina": MessageLookupByLibrary.simpleMessage(
            "È necessario denominare il file Payroll_(Marca)"),
        "newFintechOur": MessageLookupByLibrary.simpleMessage(
            "Sei nuovo nel nostro fintech"),
        "newPayment": MessageLookupByLibrary.simpleMessage("Nuovo Pagamento."),
        "newPaymentSubtitle":
            MessageLookupByLibrary.simpleMessage("C\'è un nuovo pagamento"),
        "nextContinue": MessageLookupByLibrary.simpleMessage("Continua"),
        "noIdentification":
            MessageLookupByLibrary.simpleMessage("No. DI IDENTIFICAZIONE"),
        "notCompliedPayment": m3,
        "notRed": MessageLookupByLibrary.simpleMessage("Non avete rete..."),
        "notWant": MessageLookupByLibrary.simpleMessage("No, non voglio"),
        "noteDispersion": m4,
        "numberMobil":
            MessageLookupByLibrary.simpleMessage("Numero di Cellulare"),
        "numberPhone":
            MessageLookupByLibrary.simpleMessage("Numero di Telefono"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "payment": MessageLookupByLibrary.simpleMessage("Pagamento"),
        "paymentHave": m5,
        "paymentsMassive":
            MessageLookupByLibrary.simpleMessage("Pagamenti di massa"),
        "paymentsRecharges":
            MessageLookupByLibrary.simpleMessage("Pagamenti e Ricariche"),
        "personLegal": MessageLookupByLibrary.simpleMessage("Azienda"),
        "personNature": MessageLookupByLibrary.simpleMessage("Persona"),
        "persons": MessageLookupByLibrary.simpleMessage("Persona"),
        "processRejected":
            MessageLookupByLibrary.simpleMessage("Transazione rifiutata!"),
        "processRejectedText": MessageLookupByLibrary.simpleMessage(
            "La transazione non è riuscita, si prega di contattare il nostro amministratore Xisfo.."),
        "profile": MessageLookupByLibrary.simpleMessage("Profilo"),
        "pymes": MessageLookupByLibrary.simpleMessage("PMI"),
        "recoverAccount":
            MessageLookupByLibrary.simpleMessage("Recupera il conto"),
        "registerAs": MessageLookupByLibrary.simpleMessage("Registrati come"),
        "registerHere": MessageLookupByLibrary.simpleMessage("Registrati qui"),
        "representLegal":
            MessageLookupByLibrary.simpleMessage("Rappresentante legale"),
        "request": MessageLookupByLibrary.simpleMessage("Richiesta"),
        "requestCredit":
            MessageLookupByLibrary.simpleMessage("Chiedi un credito"),
        "send": MessageLookupByLibrary.simpleMessage("Invia"),
        "sendFriends":
            MessageLookupByLibrary.simpleMessage("Invia ad un amico"),
        "sendPaymentsMassive":
            MessageLookupByLibrary.simpleMessage("INVIARE PAGAMENTI IN BLOCCO"),
        "services": MessageLookupByLibrary.simpleMessage("Servizi"),
        "signContract":
            MessageLookupByLibrary.simpleMessage("Firma del Contratto"),
        "solicitudExitosa":
            MessageLookupByLibrary.simpleMessage("Applicazione riuscita."),
        "startDetailAccount": MessageLookupByLibrary.simpleMessage(
            "Inserimento di dati del conto."),
        "textDanger": MessageLookupByLibrary.simpleMessage(
            "Non è stato possibile registrarsi."),
        "textDistrust": MessageLookupByLibrary.simpleMessage(
            "Crediamo che sia tu, vogliamo solo esserne sicuri."),
        "textFavorite":
            MessageLookupByLibrary.simpleMessage("Destinatario Preferito:"),
        "textMoney": MessageLookupByLibrary.simpleMessage(
            "Vuoi attivare le valute estere."),
        "textRecovery": MessageLookupByLibrary.simpleMessage(
            "Inserisci l\'email d\'azienda e ti invieremo le istruzioni per ripristinare la password"),
        "textRegister": MessageLookupByLibrary.simpleMessage(
            "Avere la tua registrazione come"),
        "textSegmentsPersons":
            MessageLookupByLibrary.simpleMessage("Registrati come ?"),
        "textSlideMore": MessageLookupByLibrary.simpleMessage(
            "Scorri per saperne di più ..."),
        "textSuccessFully":
            MessageLookupByLibrary.simpleMessage("Registrazione riuscita."),
        "typeDocument":
            MessageLookupByLibrary.simpleMessage("Tipo de documento"),
        "update": MessageLookupByLibrary.simpleMessage("Aggiorna"),
        "updateEmail":
            MessageLookupByLibrary.simpleMessage("Aggiorna la tua Email"),
        "updatePassword":
            MessageLookupByLibrary.simpleMessage("Aggiorna la tua password"),
        "uploadYourSupport":
            MessageLookupByLibrary.simpleMessage("Carica il tuo supporto"),
        "valueTotal": MessageLookupByLibrary.simpleMessage("Valore Totale"),
        "wallet": MessageLookupByLibrary.simpleMessage("Portafoglio"),
        "weLoveAgain": MessageLookupByLibrary.simpleMessage(
            "Non vedo l\'ora di rivederti"),
        "welcome": MessageLookupByLibrary.simpleMessage("Benvenuto"),
        "welcomeDashboard": m6,
        "welcomeFirstText":
            MessageLookupByLibrary.simpleMessage("Benvenuto in"),
        "welcomeSecondText":
            MessageLookupByLibrary.simpleMessage("Il Nostro tempo"),
        "welcomeSubtitleText":
            MessageLookupByLibrary.simpleMessage("C\'è posto per tutti!"),
        "welcomeXisfo":
            MessageLookupByLibrary.simpleMessage("Benvenuto a Xisfo."),
        "withDraw": MessageLookupByLibrary.simpleMessage("Ritirare"),
        "withdrawal": MessageLookupByLibrary.simpleMessage("Ritiro"),
        "yesWant": MessageLookupByLibrary.simpleMessage("Sì, lo voglio"),
        "youBusiness":
            MessageLookupByLibrary.simpleMessage("della vostra azienda")
      };
}
