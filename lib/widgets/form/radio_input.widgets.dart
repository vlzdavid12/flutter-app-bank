import 'package:flutter/material.dart';

class RadioInput extends StatefulWidget {
  List<String> listRadio;

  RadioInput({required this.listRadio});

  @override
  State<RadioInput> createState() => _RadioInputState();
}

class _RadioInputState extends State<RadioInput> {
  String _select = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _select = widget.listRadio.first;
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 225,
          childAspectRatio: 8 / 2,
          crossAxisSpacing: 2,
          mainAxisSpacing: 2),
      itemCount: widget.listRadio.length,
      itemBuilder: (context, index) {
        return Container(
          alignment: Alignment.topCenter,
          decoration: const BoxDecoration(color: Colors.transparent),
          child: SizedBox(
            width: double.infinity,
            child: Row(
              children: [
                Radio<String>(
                  hoverColor: MaterialStateColor.resolveWith(
                      (states) => const Color(0xff6C4F92)),
                  focusColor: MaterialStateColor.resolveWith(
                      (states) => const Color(0xff6C4F92)),
                  activeColor: MaterialStateColor.resolveWith(
                      (states) => const Color(0xff6C4F92)),
                  fillColor: MaterialStateColor.resolveWith((states) {
                    if (states.contains(MaterialState.selected)) {
                      return const Color(0xff6C4F92);
                    }
                    return Colors.black45;
                  }),
                  value: widget.listRadio[index],
                  groupValue: _select,
                  onChanged: (String? value) {
                    setState(() {
                      _select = value!;
                    });
                  },
                ),
                Flexible(
                    child: Text(widget.listRadio[index],
                        style: const TextStyle(
                            color: Colors.black45,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.clip))
              ],
            ),
          ),
        );
      },
    );
  }
}
