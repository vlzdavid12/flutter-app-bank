import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:badges/badges.dart' as badges;
import 'package:xisfo_app/bloc/notify/notify_bloc.dart';
import 'package:xisfo_app/helpers/helpers.dart';
import 'package:xisfo_app/screen/offline.screen.dart';
import 'layout_drawer.widgets.dart';
import 'menu.widgets.dart';

class LayoutApp extends StatefulWidget {
  const LayoutApp({
    Key? key,
    required this.children,
  }) : super(key: key);

  final Widget children;

  @override
  State<LayoutApp> createState() => _LayoutAppState();
}

class _LayoutAppState extends State<LayoutApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    changeStatusLight();
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final listNotification = context.watch<NotifyBloc>().state.notify;

    return OfflineBuilder(
        connectivityBuilder: (
            BuildContext context,
            ConnectivityResult connectivity,
            Widget child,
            ) {
          final bool connected = connectivity != ConnectivityResult.none;
          return connected ? Scaffold(
            backgroundColor: Colors.white,
            endDrawer: LayoutDrawer(),
            appBar: AppBar(
              leading: const BackButton(
                color: Color(0xFF363853),
              ),
              centerTitle: true,
              toolbarHeight: 80,
              backgroundColor: Colors.white,
              elevation: 0,
              title: SizedBox(
                  height: 55,
                  child: Image.asset('assets/images/logo-purple.png')),
              actions: <Widget>[
                Container(
                  width: 40,
                  padding: const EdgeInsets.only(top: 15),
                  child: Swing(
                    duration: const Duration(milliseconds: 800),
                    child: badges.Badge(
                      position: badges.BadgePosition.topEnd(top: 3, end: 2),
                      showBadge: true,
                      badgeStyle: const badges.BadgeStyle(
                        badgeColor:  Color(0xff6C4F92),
                      ),
                      badgeContent: Text(listNotification.length.toString(),
                          style: const TextStyle(color: Colors.white, fontSize: 12)
                      ),
                      child: IconButton(icon: Image.asset('assets/images/bar/notify.png'), onPressed: () {
                        Navigator.pushNamed(context, 'notify');
                      }),
                    ),
                  ),
                ),
                Builder(
                  builder: (context) => IconButton(
                    icon:  Image.asset('assets/images/bar/config.png'),
                    onPressed: () => Scaffold.of(context).openEndDrawer(),
                    tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                  ),
                ),
              ],
            ),
            body: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    alignment: Alignment.center,
                    image: AssetImage("assets/images/fondo_app.png"),
                    fit: BoxFit.contain),
              ),
              child: SafeArea(
                child: Stack(
                  alignment: AlignmentDirectional.topCenter,
                  children: [
                    SizedBox(
                      width: 450,
                      child: widget.children,
                    ),
                    Positioned(
                        bottom: 0,
                        width: size.width / 1.0,
                        child: Menu()
                    )
                  ],
                ),
              ) /* add child content here */,
            ),
          ) : OfflineScreen();
        },
        child: Container()
    );

  }
}

