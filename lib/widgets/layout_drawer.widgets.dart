import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/bloc/auth/step/step_bloc.dart';
import 'package:xisfo_app/bloc/foreign_currency/foreign_currency_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';

class LayoutDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final isForeignCurrency = context.watch<ForeignCurrencyBloc>().state.isCurrency;
    final internalization = S.of(context);
    return Drawer(
      width: double.infinity,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const SizedBox(
              height: 140.0,
              child: UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage('assets/images/menu/header_menu.jpg'), fit: BoxFit.fill),
                    color: Color(0xff6C4F92)
                ),
                accountName: Text(
                  "Pinkesh Darji",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                accountEmail: Text(
                  "pinkesh.earth@gmail.com",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),

              )),
          ListTile(
            minVerticalPadding: 30,
            textColor: Colors.black45,
            iconColor: Colors.grey,
            leading:  Image.asset('assets/images/menu/sidebar/user_profile.png', width: 40, color: Colors.black45,),
            shape: const Border(
              bottom: BorderSide(
                width: 0.3,
                color: Colors.grey,
              ),
            ),
            title: Text(internalization.profile),
            onTap: () {
              Navigator.pushNamed(context, 'profile');
            },
          ),
          ListTile(
            minVerticalPadding: 30,
            textColor: Colors.black45,
            iconColor: Colors.grey,
            leading:  Image.asset('assets/images/menu/sidebar/card_bank.png', width: 40, color: Colors.black45,),
            shape: const Border(
              bottom: BorderSide(
                width: 0.3,
                color: Colors.grey,
              ),
            ),
            title: Text(internalization.accountBank),
            onTap: () {
              Navigator.pushNamed(context, 'bank');
            },
          ),

          ListTile(
            minVerticalPadding: 30,
            textColor: Colors.black45,
            iconColor: Colors.grey,
            leading: Image.asset('assets/images/menu/sidebar/help.png', width: 40, color: Colors.black45,),
            shape: const Border(
              bottom: BorderSide(
                width: 0.3,
                color: Colors.grey,
              ),
            ),
            title: Text(internalization.help),
            onTap: () {
              Navigator.pushNamed(context, 'help');
            },
          ),
          ListTile(
            minVerticalPadding: 30,
            textColor: Colors.black45,
            iconColor: Colors.grey,
            shape: const Border(
              bottom: BorderSide(
                  width: 0.3,
                  color: Colors.grey
              ),
            ),
            leading:   Image.asset('assets/images/menu/sidebar/power_logout.png', width: 40, color: Colors.black45,),
            title: Text(internalization.logout),
            onTap: () {
              context.read<StepBloc>().add(const SetChangeStepEvent(newStep: true));
              Navigator.pushNamed(context, 'login');
            },
          ),

        ],
      ),
    );
  }
}
