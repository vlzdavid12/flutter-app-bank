import 'package:flutter/material.dart';

@override
Future<void> dialogModalBuilder(BuildContext context, Widget child) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
          insetPadding: const EdgeInsets.all(10),
          backgroundColor: Colors.white,
          alignment: AlignmentDirectional.center,
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)),
          child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        constraints: const BoxConstraints(maxHeight: 580, maxWidth: 320),
        child: SingleChildScrollView(child: child),
      ));
    },
  );
}
