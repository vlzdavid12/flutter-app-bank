
import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PopupNote extends StatefulWidget {
  String textTitle;

  PopupNote({required this.textTitle});

  @override
  State<PopupNote> createState() => _PopupNoteState();
}

class _PopupNoteState extends State<PopupNote> {
  bool closeModal = true;
  @override
  Widget build(BuildContext context) {
    return FadeInDown(
      duration: const Duration(milliseconds: 1000),
      child: closeModal ? SizedBox(
        width: 320,
        child: Column(
              children: [
                SpinPerfect(
                  child: Container(
                      decoration: BoxDecoration(
                          color: const Color(0xff6C4F92),
                          borderRadius: BorderRadius.circular(50)
                      ),
                      child:  IconButton(onPressed: (){
                        setState(() {
                          closeModal = false;
                        });
                      }, icon: const Icon(Icons.close), color: Colors.white, )),
                ),
                const SizedBox(height: 8),
                Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Text(widget.textTitle, textAlign: TextAlign.center, style: const TextStyle(color: Colors.black54, fontSize: 14),)),
              ],
            ),
      ): SizedBox(),
    );
  }
}