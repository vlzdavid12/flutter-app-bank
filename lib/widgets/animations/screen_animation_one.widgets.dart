import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../generated/l10n.dart';

class ScreenAnimationOne extends StatefulWidget {
  @override
  State<ScreenAnimationOne> createState() => _ScreenAnimationOneState();
}

class _ScreenAnimationOneState extends State<ScreenAnimationOne>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FadeIn(
            delay: const Duration(milliseconds: 500),
            child: Lottie.asset('assets/images/logoxisfo.json',
                width: 260, repeat: false),
          ),
          Text(internalization.welcomeFirstText,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 50,
                  fontFamily: 'Dongle',
                  height: 0.3)),
          Text(internalization.welcomeSecondText,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 50,
                  fontFamily: 'Dongle',
                  height: 0.8)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(internalization.fintech,
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 50,
                      fontFamily: 'Dongle',
                      height: 0.6)),
              Swing(
                  duration: const Duration(milliseconds: 2000),
                  delay: const Duration(milliseconds: 1000),
                  child: const Text('👋',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 32,
                          fontFamily: 'Dongle',
                          height: 0.6))),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Text(internalization.welcomeSubtitleText,
              style: const TextStyle(color: Colors.white, fontSize: 18)),
        ],
      ),
    );
  }
}
