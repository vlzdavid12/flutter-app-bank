import 'package:flutter/material.dart';

class AnimationLogo extends StatelessWidget {
  const AnimationLogo({
    Key? key,
    required this.controller,
    required this.moveUp,
    required this.moveScale,
    required this.moveRight,
    required this.moveTitle,
    required this.moveSubtitle,
  }) : super(key: key);

  final AnimationController controller;
  final Animation<double> moveUp;
  final Animation<double> moveScale;
  final Animation<double> moveRight;
  final Animation<double> moveTitle;
  final Animation<double> moveSubtitle;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 180,
      child: Stack(
        children: [
          AnimatedBuilder(
              animation: controller,
              builder: (BuildContext context, Widget? child) {
                return Transform.translate(
                    offset: Offset(0.0, moveUp.value),
                    child: Transform.scale(
                        scale: moveScale.value,
                        child: Transform.translate(
                          offset: Offset(moveRight.value, 0.0),
                          child: const Image(
                              image: AssetImage('assets/images/icon-app.png')),
                        )));
              }),
          AnimatedBuilder(
              animation: controller,
              builder: (BuildContext context, Widget? child) {
                return Transform.translate(
                    offset: Offset(65.0, moveTitle.value),
                    child: const SizedBox(
                      width: 180,
                      height: 180,
                      child:
                      Image(image: AssetImage('assets/images/title_1.png')),
                    ));
              }),
          AnimatedBuilder(
              animation: controller,
              builder: (BuildContext context, Widget? child) {
                return Transform.translate(
                    offset: Offset(145.0, moveSubtitle.value),
                    child: const SizedBox(
                      width: 80,
                      height: 80,
                      child: Image(
                          image: AssetImage('assets/images/subtitle_2.png')),
                    ));
              }),
        ],
      ),
    );
  }
}
