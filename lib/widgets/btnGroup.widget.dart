
import 'package:flutter/material.dart';
import '../generated/l10n.dart';

class BtnGroupWidget extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  BtnGroupWidget({Key? key, required this.pageController, required this.pageChanged,  required this.internalization})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: pageChanged >= 1 && pageChanged <= 4
          ? MainAxisAlignment.spaceBetween
          : MainAxisAlignment.center,
      children: [
        pageChanged >= 1 && pageChanged <= 4
            ? MaterialButton(
            minWidth: 140,
            height: 50,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)),
            disabledColor: const Color(0xffa18bc7),
            focusColor: const Color(0xff4d2d79),
            splashColor: const Color(0xff53298a),
            highlightColor: const Color(0xff6C4F92),
            elevation: 0,
            color: const Color(0xff6C4F92),
            onPressed: () {
              pageController.animateToPage(--pageChanged,
                  duration: const Duration(milliseconds: 250),
                  curve: Curves.bounceInOut);
            },
            child:
                Text(internalization.back.toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        height: 1.4))
            )
            : Container(),
        MaterialButton(
            minWidth: pageChanged >= 1 && pageChanged <= 4 ? 140 : 280,
            height: 50,
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            disabledColor: const Color(0xffc7a314),
            focusColor: const Color(0xffc7a314),
            splashColor: const Color(0xffc7a314),
            highlightColor: const Color(0xffEDCF53),
            elevation: 0,
            color: const Color(0xffEDCF53),
            onPressed: () {
              pageController.animateToPage(++pageChanged,
                  duration: const Duration(milliseconds: 250),
                  curve: Curves.bounceInOut);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(internalization.nextContinuo.toUpperCase(),
                    style: const TextStyle(
                        color: Color(0xff6C4F92),
                        fontWeight: FontWeight.w700,
                        height: 1.4)),
              ],
            )),
      ],
    );
  }
}
