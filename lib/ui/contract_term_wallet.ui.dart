
class TermContract {
  static String  contractWallet(){
    return r"""
<div class="file-contract">
  <h3 class="my-2 text-center">TÉRMINOS DE CONDICIONES Y USOS DE BILLETERA DIGITAL XISFO</h3>

  <p class="my-4 text-justify">Los términos de condiciones y usos descritos a continuación constituyen
    un contrato entre cualquier persona que desee acceder, descargar y utilizar la página web o
    cualquiera de los servicios prestados por <strong>XISFO.</strong></p>

  <p class="my-4 text-justify">Al momento de descargar la billetera digital <strong>XISFO</strong> en cualquier
    dispositivo electrónico o desde la Web, declara que acepta y conoce el presente contrato.</p>

  <p class="my-4 text-justify">De no aceptar los términos y condiciones de <strong>XISFO</strong>, deberá abstenerse de
    utilizar el sitio o sus servicios, y de realizar cualquier transacción.</p>

  <h4 class="my-2 text-justify">¿QUÉ ES UNA BILLETERA DIGITAL?</h4>

  <p class="my-4 text-justify">Es una aplicación o dispositivo digital que funciona como depósito o e-Wallet, en
    donde el titular es una persona natural o, una persona jurídica.</p>

  <h4 class="my-2">REGISTRO EN LA BILLETERA DIGITAL.</h4>
  <ul>
    <li class="my-2 text-justify">Para registrarse deberá completar de manera obligatoria con los datos personales que
      aparecerán en el formulario. Estos deberán ser verdaderos, exactos y precisos.
    </li>
    <li class="my-2 text-justify">Los clientes o usuarios que decidan acceder, descargar y utilizar la billetera
      digital, deberá anexar una cuenta bancaria de una entidad vigilada por la institución encargada, misma en
      la que recibirá el pago por concepto de venta de productos y servicios, o dado el caso, para una devolución del dinero.
    </li>
    <li class="my-2 text-justify">El cliente asume el compromiso de actualizar sus datos personales, de la o las cuentas
      bancarias asociadas a la billetera digital de <strong>XISFO. </strong></li>
    <li class="my-2 text-justify">Los clientes de la billetera digital de <strong>XISFO</strong> serán los únicos
      responsables de la veracidad, exactitud, vigencia e integridad de los datos suministrados. <strong>XISFO</strong>
      no se hará responsable de la veracidad de los datos electrónicos de sus clientes, mismos que se tomará como una declaración jurada.
    </li>
    <li class="my-2 text-justify">XISFO tendrá la potestad de solicitar al cliente soportes de los datos personales
      suministrados, con el fin de corroborar la veracidad de información. Y también tendrá la facultad de suspender o
      eliminar aquellos que no hayan podido completar el registro de datos personales y su estado sea en stand-by.
    </li>
    <li class="my-2 text-justify">
      El cliente al momento de realizar el registro de la billetera digital de <strong>XISFO</strong>, certificará que
      los fondos que él posee no provienen de actividades ilícitas como lavado de activos o dinero, de actividades como financiamiento del
      terrorismo y demás relacionadas. Adicional a esto, <strong>XISFO</strong> podrá exigirle al cliente información
      determinada con el fin de corroborar la procedencia de los activos del cliente.
    </li>
    <li class="my-2 text-justify">
      El cliente no podrá registrarse en <strong>XISFO</strong> si él, o la persona que administre la billetera digital
      se encuentre sujeta a sanciones, prohibiciones, restricciones para participar en la celebración de negocios, en los diferentes
      países donde opere la billetera digital de <strong>XISFO</strong> dado la naturaleza de la aplicación.
    </li>
  </ul>
  <h4 class="my-2">CUENTA BILLETERA DIGITAL XISFO.</h4>
  <ul>
    <li class="my-2 text-justify">Cuando el cliente se registre en la billetera digital de <strong>XISFO</strong>,
      deberá abrir una cuenta en <strong>XISFO,</strong> donde el usuario podrá realizar pagos y de igual manera cobrar pagos a terceros por la compra de
      sus productos y servicios mediante el débito o la recepción del dinero en la cuenta.
    </li>
    <li class="my-2 text-justify">El cliente accederá a la billetera digital ingresando su número de teléfono, y la
      clave segura de cuatro dígitos elegida. Esta clave de cuatro dígitos será solicitada al momento en que el cliente realice
      transacciones o retiros de dinero. El cliente únicamente utilizará su correo electrónico personal registrado para
      navegar en la página web de <strong>XISFO</strong> y realizar los cambios que él considere en su cuenta a nivel
      interno.
    </li>
    <li class="my-2 text-justify">Es una obligación del cliente tener absoluta confidencialidad sobre sus claves
      seguras, <strong>XISFO</strong> no se hará responsable por la revelación de estas si el motivo fuere imputable al cliente.
    </li>
    <li class="my-2 text-justify">
      El cliente será el único y <strong>EXCLUSIVO</strong> responsable por todas las operaciones realizadas en la
      billetera digital
      <strong>XISFO</strong>, sin embargo, se compromete con <strong>XISFO</strong> a informar de manera inmediata
      cualquier novedad realizada que él no haya hecho o autorizado.
    </li>
    <li class="my-2 text-justify">
      La cuenta en la billetera digital de <strong>XISFO</strong> es personal, exclusiva e intransferible, se asignará a
      la persona que realice el registro correspondiente y cumpla con todos los requisitos. Se prohíbe su cesión, venta
      o transferencia sin importar la modalidad o título por medio del cual se realice.
    </li>
    <li class="my-2 text-justify">
      El cliente autoriza a <strong>XISFO</strong> para que consulte en todas las bases de datos posibles su información financiera,
      incluyendo centrales de riesgo y demás relacionadas. También, el cliente, autorizará a <strong>XISFO</strong> para que realice
      consultas de antecedentes (penales, fiscales y disciplinarios) en todas las listas restrictivas existentes.
    </li>
  </ul>
  <h4 class="my-2">OPERACIÓN DE PAGOS.</h4>

  <ul>
    <li class="my-2 text-justify">
      <strong>CONTRATO DE OPERACIÓN DE PAGOS:</strong> Entre el cliente y <strong>XISFO</strong> podrán celebrar cada
      que sea necesario para el primero,<strong>MANDATOS DE PAGO</strong>, mediante los cuales <strong>XISFO</strong>
      brinde servicios de gestión o procesamiento de pago de acuerdo a las instrucciones específicas del cliente, con
      el fin de pagar o recibir por cuenta y orden del cliente utilizando la billetera digital de <strong>XISFO</strong>
      o su plataforma Web <a href="https://xisfo.co">https://xisfo.co</a>.
    </li>
    <li class="my-2 text-justify">
      <p>
        <strong>MANDATO DEFINITIVO DE PAGO:</strong> Al completar un <strong>MANDATO DE PAGO</strong>, el cliente otorga
        un <strong>MANDATO DEFINITIVO DE PAGO </strong> a <strong>XISFO</strong> que incluye las siguientes obligaciones:

      <ul style="list-style-type: upper-roman">
        <li class="my-2">Pagar por su cuenta y orden una suma de dinero determinada donde se complete la solicitud de
          <strong>MANDATO
            DE PAGO.</strong>
        </li>
        <li class="my-2">Cobrar el dinero o los fondos por su cuenta y orden, sujeto a los términos de condiciones del
          servicio de <strong>OPERACIÓN DE PAGOS.</strong>
        </li>
        <li class="my-2">
          <strong>Vencidos los doce (12) meses mínimos</strong>, contados a partir del momento que hubieren acreditado
          fondos o dinero en su cuenta, el cliente no los hubiere retirado, éste ordena de manera irrevocable
          a <strong>XISFO</strong> para qué: a realizar una consignación de estos dineros en la cuenta bancaria que
          dicho usuario hubiera registrado o utilizado para retirar los fondos o dinero con anterioridad. Si <strong>XISFO </strong>no
          cuenta con la información bancaria sobre cuentas o tarjetas inscritas por el cliente antes de que se venza el
          plazo, el cliente autoriza a <strong>XISFO</strong> para hacer la devolución al medio de pago empleado,
          siempre y cuando el pago se hubiera realizado utilizando tarjeta crédito o débito. b. Si la devolución al
          medio de pago inscrito no fuera posible, el cliente autoriza a <strong>XISFO</strong> a donar, por cuenta, orden y riesgo del
          cliente, a cualquier organización sin ánimo de lucro que seleccione <strong>XISFO</strong> al momento de la donación, los
          fondos o dineros que tuvieran una antigüedad mayor al plazo.
        </li>
      </ul>
      <p class="my-2 text-justify">
        El mandato definitivo de pago otorgado por el cliente implica una autorización para disponer en su nombre de
        ciertos fondos o dineros de su billetera digital <strong>XISFO</strong> y transferir estos a cierto destinatario
        mediante acreditación en una cuenta <strong>XISFO</strong> designada de acuerdo a sus instrucciones. Mismos que
        se describieron en el párrafo anterior.
      </p>
      <p class="my-2 text-justify">
        Si el objetivo del cliente es ahorrar el dinero en la billetera digital <strong>XISFO</strong>, éste deberá
        enviar notificación a los canales digitales autorizados por <strong>XISFO</strong>, donde quedará constancia del
        deseo de ahorrar su dinero a largo plazo. De la misma manera, <strong>XISFO</strong> cobrará una comisión según
        el numeral 5.3. de los términos de condiciones y usos de billetera digital <strong>XISFO</strong>.
      </p>
      <p class="my-2 text-justify">
        El cliente podrá dejar los fondos o dineros que considere, pero no podrán ser inferiores a UN <strong>(1)
        DÓLAR</strong>, o al equivalente de éste. Al momento del retiro, el cliente deberá tener en cuenta que no podrá
        retirar menos de <strong>VEINTICINCO (25) DÓLARES</strong>, o el equivalente a éste. En caso que desee hacerlo
        tendrá un costo de transferencia.
      </p>
    </li>
    <li class="my-2 text-justify">
      <p class="my-2 text-justify"><strong>CELEBRACIÓN DE LA SOLICITUD DE PAGOS:</strong> El cliente comprador celebra
        esta solicitud de pago mediante el diligenciamiento de un formulario on-line de solicitud de pagos donde
        detallarán las instrucciones respecto a los fondos o dineros. Por su parte, el cliente vendedor celebra
        solicitud de pago cuando acepta recibir pagos a través de la aplicación o página web de <strong>XISFO.</strong></p>
      <p class="my-2 text-justify"><strong>XISFO</strong> se reserva el derecho de no procesar las solicitudes de pago
        que estén incompletas o en las cuales haya discrepancias entre los datos provistos por los clientes y los datos
        ingresados efectivamente en <strong>XISFO.</strong>
      </p>
    </li>
    <li class="my-2 text-justify">
      <strong>PERFECCIONAMIENTO DE LA SOLICITUD DE PAGO:</strong> La solicitud de pago no se considerará perfeccionada y
      <strong>XISFO</strong> no
      asumirá responsabilidad u obligación alguna bajo la solicitud de pago hasta qué: 1. <strong>XISFO</strong> no haya
      aceptado la solicitud de pago del cliente. 2. No haya recibido y se encuentren disponibles la totalidad de los
      fondos o dineros.
    </li>
    <li class="my-2 text-justify">
      <strong>RESPONSABILIDAD POR SOLICITUD:</strong> XISFO no será responsable por órdenes, instrucciones, solicitudes
      de pago y pagos equivocados, incompletos, que sean causados por el diligenciamiento erróneo del número telefónico,
      información importante del destinatario o de la operación de pago efectuados por el cliente.
    </li>
    <li class="my-2 text-justify">
      <p class="my-2 text-justify"><strong>RESPONSABILIDAD DE XISFO POR LAS INSTRUCCIONES EN EL PAGO:</strong> XISFO no
        será responsable ni garantizará el cumplimiento de las obligaciones que hayan asumido los clientes con terceros
        en relación a los pagos a efectuar o a cobrar a través de la billetera digital. El cliente reconoce y acepta
        realizar transacciones u operaciones con otros clientes o terceros por su propia voluntad, representando su
        consentimiento libre y bajo su responsabilidad.</p>
      <p class="my-2 text-justify">Si uno o más clientes, o terceros inician cualquier tipo de reclamo o acciones
        jurídicas y legales contra otro u otros clientes, todos los involucrados eximen de responsabilidad a <strong>XISFO</strong>, sus gerentes,
        representantes legales, operarios y demás sobre cualquier responsabilidad. Los clientes tendrán un plazo de
        sesenta <strong>(60) días </strong> desde la compra para iniciar un reclamo contra otro u otros clientes.
      </p>
      <p class="my-2 text-justify"> Una vez diligenciado el formulario de reversión de pagos dentro de los <strong>sesenta (60) días
          hábiles</strong>, y a su vez haya presentado la queja por medio del canal autorizado, <strong>XISFO</strong>
        deberá revisar y solucionar dicha solicitud.</p>

      <p class="my-2 text-justify">Si la solicitud de pago no se completó, por causa externa a <strong>XISFO</strong>,
        ésta se anulará. Se entiende por no completada, cuando el comprador no haya realizado el pago a la cuenta de la
        billetera digital de <strong>XISFO</strong> del vendedor y hubiese quedado en stand-by.</p>

      <p class="my-2 text-justify">Si la solicitud de pago si se completó, pero por causa externa a
        <strong>XISFO</strong> el vendedor no
        cumplió con su parte en la operación comercial, él comprador deberá iniciar la solicitud de reversión de pagos o
        adherirse al programa de protección al comprador.</p>

      <p class="my-2 text-justify">Si el cliente comprador se llegare a equivocar en el diligenciamiento del formulario
        on-line previa solicitud de pagos, este deberá igualmente realizar la solicitud de reversión de pagos por medio
        del formulario, y sustentar la situación en el espacio en blanco
        <strong>“Otro”</strong> en el acápite que dice <strong>“Motivo que da origen a la solicitud de reversión de
          pagos”.</strong> Si <strong>XISFO</strong> llegaré a
        comprobar que efectivamente el envío de fondos o dineros fue equivocado, y el cliente destinatario se opone a
        realizar la devolución del dinero, <strong>XISFO</strong> no podrá retener en la cuenta de
        este el monto total de los fondos o dineros que fueron enviados por equivocación, y deberá notificar al cliente
        vendedor sobre la situación para poder hacer efectiva la devolución.</p>

      <p class="my-2 text-justify">
        <strong>RESPONSABILIDAD DE XISFO POR DINEROS DONADOS POR CUENTA, ORDEN Y RIESGO DEL CLIENTE:</strong> De
        conformidad con el MANDATO DE PAGO otorgado por el cliente, <strong>XISFO</strong> está en la obligación de
        donar, por cuenta, orden y riesgo del cliente a cualquiera de las fundaciones o cualquier
        organización sin ánimo de lucro los fondos o dineros en cuenta que tengan una mayor antigüedad al plazo pactado,
        es decir, de <strong>doce (12) a veinticuatro (24) meses</strong>. <strong>XISFO</strong> no asume ninguna
        responsabilidad con el cliente, ni con ningún tercero por razón a la donación de los fondos o dineros.
        En casos en que los fondos o dineros de la cuenta del cliente superen el monto de <strong>cincuenta (50)
        salarios mínimos mensuales legales vigentes</strong>, XISFO podrá repartir las donaciones entre dos o más
        fundaciones u organización sin ánimo de lucro.
      </p>
    </li>
  </ul>
  <h4>ENTREGA Y RETIRO DE LOS FONDOS:</h4>
  <ul>
    <li class="my-2 text-justify">
      <p class="my-2 text-justify"><strong>Entrega de los fondos o dineros por el cliente:</strong>
        Aceptada la solicitud de pago por <strong>XISFO</strong>, el cliente deberá enviar a <strong>XISFO</strong> la
        cantidad de dinero necesaria para poder cumplir con la instrucción del pago. El cliente entregará los fondos o
        dineros a <strong>XISFO</strong> mediante la utilización
        de:</p>

      <ul style="list-style: decimal">
        <li class="my-3">Cualquiera de los medios disponibles y autorizados por <strong>XISFO</strong></li>
        <li class="my-3"> De los fondos o dineros disponibles en su billetera digital <strong>XISFO</strong> siempre
          que hubiera un monto suficiente para cumplir con las indicaciones dadas en la solicitud de pago, en caso
          contrario, no se procesará la solicitud.
        </li>
      </ul>

      <p class="my-2 text-justify">La acreditación de los fondos o dineros en la billetera digital de
        <strong>XISFO</strong> del cliente, se realizará dentro de los dos
        <strong>(2) días hábiles</strong> contados desde que <strong>XISFO</strong> reciba la autorización del medio
        de pago utilizado en la transacción.
      </p>

      <p class="my-2 text-justify">El límite de transferencia de fondos a cuenta propia que podrá realizar el cliente
        es de <strong>CINCUENTA MIL DÓLARES (USD 50.0000).</strong>Si el cliente excede este límite, deberá certificar
        que los fondos o dineros no provienen de actividades ilícitas, ilegales, fraudulentas, independiente del país
        donde haya descargado la <strong>Billetera Digital XISFO.</strong></p>

      <p class="my-2 text-justify"><strong>XISFO</strong> ingresará a la billetera digital del cliente el importe que
        haya sido acreditado efectivamente por el medio del pago utilizado, independientemente del monto declarado.
      </p>

    </li>
    <li class="my-2 text-justify">
      <p class="my-2"><strong>Disponibilidad de Fondos o Dineros:</strong> Los fondos o dineros que se acrediten en la
        billetera digital del cliente quedarán
        disponibles como máximo a partir del quinto (5to) día hábil siguiente a la fecha de acreditación del pago.
      </p>
      <p class="my-2"><strong>XISFO</strong> podrá liberar el dinero antes de este plazo. Los fondos o dineros podrán
        permanecer indisponibles cuando,
        según <strong>XISFO</strong>, existan fuertes sospechas de ilegalidades o fraudes, o cualquier acto contrario
        a los términos y condiciones generales, por razones de seguridad.</p>
    </li>
    <li class="my-2 text-justify">
      <p>
        <strong>Instrucciones de Fondos o Dineros:</strong> XISFO seguirá las instrucciones realizadas por el cliente, para:
      </p>
      <ul style="list-style-type: upper-roman">
        <li class="my-2">Realizar un pago o envío de los fondos o dineros en su cuenta o depositados a tal efecto</li>
        <li class="my-2"> Acreditar y/o recibir los fondos o dineros en su cuenta. Se aclara que el cliente al
          registrarse en <strong>XISFO</strong> y mantener su cuenta en estado activo, acuerda, acepta y autoriza recibir fondos o dineros que oportunamente
          envíen otros clientes a su cuenta y que se le debite cualquier cargo que resulte aplicable.
        </li>
        <li class="my-2">Pagar servicios que el cliente indique, siempre y cuando <strong>XISFO</strong> tenga convenio
          con la empresa recaudadora. (Tercero recaudo).
        </li>
      </ul>
    </li>
    <li>
      <p class="text-justify">
        <strong>Retiros:</strong> Una vez que los fondos o dineros se encuentren acreditados en la cuenta registrada
        del cliente, este podrá optar por: </p>
      <ul style="list-style-type: upper-roman">
        <li class="my-2">Retirar todo o parte del saldo de su cuenta o billetera digital a su banco, siempre y cuando se
          encuentre
          a nombre titular (Propio).
        </li>
        <li class="my-2">
          Dar indicaciones a <strong>XISFO</strong> para usar los fondos o dineros para realizar otros pagos. El
          cliente acepta y reconoce que el retiro de los fondos o dineros quedará supeditado al previo pago de cualquier
          deuda que el cliente tenga con <strong>XISFO,</strong> sin importar la causa o el motivo.
        </li>
      </ul>
      <p class="text-justify">
        El retiro de los fondos o dineros que realice el cliente, se harán mediante transferencia bancaria a una cuenta
        indicada oportunamente por el cliente. Esta transferencia podrá tardar hasta dos (2) días hábiles después de que
        se realice la solicitud de retiro. Por causas externas a <strong>XISFO</strong> y al cliente, el retiro podrá
        tardar hasta dos <strong> (2) días más. </strong>
      </p>
    </li>
    <li class="text-justify my-2">
      <strong>Límites:</strong> XISFO determinará un importe máximo para las solicitudes de pago, monto que podrá variar
      de acuerdo al método de pago elegido, al tipo de solicitud, a la cantidad de dinero y/o a criterio de <strong>XISFO,</strong> incluso podrá ser
      modificado en cualquier momento siendo suficiente su publicación en el aplicativo o página Web.
    </li>
    <li>
      <p class="text-justify my-2">
        <strong>El Usuario que Reciba Fondos en su Cuenta:</strong> En pago de productos o servicios, por otro cliente
        (Remitente), sólo deberá enviar dicho producto o servicio a quien lo haya o adquirido y pagado al cliente comprador
        (Destinatario), luego de verificar en su cuenta, a través de la plataforma <strong>XISFO</strong>, que los fondos estén
        debidamente acreditados.
      </p>
      <p class="text-justify my-2">
        Si el cliente remitente envía sin verificar en su cuenta los fondos o dineros pagados por el cliente
        destinatario, será <strong>ÚNICAMENTE</strong> responsabilidad del cliente remitente y asumirá el riesgo por la falta del pago
        del producto o servicio.
      </p>
      <p class="text-justify my-2">
        Si el cliente destinatario no recibe el producto o servicio por el que pagó al cliente remitente,
        podrá realizar una queja formal a través de la página de <strong>XISFO dentro de los quince (15) días
        siguientes</strong> a la ocurrencia del hecho.
      </p>
      <p class="text-justify my-2">
        Cuando <strong>XISFO</strong> realice las investigaciones necesarias por el incumplimiento del cliente remitente
        al cliente destinatario, podrá iniciar automáticamente un reclamo bajo el mecanismo de Resolución y Reclamos y los
        fondos o dineros de la billetera del cliente remitente serán retenidos por el mismo valor del producto o
        servicio (en caso de contar con los fondos), por <strong>XISFO</strong> hasta tanto no sea resuelta la queja.
      </p>
    </li>
    <li>
      <p class="text-justify my-2"><strong>Responsabilidad de los Fondos:</strong> XISFO mantendrá los fondos o dineros
        de las billeteras digitales en una cuenta bancaria principal
        a su nombre <strong>(XISFO)</strong>.</p>
      <p class="text-justify my-2"><strong>XISFO</strong> no será responsable en ningún caso por la insolvencia del
        banco, entidad financiera o agente financiero utilizado para la
        transferencia de los fondos o dineros, o cualquier cambio legal o regulatorio que afecte las cuentas o
        billeteras digitales en los cuales sean depositados por <strong>XISFO</strong>.</p>
      <p>Por lo tanto, no podrá atribuirse responsabilidad a <strong>XISFO</strong>, ni a sus gerentes, representantes
        legales, asesores, contratistas y demás.</p>
    </li>
    <li class="text-justify my-2">
      <strong>CANASTA DE COMPRAS Y PAGOS:</strong> Se denominan como herramientas de venta y compra implementados en la
      billetera digital XISFO. A través de la cual los clientes podrán realizar las compras que deseen y pagarlas de
      manera instantánea, bien sea por el aplicativo <strong>(Billetera Digital XISFO)</strong>, o por la página Web <a
      href="https://xisfo.co">https://xisfo.co.</a>
    </li>
    <li>
      Requisitos: Para la utilización de alguna de las herramientas de venta y compra, es necesario:
      <ul style="list-style-type: upper-roman;">
        <li class="my-2">
          Estar debidamente registrado en la <strong>Billetera Digital XISFO.</strong>
        </li>
        <li class="my-2">
          Los clientes que realicen compras serán llamados compradores, y los que realicen ventas, vendedores.
        </li>
      </ul>
    </li>
    <li>
      Prohibiciones: Se prohíbe utilizar las herramientas de venta y compra en:
      <ul style="list-style-type: upper-roman;">
        <li class="my-2 text-justify">Cualquier sitio Web que sea contrario a los términos y condiciones de uso.</li>
        <li class="my-2 text-justify">Promueva o tenga contenido sexual donde participen menores de edad. (Pedofilia,
          pornografía, desnudos).
        </li>
        <li class="my-2 text-justify">Que involucre o relacione menores de edad, si se tratare de películas o
          fotografías tomadas de forma ilegal, o sin el consentimiento de las personas que los
          representen o que en ellas aparezcan.
        </li>
        <li class="my-2 text-justify">Promueva violencia, discriminación racial o de cualquier tipo, la
          prostitución, actos delictivos, violencia, lavado de dinero, tráfico de armas y personas o animales, y
          cualquier actividad ilegal.
        </li>
        <li class="my-2 text-justify">Ofrezcan contenido que viole las disposiciones normativas y reglamentarias,
          incluyendo aquellas que violen los derechos de la propiedad intelectual, piratería, plagio, y todos sus
          derivados.
        </li>
      </ul>
      <p>En caso de cualquier actividad ilícita, ilegal o fraudulenta, incluyendo las descritas en el párrafo anterior,
        <strong>XISFO</strong> tendrá la potestad de dar por terminado el presente contrato, y cancelar o eliminar la
        billetera digital del cliente, sin previa notificación o aviso.
      </p>
    </li>
    <li class="my-2">
      <strong>Costos por Cobrar a Favor de XISFO:</strong>
      Los cargos a favor de <strong>XISFO</strong> por la utilización de la billetera digital, la página Web, y en general los servicios
      de <strong>XISFO</strong> serán cobrados de la siguiente manera:
      <table class="table-contract">
        <tr class="header">
          <th class="p-2 w-details">
            <strong>Detalle</strong></th>
          <th class="p-2">
            <strong>Comisión</strong></th>
          <th class="p-2">
            <strong>IVA</strong></th>
          <th class="p-2">
            <strong>Total</strong></th>
        </tr>
        <tr>
          <td class="p-2">
            <strong>Transferencias entre billeteras Xisfo</strong>
          </td>
          <td class="p-2">$ 0</td>
          <td class="p-2">$ 0</td>
          <td class="p-2">$ 0</td>
        </tr>
        <tr>
          <td class="p-2">
            <strong>Transferencias Masivas/Abono nomina
              *Por transferencia</strong>
          </td>
          <td class="p-2">$ 2.000</td>
          <td class="p-2">$ 380</td>
          <td class="p-2">$ 2.380</td>
        </tr>
        <tr>
          <td class="p-2">
            <strong>Solicitud de Retiro a cuenta</strong>
          </td>
          <td class="p-2">$ 3.500</td>
          <td class="p-2">$ 680</td>
          <td class="p-2">$ 4.165</td>
        </tr>
        <tr>
          <td class="p-2">
            <strong>Solicitud de Retiro a cuenta</strong></td>
          <td class="p-2">$ 5.000</td>
          <td class="p-2">$ 950</td>
          <td class="p-2">$ 5.950</td>
        </tr>
        <tr>
          <td class="p-2">
            <strong>Solicitud de Retiro a cuenta</strong></td>
          <td class="p-2">$ 3.000</td>
          <td class="p-2">$ 570</td>
          <td class="p-2">$ 3.570</td>
        </tr>
      </table>
    </li>
    <li>
      <p class="my-2 text-justify"><strong>Responsabilidad por la Utilización de las Herramientas de V y C:</strong> El
        vendedor será el único y exclusivo responsable por los productos o servicios publicados en su sitio Web o de los conceptos por los cuales recibirá
        sus pagos. </p>
      <p class="my-2 text-justify">
        El vendedor mantendrá indemne a <strong>XISFO</strong> frente a eventuales reclamaciones o quejas, y también, declaran que
        son partes indemnes, no existe de por medio ninguna relación societaria o comercial por fuera de las pactadas en
        el presente acuerdo.</p>
    </li>
  </ul>
  <h4 class="my-2">POLÍTICA DE MARCAS DE XISFO.</h4>
  <ul>
    <li>
      <p class="my-2 text-justify">
        <strong>Uso de Marcas, Logos y sus derivados:</strong> XISFO es titular de los derechos de propiedad intelectual
        contenidos en la billetera digital y en su página web. El vendedor utilizará los contenidos de <strong>XISFO</strong>
        acorde a las instrucciones pactadas en los términos y condiciones de uso.
      </p>
      <p class="my-2 text-justify">
        El vendedor no utilizará banners o logos diseñados por sí mismo o por terceros, como cualquier otro material
        similar al proporcional al suministrado por <strong>XISFO</strong>
      </p>
    </li>
    <li class="my-2 text-justify">
      <strong>Página del Vendedor:</strong> Logo, comunicaciones de promociones bancarias y medios de pagos habilitados.
      El vendedor se obliga a utilizar y comunicar en la página de inicio de su sitio
      web las marcas, logos y banners de promociones bancarias y medios de pagos aceptados por <strong>XISFO.</strong>
    </li>
    <li class="my-2 text-justify">
      <strong>Sección de Medios de Pagos del Sitio Web del Vendedor:</strong> El vendedor se compromete a comunicar sus
      clientes compradores
      que la plataforma de pago o billetera digital es provista por <strong>XISFO.</strong>
    </li>
    <li class="my-2 text-justify">
      <strong>Posicionamiento de XISFO:</strong> En caso de que el vendedor cuente con más de un proveedor de solución
      de pagos, deberá proporcionar el banner de <strong>XISFO</strong> entre la primera y la tercera opción.
    </li>
    <li class="my-2 text-justify">
      <strong>Integración de XISFO:</strong> El vendedor debe integrar <strong>XISFO</strong> en su sitio Web,
      cumpliendo con las políticas de integración
      efectiva de <strong>XISFO</strong>, que incluyen:
      <ul style="list-style-type: upper-roman;">
        <li class="my-2">E-mail del comprador al momento de conexión entre la web o billetera digital y
          <strong>XISFO.</strong></li>
        <li class="my-2">Implementación de la versión checkout más actualizada de <strong>XISFO.</strong></li>
      </ul>
    </li>
    <li class="my-2 text-justify">
      <strong>Uso de Marcas, Logos y Derechos de Propiedad Intelectual de los Vendedores:</strong> Los vendedores
      otorgan a <strong>XISFO</strong> una autorización de uso gratuito y sin límite temporal del nombre comercial, marcas, logos, símbolos, emblemas,
      colores, diseños del vendedor que fueran remitidos a <strong>XISFO,</strong> para acciones promocionales, publicitarias o
      de comunicaciones relacionadas con <strong>XISFO.</strong>
    </li>
  </ul>
  <h4 class="my-2">CONDICIONES GENERALES DE CONTRATACIÓN.</h4>
  <p><strong>Capacidad:</strong> Solo podrán celebrar solicitudes de pago aquellas personas qu</p>
  <ul>
    <li class="my-2 text-justify">Sean personas naturales o jurídicas, residentes y con capacidad de contratar.</li>
    <li class="my-2 text-justify">Ser mayor de edad, dependiendo de las regulaciones en los distintos países que
      funcione la billetera digital de <strong>XISFO.</strong>
    </li>
    <li class="my-2 text-justify">Personas o clientes que no se encuentren inhabilitados o suspendidos por <strong>XISFO,</strong>
      cualquiera que sea su estado
    </li>
    <li>
      <p class="my-2 text-justify"><strong>Calificaciones:</strong>XISFO y el cliente comprador darán una calificación
        de los productos o servicios ofrecidos por el vendedor, a su vez, este mismo también tendrá una calificación.
      </p>
      <p class="my-2 text-justify">Para obtener esta calificación no podrá presentarse ninguna novedad frente a los productos o
        servicios ofrecidos por el vendedor. Si la calificación fuera desfavorable, el vendedor tendrá una puntuación
        negativa en su consolidado. </p>
    </li>
    <li class="my-2 text-justify">
      <strong>Aclaración:</strong> Se aclara expresamente que <strong>XISFO</strong> no es una entidad financiera y no
      presta al cliente ningún servicio bancario, solo brinda un servicio de gestión y procesamiento de pagos por cuenta
      y a la orden de los clientes, así como los pagos de servicios públicos, recaudos y conexiones con empresas que
      brinden sus servicios o permitan realizar las transacciones de pago por medio de la billetera <strong>XISFO.</strong>
    </li>
    <li class="my-2 text-justify">
      <strong>Intereses:</strong>
      Los fondos o dineros acreditados en la billetera digital de <strong>XISFO</strong> no generan intereses y el cliente podrá disponer
      libremente de ellos para realizar las operaciones descritas en los presentes términos y condiciones de uso.
    </li>
    <li class="my-2 text-justify">
      <p class="my-2 text-justify"><strong>Pagos en Moneda Extranjera:</strong> Los pagos deberán realizarse en pesos colombianos. Al momento de la
        conversión de las divisas por <strong>XISFO</strong>, la conversión se realizará por parte de <strong>XISFO</strong> al tipo de
      cambio que se establezca para las divisas correspondientes.</p>
      <p class="my-2 text-justify">El tipo de cambio de la transacción se ajusta con la regularidad e incluye una comisión por conversión de divisas
      que se encuentra publicado en el portal de moneda extranjera en la billetera <strong>XISFO</strong>. El tipo de
        cambio básico se basa en los precios de negociación de la <strong>TRM</strong> gestionado por el banco regulado del mercado
        cambiario.</p>
    </li>
    <li class="my-2 text-justify">
      <p><strong>Tarifa por la Operación de Pagos:</strong> Por la utilización del servicio de solicitud de pagos, el
        cliente acepta a pagar a <strong>XISFO</strong> una tarifa por la operación de pagos, cada que se acrediten
        fondos o dineros en su cuenta y que se acredite un pago a su favor.</p>
      <p>Asimismo, el cliente autoriza a <strong>XISFO</strong> para que descuente y retenga los fondos o dineros
        disponibles en su cuenta sobre cualquier valor
        o monto adeudado a empresas con quien <strong>XISFO</strong> tenga algún acuerdo comercial.
        <strong>XISFO</strong> tendrá la obligación de informar a los clientes cuales
        son las empresas con las que tiene un convenio comercial vigente.</p>
    </li>
    <li class="my-2 text-justify">
      <p class="my-2 text-justify"><strong>Confidencialidad:</strong> XISFO no venderá, negociará, difundirá, revelará la información personal de
        los clientes, salvo en las formas y casos establecidos en las políticas de privacidad. Los datos suministrados
        serán utilizados para realizar las operaciones de pago.
      </p>
      <p class="my-2 text-justify">La información personal se almacena en servidores o medios magnéticos con altos estándares de seguridad.</p>
      <p class="my-2 text-justify"><strong>XISFO</strong> mantendrá en estricta confidencialidad toda la información personal de clientes, pero no responderá por
        los perjuicios que se puedan derivar de la violación de estas medidas por parte de terceros que utilicen las redes públicas o el
        internet para acceder a la billetera virtual o página web.
      </p>
    </li>
    <li class="my-2 text-justify">
      <strong>Limitación de Responsabilidad por Servicio de Billetera Digital y Plataforma:</strong> XISFO no garantiza el acceso continuo e ininterrumpido
      a su sitio web o billetera digital. Eventualmente no podrán estar disponibles debido a dificultades técnicas o fallas en internet, o, simplemente por
      mejoramiento y actualización de los servidores.
      Los clientes no podrán imputar responsabilidad a <strong>XISFO</strong>, ni exigir resarcimiento alguno, en virtud de los perjuicios resultantes de las dificultades mencionadas.
    </li>
    <li class="my-2 text-justify">
      <strong>Licencia:</strong> Todos los derechos intelectuales e industriales del sitio web o de la billetera digital de <strong>XISFO</strong> y demás
      que se relacionen, son de propiedad de <strong>XISFO.</strong> En ningún caso, salvo lo expreso por escrito, el cliente tendrá derechos conferidos sobre estas herramientas.
      <strong>XISFO</strong> solo autoriza al cliente para hacer uso de su propiedad intelectual sobre las herramientas puestas a disposición del vendedor o comprador.
    </li>
    <li class="my-2 text-justify">
      <strong>Modificaciones en los Términos y Condiciones:</strong> XISFO podrá modificar en cualquier momento los términos y condiciones de uso. Notificará a los clientes
      la versión actualizada, y entrará a regir contados cinco <strong>(5) días</strong> a partir de su divulgación y publicación. El cliente deberá decidir si acepta o no
      las nuevas disposiciones al momento de ingresar en la página web o billetera digital <strong>XISFO</strong> actualizada. Si vencido el plazo el cliente no se pronuncia
      sobre las modificaciones a los términos y condiciones, se entenderá que el cliente acepta los nuevos términos y condiciones.
    </li>
    <li class="my-2 text-justify">
      <strong>Terminación del Contrato XISFO y el Cliente:</strong> XISFO y el cliente podrán terminar en cualquier oportunidad el presente contrato, sin expresión de causal alguna,
      lo que implicará el cierre de la cuenta del cliente.<strong>XISFO</strong> tendrá el derecho de cerrar la cuenta del cliente cuando éste incumpla las disposiciones normativas
      aquí contenidas, o las establecidas por la Ley aplicable al país o lugar donde se encuentre utilizando los servicios de <strong>XISFO.</strong>
    </li>
    <li class="my-2 text-justify">
      <strong>Notificaciones:</strong> Las notificaciones a los clientes se realizarán por medio de los correos electrónicos registrados en la web de <strong>XISFO</strong> y dentro de la plataforma en la sección de notificaciones.
    </li>
    <li class="my-2 text-justify">
      <strong>Firma Digital:</strong> El documento se firmará y dará por aceptado por medio del onboarding digital una vez acepte los términos y condiciones.
    </li>
    <li class="my-2 text-justify">
      <strong>Domicilios:</strong> El domicilio principal sería en la <strong>Calle 93 a #13-24, Edificio QBO, Piso 5 oficina 26, Teléfono 601 66 72 651, en Bogotá, D.C.</strong>
    </li>
    <li>
      <p class="my-2 text-justify"><strong>Reversión de Pagos:</strong> En cumplimiento con el <strong>Decreto 587 de 2016, XISFO</strong> en calidad de participante de procesos de pago o de prestadores de servicio, crea el formulario para la reversión
      de pagos. (Aplica para Colombia).</p>
      <p class="my-2 text-justify"><strong>XISFO</strong> contará con quince <strong>(15) días hábiles</strong> para hacer efectiva la reversión del pago, si a ello hubiere lugar.</p>
      <p class="my-2 text-justify">Cuando <strong>XISFO</strong> realice la reversión, verificará por una sola vez por solicitud la existencia de fondos de la respectiva cuenta y procederá a efectuar el descuento.</p>
      <p class="my-2 text-justify">Si contra un mismo vendedor existieren ordenes de reversión pendientes, <strong>XISFO</strong> las hará efectivas de acuerdo con el orden de llegada de las notificaciones. </p>
      <p class="my-2 text-justify">Dado el caso de que los fondos o dineros en la cuenta del vendedor no sean suplan la cantidad total del monto de la reversión, esta se hará de manera gradual hasta completar el
        pago total, sin embargo, <strong>XISFO</strong> deberá informar al cliente comprador sobre esta situación.
      </p>
      <p class="my-2 text-justify">Frente al derecho de reservar los pagos correspondientes a obligaciones de cumplimiento periódico, el consumidor y/o comprador que hubiere autorizado pagos periódicos
        con cargos a sus tarjetas de crédito, débito, billetera digital <strong>XISFO</strong>, o cualquier otro instrumento electrónico podrá en cualquier momento y sin que medie justificación alguna:
      </p>
      <ul style="list-style-type: upper-roman;">
        <li class="my-2 text-justify">Comunicar a la entidad con la que pactó el débito automático, por escrito o a través de cualquier medio establecido, entre las partes para ello, que permita conservar un soporte de la misma y acreditar fecha cierta de su presentación, especificando su voluntad de revocar la autorización de realizar los pagos por dichos medios. Si no corresponde el emisor del instrumento delpago con la entidad con la que se pactó el débito automático, el consumidor y/o comprador deberá comunicar al emisor del instrumento del pago sobre la instrucción de cesación de los pagos por dichos medios, dentro de los cinco <strong>(5) días siguientes.</strong></li>
        <li class="my-2 text-justify">Solicitar la reversión del pago correspondiente a cualquier servicio u obligación de cumplimiento periódico. Para proceder con la reversión, el consumidor deberá solicitarla al emisor del instrumento de pago en un tiempo máximo de un mes después de ocurrido el pago por los canales que dicha entidad disponga.
        </li>
      </ul>
      <p class="my-2 text-justify">En caso de que dentro del proceso de reversión de pago resulte demostrada la <strong>MALA FE</strong> por parte del consumidor, se sujetará a las sanciones correspondientes por la <strong>Superintendencia de Industria y Comercio (SIC).</strong> </p>
    </li>
    <li class="my-2 text-justify"><strong>Ley Aplicable:</strong> La Leyes aplicables serán las vigentes en cada país donde el cliente descargue la billetera digital de <strong>XISFO</strong> o utilice cualquiera de sus servicios.
    </li>
  </ul>
</div>

""";
}
}