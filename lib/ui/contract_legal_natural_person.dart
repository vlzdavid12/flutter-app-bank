class TermContractLegal {
  static String contractLegalNaturePerson() {
    return r""" <div class="file-contract">
  <h4 style="text-align: center">CONTRATO DE COMPRAVENTA DE FACTURAS CON DESCUENTO PERSONA NATURAL O JURIDICO</h4>
  <p style="margin: 10px 4px; text-align: justify">Contrato de <strong>COMPRAVENTA DE FACTURAS CON DESCUENTO</strong> suscrito entre el <strong>VENDEDOR</strong> y el <strong>COMPRADOR</strong>, donde el
    presente se regirá por las siguientes cláusulas y en lo previsto en ellas por disposiciones del Código
    de <strong>Comercio</strong> y/o <strong>Código Civil (Art. 772 y 621)</strong>, respectivamente, y demás que sean aplicables a la materia
    de que se ocupa este contrato. </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA PRIMERA. OBJETO DEL CONTRATO:</strong> Por medio de este contrato se establecen las reglas
    generales para operaciones de descuento sobre facturas con responsabilidad que ha expedido
    <strong>EL VENDEDOR</strong> como producto de operaciones reales de comercio. En virtud de la mencionada
    operación, EL VENDEDOR endosará con responsabilidad a favor de <strong>EL COMPRADOR</strong> las facturas, y
    <strong>EL COMPRADOR</strong> pagará la suma equivalente al monto de las mismas, menos un porcentaje (%) por
    concepto de cada una de las operaciones de descuento. <strong>Parágrafo 1.</strong> Los títulos objeto de la
    operación de descuento serán los que se describan en el formulario on-line en la página web de <strong>XISFO</strong>
    que llenará previamente <strong>EL VENDEDOR</strong>. El descuento y desembolso está sujeto a la condición suspensiva
    de verificación  previa de <strong>EL COMPRADOR</strong> de acuerdo con sus políticas internas y los cupos asignados a cada uno de
    los deudores de las facturas.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA SEGUNDA. APTITUD DE LOS TÍTULOS:</strong> Los títulos que presente <strong>EL VENDEDOR</strong> de las obligaciones derivadas del
    contrato que sirve como negocio causal del título valor a descontar, lo cual se entiende en la suscripción del
    presente contrato previo diligenciamiento del formulario on-line en la página web de <strong>XISFO</strong>. <strong>Parágrafo 1.</strong> El descuento
    de los títulos valores presentados a través del formulario on-line como solicitud de pago de facturas, está sujeto a
    la condición suspensiva de la verificación previa de EL COMPRADOR del cumplimiento de los requisitos mencionados.
    <strong>Parágrafo 2.</strong> <strong>EL VENDEDOR</strong> declara que la(s) factura(s) y/o crédito(s) de cada una de las operaciones objeto de este
    contrato que se realicen, y lo cual se entiende con la suscripción del presente instrumento existen y que corresponden
    a deudas reales líquidas y exigibles a cargo del deudor de las facturas, en su fecha de vencimiento y que se ha
    cumplido conforme a los términos del contrato correspondiente, que da origen a las facturas endosadas a cabalidad.
    De manera general <strong>EL VENDEDOR</strong> declara que las facturas y/o créditos referenciados no sufren, ni sufrirán ninguna
    afectación, menoscabo o incumplimiento, ni que serán objeto de transacción o compensación alguna, durante la vigencia
    de este contrato, que pueda llegar a afectar su pago total o parcial en la respectiva fecha de vencimiento.
    <strong>EL VENDEDOR</strong> enviará junto con las facturas debidamente endosadas, copia del acta de entrega y aceptación de los
    bienes y/o servicios objeto de las mismas, por parte del deudor de las facturas. Dicha constancia será parte integral
    del presente contrato. EL VENDEDOR garantiza el cumplimiento de los citados requisitos, así como aquellos descritos
    en la <strong>Ley 1231 de 2008</strong>, por lo que cualquier falencia los hará responsables del pago de las facturas descontadas.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA TERCERA. OBLIGACIONES DEL VENDEDOR:</strong> <strong>1)</strong> EL VENDEDOR se obliga a adjuntar en el
    formato de diligenciamiento on-line la certificación de la cuenta expedida por la entidad financiera correspondiente en
    donde se realizará la transferencia, depósito y/o pago de los dineros que se generen con ocasión de la compra de las
    facturas. Esta cuenta deberá estar registrada a nombre del establecimiento de comercio y si es persona natural a nombre
    de esta. <strong>2)</strong> EL VENDEDOR se obliga a diligenciar el formato on-line de la página web de <strong>EL COMPRADOR.</strong>
    La información que debe suministrar <strong>EL VENDEDOR</strong> es: <strong>A)</strong> Nombre de la plataforma de transmisión.
    <strong>B)</strong> El valor de las facturas que se van a negociar. <strong>C)</strong> El pantallazo del título valor objeto de cobro.
    No se admitirá de ninguna manera instrucciones telefónicas. Cualquier modificación o revocación de la operación requerirá de una
    comunicación firmada por las personas autorizadas y remitida dentro del horario hábil de <strong>XISFO</strong> y tendrá que ser
    aceptada por <strong>EL COMPRADOR. EL COMPRADOR</strong> no realizará ninguna operación, revocación o modificación de la compra de
    facturas sin la comunicación firmada por las personas autorizadas, fuera del horario hábil, o con instrucciones incompletas y poco claras.
    <strong>3)</strong> EL VENDEDOR se compromete a cumplir con la normatividad cambiaria y mediante el formulario on-line autoriza a <strong>EL COMPRADOR</strong>
    para suscribir los documentos que exijan las autoridades monetarias y cambiarias para llevar a cabo la compraventa de las facturas que sean generadas a favor
    de aquel. <strong>4)</strong> EL VENDEDOR llevará los registros contables y soportes exigidos por la ley y los organismos de control respectivos, con ocasión
    de las facturas que se generen a su favor y que es objeto del presente contrato. <strong>5)</strong> EL VENDEDOR deberá dar apertura a una cuenta bancaria,
    cualquiera que sea su denominación en la entidad de su escogencia, o en caso de tenerla activa a la firma del presente contrato, deberá informar por escrito a
    <strong>EL COMPRADOR</strong> de su existencia, a la dirección física y/o electrónica que para el efecto le haya sido notificada. <strong>6)</strong> En caso de presentarse
    un reajuste al valor de las facturas compradas y que disminuyan el valor de la tarifa que se tiene pactada a favor de EL COMPRADOR y que se describe en la cláusula QUINTA
    del presente contrato, <strong>EL VENDEDOR</strong> deberá compensar dicho valor hasta alcanzar el margen de utilidad pactada a favor de <strong>EL COMPRADOR.</strong> En el
    evento en que dicha variación represente un aumento en el valor cancelado por <strong>EL COMPRADOR</strong> y que represente un sobre costo para éste, <strong>EL VENDEDOR</strong>
    devolverá la diferencia causada. <strong>7)</strong> Asumir el riesgo de insolvencia de los clientes de las facturas, así como cubrir el importe de los instrumentos
    crediticios no cancelados. <strong>8)</strong> Cobrar los créditos cuyos derechos ha subrogado a <strong>EL COMPRADOR</strong>. <strong>9)</strong> Informar a <strong>EL COMPRADOR</strong>
    del comportamiento de la cartera de los clientes cedidos. <strong>10)</strong> Ceder a <strong>EL COMPRADOR</strong> los documentos e instrumentos de contenido crediticio objeto de la adquisición.
    <strong>11)</strong> Remitir a <strong>EL COMPRADOR</strong> los rubros que hubieren pagado directamente los clientes cedidos, a fin de cumplir el compromiso de reembolso
    pactado. <strong>12)</strong> Notificar a sus clientes, de la transferencia de los instrumentos de contenido crediticio a favor de <strong>EL COMPRADOR</strong>.
    <strong>13)</strong> Garantizar la existencia de los créditos transferidos
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA CUARTA. OBLIGACIONES DEL COMPRADOR:</strong> <strong>1)</strong> Adquirir los créditos que se originen de la manera y en las condiciones previstas contractualmente.
    <strong>2)</strong> EL COMPRADOR se obliga a transferir al VENDEDOR el dinero, previo menos el descuento que se pacta en la cláusula <strong>QUINTA</strong> del presente contrato
    como tarifa por la labor que representa la compra de facturas que sean generadas a nombre de éste con ocasión de la actividad comercial que realiza <strong>EL VENDEDOR.</strong>
    <strong>3)</strong>EL COMPRADOR se comprometerá a revisar los datos registrados de <strong>EL VENDEDOR</strong> en el formato de diligenciamiento on-line, para verificar toda la
    información suministrada antes de iniciar las operaciones de compra de factura. <strong>Parágrafo 1.</strong> El desembolso que realice EL COMPRADOR como consecuencia de la
    compra de facturas, solo se hará a la cuenta que haya sido adjuntada en el formulario <strong>on-line</strong> por <strong>EL VENDEDOR</strong>, como se menciona en la cláusula
    <strong>TERCERA</strong>, sin embargo, en el evento de que se solicite por parte de <strong>EL VENDEDOR</strong> la realización de un desembolso a la cuenta distinta de la informada,
    <strong>EL VENDEDOR</strong> deberá solicitarlo de manera escrita desde el correo electrónico utilizado en el registro, haciendo claridad del número de cuenta, clase de cuenta, banco
    e identificación del titular de la cuenta. Esta petición, también deberá ir con la respectiva firma de <strong>EL VENDEDOR</strong> o representante legal de la empresa
    (Aplica para personas jurídicas) (Con su respectiva certificación bancaria).
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA QUINTA. REMUNERACIONES DEL COMPRADOR Y FORMA DE PAGO:</strong> <strong>EL VENDEDOR</strong> reconocerá a <strong>EL COMPRADOR</strong> una tarifa representada en el
    <strong>CINCO POR CIENTO (5%)</strong> del valor total de cada una de las compras de las facturas negociadas, la cual será descontada el mismo día que se realiza la negociación de
    las facturas y antes de que se efectúe la consignación en la cuenta bancaria que para el efecto designe EL VENDEDOR, la cual fue suministrada y notificada por medio del formulario
    on-line a EL COMPRADOR, la operación puede generar costos financieros que serán asumidos por <strong>EL VENDEDOR</strong>, en caso de que por error no se descuente el interés,
    <strong>EL COMPRADOR</strong>, podrá debitarlos de los fondos disponibles o del siguiente pago enviando una alerta o notificación a <strong>EL VENDEDOR</strong> por medio de la
    página web de <strong>XISFO.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    Parágrafo. <strong>EL COMPRADOR</strong> se reservará el derecho a ejecutar las facturas, hasta tanto <strong>LA EMPRESA PAGADORA</strong> no haya cancelado los dineros
    correspondientes a la cuenta que <strong>EL COMPRADOR</strong> adjunte o especifique. <strong>EL COMPRADOR</strong> se exime de toda responsabilidad frente a las demoras o retrasos que sean atribuibles a <strong>LA EMPRESA PAGADORA O PLATAFORMAS INTERMEDIARIAS.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA SEXTA. RETRACTACIÓN:</strong> EL VENDEDOR en ningún caso después de realizadas las compras de facturas y/o cerradas las órdenes de compra podrá retractarse de las
    mismas, pudiendo <strong>EL COMPRADOR</strong> culminar con la labor y proceder a descontar la tarifa pactada en la cláusula QUINTA del presente contrato.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA SÉPTIMA.</strong> EL VENDEDOR se compromete con EL COMPRADOR a notificar de forma oportuna y, de manera
    previa al desembolso, la información necesaria para el cobro de las facturas negociadas, a prestar su colaboración para
    obtener la aceptación de la misma, y a efectuar oportunamente todas las gestiones, trámites y requisitos previstos legal
    o contractualmente para que éste efectúe el pago de las facturas directamente a <strong>EL COMPRADOR.</strong> Del mismo
    modo, <strong>EL VENDEDOR</strong> se obliga a no modificar, resolver, novar, prorrogar o de cualquier forma a cambiar o
    afectar los créditos y/o facturas negociadas con EL COMPRADOR, sin la previa autorización de este último.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA OCTAVA.</strong> En caso de no pago de las facturas, ya sea por impago o por una situación imputable
    directamente a <strong>EL VENDEDOR</strong> por el incumplimiento de sus compromisos contractuales frente a los deudores
    de las facturas, <strong>EL COMPRADOR</strong> podrá exigir el reembolso de las sumas entregadas, para ello será necesario
    que <strong>EL COMPRADOR</strong> acredite siquiera sumariamente las razones que el cliente y/o deudor de las facturas
    esgrime como fundamento del incumplimiento contractual. Para obtener el reembolso de las sumas entregadas a <strong>EL VENDEDOR,</strong>
    en cualquier caso, podrá hacer compensación con las sumas que son objeto de otros títulos objeto de la presente relación. <strong>EL COMPRADOR</strong>
    deberá comunicar a <strong>EL VENDEDOR</strong>, con cinco <strong>(5) días</strong>  de antelación, su intención de recuperar los desembolsos hechos,
    durante este lapso <strong>EL VENDEDOR</strong> podrá suministrar la información requerida para proceder con el reembolso o para que la obligación
    sea exigida legalmente. <strong>EL VENDEDOR</strong> deberá reembolsar el dinero pagado de las facturas no canceladas por su cliente a <strong>EL COMPRADOR</strong>
    en un término no mayor a treinta <strong>(30) días calendario.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA NOVENA.</strong> En caso de que, por error, <strong>EL VENDEDOR</strong> reciba el pago total o parcial de las facturas y/o créditos,
    se compromete a trasladar tales recursos inmediatamente a EL COMPRADOR, sin que por esta operación se cause cobro alguno.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA.</strong> La cesión y/o endosos de las facturas no trasladará a EL COMPRADOR obligación ni
    responsabilidad de <strong>EL VENDEDOR</strong> frente al comprador de los bienes y/o servicios, emanada de la relación contractual
    que da origen a los títulos valores descontados.
  </p>
  <p style="margin: 10px 4px; text-align: justify"><strong>CLÁUSULA DÉCIMA PRIMERA. DURACIÓN DEL CONTRATO:</strong> El presente contrato será por un término de UN
    <strong>(1) AÑO</strong>, el cual cubrirá o cobijará todas las facturas que se generen a nombre de <strong>EL VENDEDOR</strong>
    con ocasión a la actividad comercial que se realiza y frente a la cual recibe los respectivos pagos, pudiendo <strong>EL VENDEDOR o EL COMPRADOR</strong>
    terminarlo en cualquier tiempo mediante comunicación escrita dirigida a la otra parte, con una antelación no anterior a <strong>QUINCE (15) DÍAS HÁBILES</strong>, sin que haya
    lugar al pago de los perjuicios o indemnización alguna. <strong>Parágrafo 1.</strong> Las partes podrán RENOVAR o PRORROGAR el presente contrato de común acuerdo, si dentro de
    los <strong>QUINCE (15) DÍAS HÁBILES</strong>, NO se recibe notificación alguna de cualquiera de las partes con la intención de dar por terminado el presente contrato, mismo
    que se renovará o prorrogará por un término igual al inicialmente pactado. Dado el caso de que <strong>EL VENDEDOR</strong> haya tenido una actualización en su información
    deberá comunicar al comprador posterior a la renovación del contrato y se actualizarán las consultas por parte de <strong>EL COMPRADOR.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA SEGUNDA. EJECUCIÓN DEL CONTRATO:</strong> EL COMPRADOR comenzará a realizar las compras de las facturas que se generen a nombre
    de <strong>EL VENDEDOR</strong> con ocasión a la actividad comercial que realiza y frente a la cual recibe los respectivos pagos, a partir del día
    siguiente a la fecha de la firma del contrato y aprobación por parte de <strong>EL COMPRADOR.</strong> Parágrafo 1. Para iniciar el trámite, <strong>EL VENDEDOR</strong>
    deberá diligenciar todo el formulario de diligenciamiento <strong>on-line</strong> correspondiente y enviarlo.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA TERCERA – IMPUESTOS:</strong> EL VENDEDOR será el exclusivo responsable por la declaración y pago
    de todos los impuestos, retenciones o cualquier otro emolumento de orden tributario que resulten en virtud de los pagos
    que el deudor de las facturas efectúe. <strong>EL VENDEDOR</strong> acepta que ninguno de los pagos efectuados implicará
    responsables fiscales, especialmente por cuanto tales recursos serán orientados al pago de las sumas desembolsadas a
    <strong>EL VENDEDOR</strong> a título de descuentos.
  </p>
  <p  style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA CUARTA. TÉRMINACIÓN ANTICIPADA DEL CONTRATO:</strong> Cualquiera de LAS PARTES podrá dar por
    terminado el contrato marco antes del vencimiento de su término, en forma inmediata, automática y extrajudicial cuando
    se presente uno o varios de los siguientes eventos: 1) Que alguna de LAS PARTES incurra en una causal legal de
    disolución, reestructuración, liquidación o toma de posesión de acuerdo con lo establecido en las leyes colombianas.
    <strong>2)</strong> La promulgación de Ley, Decreto, Resolución o cualquier otra norma jurídica, así como una decisión
    judicial ejecutoriada que afecte la existencia y validez o el vigor de alguna norma que haga devenir ilegal en
    cumplimiento de las obligaciones adquiridas, salvo en aquellos casos en que las partes lleguen a un acuerdo que haga
    posible cumplir las obligaciones en concordancia con la normatividad aplicable. <strong>3)</strong>El mutuo acuerdo entre
    las partes. <strong>4)</strong> La configuración de imposibilidad de cumplimiento de las obligaciones adquiridas bajo
    este contrato, cuando esas encuadren dentro de la fuerza mayor o el caso fortuito. <strong>5)</strong> Incumplimiento de
    alguna de las partes en el pago de las obligaciones con terceros que haga suponer una deficiente situación financiera.
    <strong>6)</strong> Por el no pago de la tarifa del contrato. <strong>7)</strong>La decisión unilateral de las partes,
    comunicada a la otra parte, por escrito, y con una antelación mínima de <strong>QUINCE (15) DÍAS HÁBILES.</strong> <strong>8)</strong>
    Por el cumplimiento del término pactado en el marco del presente contrato. <strong>Parágrafo 1.</strong> La terminación anticipada del contrato
    no libera a las partes del cumplimiento de las obligaciones y solamente operará tras la liquidación de todas las sumas y obligaciones pendientes
    de cumplimiento, y contraídas previamente a la ocurrencia del evento de terminación.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA QUINTA. EVENTOS DE INCUMPLIMIENTO:</strong> Se considerará como eventos de incumplimiento la ocurrencia de
    cualquiera de los siguientes eventos: <strong>1)</strong> La falsedad o inexactitud de las declaraciones y garantías otorgadas por
    cualquiera de las partes en este contrato. <strong>2)</strong> El incumplimiento de cualquiera de las obligaciones de pago o entrega,
    una u otra, en las condiciones estipuladas en este contrato. <strong>3)</strong> En el evento en que una parte, unilateralmente sin
    que exista declaración judicial, desconozca, repudie, rechace o cuestione la existencia o validez del contrato.
    En caso de presentarse cualquiera de los anteriores eventos de incumplimiento, se notificará a la parte incumplida,
    especificando cuál de los eventos de incumplimiento ha ocurrido, explicando a un nivel razonable detallado los hechos
    que se han tenido en cuenta, para considerar que, si ha existido dicho evento de incumplimiento, para efectos de
    subsanarlo. En el caso de que se subsane el evento de incumplimiento, tal hecho no implica que se libere al incumplido
    de pagar la totalidad de los daños causados y derivados del incumplimiento, los cuales deberán ser aprobados y
    cuantificados por la parte que los alegue, siempre y cuando se demuestre que es por negligencia de la parte y no por
    un tercero pagador.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA SEXTA. LIMITACIÓN DE RESPONSABILIDAD:</strong> <strong>1)</strong> EL COMPRADOR sólo será responsable por los daños
    que ocasione a <strong>EL VENDEDOR</strong> en virtud de un error atribuible a la negligencia de aquel. <strong>2)</strong> EL VENDEDOR será
    responsable por la información que suministre a EL COMPRADOR, quien no será responsable frente a terceros por daños
    sufridos por EL VENDEDOR y que se causen por la mala información que éste suministre. <strong>3)</strong> EL COMPRADOR no se hará responsable
    en caso de presentarse una reforma a la ley que regule negocios de la naturaleza que en este contrato se celebra y que altere el proceder normal
    – Compraventa de facturas con descuento de la labor contratada. En todo caso, en dicho evento <strong>EL COMPRADOR</strong> informará de los mismos a EL VENDEDOR
    por escrito para su conocimiento. <strong>4)</strong> EL COMPRADOR no se hará responsable de los débitos y/o descuentos que se realicen a <strong>EL VENDEDOR</strong> por parte de
    la entidad bancaria informada posteriores a la transferencia de los dineros negociados. <strong>5)</strong> EL VENDEDOR será el único responsable de sus actuaciones como empresa,
    por lo que cualquier controversia administrativa, extrajudicial o judicial (No pago de acreencias laborales, cumplimientos de contratos, investigaciones fiscales y/o penales, etc.…),
    deberá ser asumida por éste en su totalidad, debiendo salir al saneamiento y responder en favor de <strong>EL COMPRADOR.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA SÉPTIMA. CESIÓN DEL CONTRATO:</strong> Ninguna de LAS PARTES podrá ceder el contrato, salvo
    previa autorización expresa y escrita de la otra.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA OCTAVA. NATURALEZA:</strong> LAS PARTES hacen constar expresamente que en el presente contrato no
    existe continuada subordinación y dependencia entre ellas, por consiguiente, no se configura vínculo laboral alguno entre
    los colaboradores que <strong>EL COMPRADOR</strong> contrate para dar cumplimiento al objeto del presente contrato (Si así lo considera
    necesario), y <strong>EL VENDEDOR</strong>, ni entre los empleados de este y <strong>EL COMPRADOR</strong>. En consecuencia, cada una de las partes tendrá
    la calidad de empleador directo respecto de sus trabajadores.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA DÉCIMA NOVENA. CONFIDENCIALIDAD:</strong> En virtud del presente contrato, LAS PARTES se obligan a no
    revelar, divulgar, exhibir, mostrar, comunicar, utilizar y/o emplear la información con persona natural o jurídica,
    en su favor o en el de terceros, que reciban de la otra parte y en consecuencia a mantenerla de manera confidencial y
    privada y a proteger dicha información para evitar su divulgación no autorizada, ejerciendo sobre esta el mismo grado de diligencia que utiliza para proteger información confidencial de su propiedad, haciéndose extensivo el compromiso a los administradores. Se define como información confidencial toda información técnica, financiera, comercial y estratégica y cualquier información relacionada con las operaciones de negocios presentes y futuros, o condiciones financieras de LAS PARTES, bien sea que dicha información sea escrita, oral o visual. Complementariamente, y para los efectos del presente contrato, LAS PARTES manifiestan expresamente desde ya, que la información utilizada en el desarrollo de su objeto social tiene el carácter de información confidencial. Adicionalmente, cualquier información suministrada, previa a la celebración y durante la ejecución del presente contrato, se considerará como confidencial y estará sujeta a los términos del mismo. La Información Confidencial no puede ser utilizada por la PARTE RECEPTORA en detrimento de la PARTE REVELADORA o para fines diferentes a los establecidos en el presente contrato ya que sólo podrá utilizarse en relación con los temas que se desarrollen en el contexto de este contrato de prestación de servicios. La PARTE RECEPTORA no distribuirá, publicará o divulgará la información confidencial o el material que se derive de la realización del presente contrato a persona alguna, salvo a sus empleados que tengan necesidad de conocerla para el propósito para el cual es divulgada.
    La información confidencial de la <strong>PARTE REVELADORA</strong> deberá ser tratada como tal y resguardada bajo este aspecto por la
    PARTE RECEPTORA. La entrega de información sea confidencial o no, no concede, ni expresa ni implícitamente, autorización,
    permiso o licencia de uso de marcas comerciales, patentes, derechos de autor o de cualquier otro derecho de propiedad
    industrial o intelectual. Ni este contrato, ni la entrega o recepción de información, sea confidencial o no, constituirá
    o implicará promesa de efectuar contrato alguno por cualquiera de <strong>LAS PARTES.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA. MEDIOS ELECTRÓNICOS:</strong> Con la aceptación del presente documento, <strong>LAS PARTES</strong> convienen que algunos
    procedimientos y actividades derivados de los servicios aquí regulados podrán tramitarse a través de medios
    electrónicos, incluyendo la fase previa del formulario on-line. Estos procedimientos podrán extenderse, sin
    limitarse, al intercambio de información, así como la autorización de desembolsos, operaciones derivadas de
    los servicios, transferencias, pagos a terceros, entre otros.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA PRIMERA. COMPROMISORIA – TRIBUNAL DE ARBITRAMIENTO:</strong> Cualquier divergencia que surja
    entre las partes con ocasión de la celebración, ejecución o liquidación de este contrato que no sea posible solucionar
    amigablemente conforme a lo establecido en las cláusulas precedentes, será dirimida por un Tribunal de Arbitramento,
    el que se regirá por las siguientes reglas: El tribunal estará compuesto por tres <strong>(3) árbitros designados de común
    acuerdo por las partes</strong>. Tal designación deberá realizarse dentro de los diez (10) días siguientes a la fecha en que se
    entienda agotada la etapa de conciliación. Si tal acuerdo no se lograra, las partes recurrirán a la <strong>Cámara de
    de Bogotá</strong> para que sea ésta quien los designe. No obstante, si el valor económico de la disputa, estimado por
    la parte que la propusiere, fuere inferior a <strong>DOSCIENTOS MILLONES DE PESOS ($200.000.000),</strong> el tribunal estará integrado
    por un árbitro único. Los árbitros decidirán en derecho.  El tribunal se sujetará al reglamento del Centro de Arbitraje
    y <strong>Conciliación de la Cámara de Comercio de Bogotá,</strong> y se regirá por lo previsto en esta cláusula y por las disposiciones
    de la <strong>Ley 1563 de 2012,</strong> o por las normas que lo adicionen, modifiquen, o reemplacen. La sede del tribunal será Bogotá.
    Los gastos que ocasione el tribunal de arbitramento serán cubiertos por la parte que resulte vencida.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA SEGUNDA. VALIDEZ DEL DOCUMENTO:</strong> Si cualquiera de las cláusulas o el texto parcial de alguna de ellas
    se declara nulo, ininteligible o en conflicto con la ley, la validez de las demás estipulaciones no será afectada por la
    declaración. Las partes acuerdan negociar en buena fe para reemplazar las cláusulas inválidas, inaplicables o ilegales,
    para preservar los derechos y obligaciones de ambas partes de la mejor y más práctica forma y de conformidad con los
    términos del <strong>artículo 902 del Código de Comercio.</strong>
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA TERCERA. REFORMAS AL CONTRATO:</strong> Ninguna reforma, modificación o adición a este contrato
    obligará a ninguna de las partes, salvo que conste por escrito y esté suscrita por ambas. Por consiguiente, no existen
    otras restricciones, promesas, pronunciamientos, garantías, estipulaciones o contrato diferentes a los que expresamente
    están aquí contemplados. Este contrato y sus anexos prevalecen sobre cualquier entendimiento previo a las partes.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA CUARTA. HABEAS DATA:</strong> Los datos consignados en el presente contrato, y en general todos
    aquellos que se encuentren en el ecosistema de XISFO serán tratados de acuerdo a lo establecido en la <strong>Ley 1581 de 2012,</strong>
    en el <strong>Decreto 1377 de 2013</strong> y en cualquier otra normatividad referente a la protección de información.
    <strong>LAS PARTES</strong> acuerdan conocer y acatar las políticas de protección y tratamiento de datos personales de
    las contrapartes.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA QUINTA. TÍTULO EJECUTIVO:</strong> Para todos los efectos, el presente contrato presta mérito
    ejecutivo al establecerse en el presente contrato una obligación clara, expresa y exigible, y se hará exigible al día
    siguiente de la fecha en la que se incumpla el pago de la tarifa pactada y que dé lugar a exigir el cumplimiento del
    presente contrato por vía judicial. Por lo que acreditarse cualquiera de ellas, no se necesitará constituir en mora a
    la parte que incumple.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA SEXTA. DOMICILIO:</strong> LAS PARTES convienen en señalar como domicilio principal la ciudad de Bogotá, departamento de Cundinamarca.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA SÉPTIMA. NOTIFICACIONES:</strong> EL COMPRADOR recibirá las notificaciones a las que haya lugar
    en La <strong>Calle 93 A, #13-24, Edificio QBO Piso 5, Oficina 26 en la ciudad de Bogotá,</strong> o a los correos electrónicos
    <strong>info@xisfo.com</strong> y  <strong>lidercomercial@xisfo.co</strong>, <strong>EL VENDEDOR</strong> en la dirección física,
    correos electrónicos y números de teléfono que refiera en el formato de vinculación que se completa previa la suscripción del
    contrato. El cambio de dirección lo informarán <strong>LAS PARTES</strong> por escrito, por medio de correo certificado la cuál
    empezará a regir cinco <strong>(5) días hábiles</strong> después de la fecha de recepción de la otra parte.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA VIGÉSIMA OCTAVA. CENTRALES DE RIESGO:</strong> EL <strong>VENDEDOR</strong> autoriza a <strong>EL COMPRADOR</strong> de manera libre,
    expresa e irrevocables para que obtenga de cualquier fuente y reporte y actualice a cualquier banco de datos las
    informaciones y referencias relativas a su persona y documento de identificación, a su comportamiento y crédito
    comercial, hábitos de pago, manejo de su (s) cuenta (s) corriente (s) bancaria (s) y en general el cumplimiento de
    sus obligaciones pecuniarias. <strong>Parágrafo 1.</strong> En caso de reportes negativos antes las Centrales de Riesgo <strong>EL COMPRADOR</strong>
    se obliga con <strong>EL VENDEDOR</strong> a dar cumplimiento al término de que trata las notificaciones previas establecido en el
    <strong>artículo 12 de la Ley 1266 de 2009</strong> y las demás normas concordantes.  Consultas en listas restrictivas nacionales e
    internacionales
  </p>
  <p style="margin: 10px 4px; text-align: justify">
  <strong>CLÁUSULA VIGÉSIMA NOVENA. ESTIPULACIONES ANTERIORES:</strong> Este contrato presenta el acuerdo total entre las partes y
  deroga las estipulaciones anteriores que pudieran haberse pactado entre las partes de manera verbal o escrita con relación
  al mismo objeto.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA TRIGÉSIMA. PREVENCIÓN DE LAVADO DE ACTIVOS:</strong> LAS PARTES declaran que: <strong>A)</strong> Acatan las normas de
    prevención de lavado de activos y financiación del terrorismo. <strong>B)</strong> Aplican como práctica comercial la debida diligencia
    en el conocimiento de sus clientes y proveedores. <strong>C)</strong> No permitirán que bienes o estructuras de negocio sean utilizadas
    para actividades delictivas, lavado de activos o financiación del terrorismo. <strong>D)</strong> Tienen una estructura de capital
    transparente y proporcionarán la información necesaria para prevenir el riesgo de lavado de activos. En consecuencia,
    se obligan a responder ante la otra parte por todos los perjuicios que se llegaren a causar como consecuencia de esta
    afirmación. Será justa causa de terminación de este contrato, el hecho que la otra parte o sus socios sean incluidos
    en procesos de investigación como sospechosos de lavado de activos y terrorismo.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    <strong>CLÁUSULA TRIGÉSIMA PRIMERA. INDEMNIDAD:</strong> Cada Parte se obliga a partir de la fecha de suscripción de este contrato a proteger
    y mantener indemne a la Otra Parte, así como a cualquiera de sus respectivas divisiones, subsidiarias, afiliadas, matrices, y a sus respectivos
    accionistas, oficiales, directivos, empleados, agentes, sucesores, cesionarios y a cualquier otra parte a la que estos deban mantener indemne
    (los “Indemnizados” o el “Indemnizado”), por cualquier daño o perjuicio que se les cause como consecuencia de cualquier reclamación que pueda
    surgir por parte de un tercero o autoridad gubernamental contra los Indemnizados, que surjan luego de la fecha de suscripción de este contrato
    y como consecuencia de actos o hechos ocurridos a partir de dicha fecha y hasta la fecha de terminación del mismo, en relación con lo siguiente:
    <strong>(I)</strong> El incumplimiento de las obligaciones de las Partes bajo el presente contrato o <strong>(II)</strong> Cualquier hecho, acto
    o asunto respecto de asuntos corporativos, tributarios o laborales de las Partes durante la ejecución del presente
    contrato todo lo anterior siempre y cuando no medie dolo o culpa leve del Indemnizado.
  </p>
  <p style="margin: 10px 4px; text-align: justify">
    El contrato quedará firmado una vez <strong>EL VENDEDOR</strong> realice y complete, la verificación y autenticación por medio del onboarding digital.
  </p>
</div>
""";
  }
  static String contractDeclarationFound(){
    return r"""
    <ul>
      <li style="margin: 10px 5px; text-align: justify">Obrando en mi propio nombre y en representación legal de la sociedad, de manera voluntaria y dando certeza que todo lo aquí consignado y en los documentos anexos es cierto, realizo la siguiente declaración de origen y destino de fondos a <strong>S&C GROUP S.A.S.</strong> provienen de:  Actividades lícitas de acuerdo a la actividad económica </li>
      <li style="margin: 10px 5px; text-align: justify">Declaro de manera expresa y voluntaria que el origen de los recursos y/o bienes y/o servicios y/o recursos financieros, que manejo y manejaré en desarrollo de cualquier relación con <strong>SYC GROUP S.A.S</strong> provienen de actividades licitas, enmarcadas dentro de nuestro objeto social, y no resultan de ninguna actividad ilícita de las contempladas en el Código Penal colombiano, y/o en la normatividad vigente en materia de lavado de activos y/o financiación de terrorismo.</li>
      <li style="margin: 10px 5px; text-align: justify">Declaro que ni yo, ni la persona jurídica que represento, ni sus accionistas, ni sus asociados, ni sus socios, ni sus representantes legales y/o sus miembros de la Junta Directiva,</li>
    </ul>
    
    """;
  }
}