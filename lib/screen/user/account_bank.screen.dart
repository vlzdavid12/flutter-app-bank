import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/bloc/bank/bank_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/models/list_bank.model.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';
import 'package:xisfo_app/widgets/form/checkbox_input.widgets.dart';
import 'package:xisfo_app/widgets/form/dropdown_input.widgets.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';
import 'package:xisfo_app/widgets/modal.widgets.dart';

class AccountBankScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(15),
      child: BlocProvider(
          create: (BuildContext context) => BankBloc(), child: _BankListView()),
    ));
  }
}

class _BankListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final international = S.of(context);
    final listBanks = context.watch<BankBloc>().state.banks;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(international.registerAccountBank("Cafer"),
            style: const TextStyle(
                fontSize: 45,
                fontFamily: 'Dongle',
                fontWeight: FontWeight.w700,
                height: 0.8,
                color: Color(0xff6C4F92))),
        const SizedBox(height: 10),
        Text(international.recordBankText,
            style: const TextStyle(fontSize: 14, color: Colors.black45)),
        const SizedBox(height: 20),
        Center(
          child: MaterialButton(
              minWidth: 480,
              height: 50,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              disabledColor: const Color(0xffc7a314),
              focusColor: const Color(0xffc7a314),
              splashColor: const Color(0xffc7a314),
              highlightColor: const Color(0xffEDCF53),
              elevation: 0,
              color: const Color(0xffEDCF53),
              child: Text(international.addAccount.toUpperCase(),
                  style: const TextStyle(
                      color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
              onPressed: () {
                _showAddBank(context);
              }),
        ),
        const SizedBox(
          height: 10,
        ),
        Expanded(
            child: ListView.builder(
                itemCount: listBanks.length,
                itemBuilder: (context, index) {
                  final item = listBanks[index];
                  return Dismissible(
                    direction: DismissDirection.endToStart,
                    key: UniqueKey(),
                    onDismissed: (DismissDirection direction) {
                      if (direction == DismissDirection.endToStart) {
                        context
                            .read<BankBloc>()
                            .add(RemoveBankEvent(bank: item));
                      }
                    },
                    background: Container(
                      alignment: AlignmentDirectional.centerEnd,
                      color: const Color(0xFF6D3DB0),
                      child: const Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 30.0, 0.0),
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    child: TextButton(
                        onPressed: () {
                          _showBankDetails(context, item);
                        },
                        child: ListTile(
                          leading: const Icon(Icons.chevron_right_sharp),
                          title: Text(item.nameBank),
                        )),
                  );
                }))
      ],
    );
  }
}

Future<void> _showBankDetails(BuildContext context, ListBank item) async {
  final detailsContent = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsets.all(10),
            child: Text("Detalles",
                style: const TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w700,
                    fontSize: 24))),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Text("Nombre Banco: ${item.nameBank}",
              style: const TextStyle(color: Colors.black54)),
        ),
        Padding(
            padding: const EdgeInsets.all(10),
            child: Text("Tipo Cuenta:  ${item.typeAccount}",
                style: const TextStyle(color: Colors.black54))),
        Padding(
            padding: const EdgeInsets.all(10),
            child: Text("Número Cuenta:  ${item.numberAccountBank}",
                style: const TextStyle(color: Colors.black54))),
      ]);
  dialogModalBuilder(context, detailsContent);
}

Future<void> _showAddBank(BuildContext context) async {
  final international = S.of(context);
  final GlobalKey<FormState> _formBankey = GlobalKey<FormState>();
  TextEditingController valueBank = TextEditingController();
  TextEditingController valueTypeAccount = TextEditingController();
  String? _numberAccountBank;

  void _addSubmit() {
    final form = _formBankey.currentState;
    if (form == null || !form.validate()) return;
    form.save();
    context.read<BankBloc>().add(AddBankEvent(
        nameBank: valueBank.text,
        typeAccount: valueTypeAccount.text,
        numberAccount: _numberAccountBank!));
    Navigator.of(context).pop();
  }

  final childContent = SingleChildScrollView(
    child: Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: _formBankey,
      child: Column(
        children: [
          DopDownInput(
            list: ['Bancolombia'],
            labelDocument: international.bank,
            dropDownSelect: valueBank,
            prefixIcon: Icons.business,
          ),
          DopDownInput(
            list: ['Ahorro', "Corriente"],
            labelDocument: international.typeAccountBank,
            dropDownSelect: valueTypeAccount,
            prefixIcon: Icons.account_balance_wallet,
          ),
          TextFormField(
            autocorrect: false,
            style: const TextStyle(color: Colors.black45),
            keyboardType: TextInputType.phone,
            validator: (String? value) =>
                (value == null || value.trim().isEmpty)
                    ? "Campo es requerido"
                    : null,
            onSaved: (String? value) {
              _numberAccountBank = value;
            },
            decoration: InputDecorations.generalInputDecoration(
                hinText: '#######',
                labelText: international.numberAccountBank,
                prefixIcon: Icons.numbers_sharp,
                colorInput: Colors.grey,
                colorError: const Color(0xFFC21839)),
          ),
          CheckBoxInput(lisTitles: [international.sureAccountAdd]),
          CheckBoxInput(
              lisTitles: [international.confirmVerificationTransferBank]),
          const SizedBox(height: 5),
          MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              disabledColor: const Color(0xffa18bc7),
              focusColor: const Color(0xff4d2d79),
              splashColor: const Color(0xff53298a),
              highlightColor: const Color(0xff6C4F92),
              elevation: 0,
              color: const Color(0xff6C4F92),
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  international.addAccountBank.toUpperCase(),
                  style: const TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),
              ),
              onPressed: () => _addSubmit()),
        ],
      ),
    ),
  );
  dialogModalBuilder(context, childContent);
}
