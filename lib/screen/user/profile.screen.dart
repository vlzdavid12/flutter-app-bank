import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/bloc/foreign_currency/foreign_currency_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';
import 'package:xisfo_app/widgets/modal.widgets.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return LayoutApp(
      children: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(internalization.myAccount,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      color: Color(0xff6C4F92),
                      fontFamily: 'Dongle',
                      fontSize: 52,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -2)),
              const Text('Pinkesh Darji',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.black54,
                      height: 0.1,
                      fontWeight: FontWeight.w700,
                      fontSize: 18)),
              const SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: const Color(0xff6C4F92),
                        borderRadius: BorderRadius.circular(50)),
                    padding: const EdgeInsets.all(10),
                    child: const Icon(Icons.email, color: Colors.white),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(internalization.email,
                            style: const TextStyle(color: Colors.black54)),
                        const Text('pinkesh.earth@gmail.com',
                            style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w700, fontSize: 18))
                      ]),
                ],
              ),
              const SizedBox(height: 10),
              Align(
                alignment: Alignment.center,
                child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 40, vertical: 15),
                      child: Text(internalization.updateEmail.toUpperCase(),
                          style: const TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700)),
                    ),
                    onPressed: () {
                      _showModalChangeEmail(context);
                    }),
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: const Color(0xff6C4F92),
                        borderRadius: BorderRadius.circular(50)),
                    padding: const EdgeInsets.all(10),
                    child: const Icon(Icons.phone, color: Colors.white),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(internalization.mobil,
                            style: const TextStyle(color: Colors.black54)),
                        const Text('3213456789',
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontWeight: FontWeight.w700))
                      ]),
                ],
              ),
              const Divider(height: 50, color: Colors.black45),
              Text(internalization.myData,
                  style: const TextStyle(
                      color: Color(0xff6C4F92),
                      fontFamily: 'Dongle',
                      fontSize: 52,
                      height: 0.8,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -2)),
              Stack(children: [
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    width: double.infinity,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    child: const Text("Cédula de ciudadanía", style: TextStyle(color: Colors.black26))),
                Positioned(
                    top: 3,
                    left: 12,
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        decoration: const BoxDecoration(color: Colors.white),
                        child: Text(internalization.typeDocument, style: TextStyle(color: Color(0xff6C4F92)),))),
              ]),
              Stack(children: [
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    width: double.infinity,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    child: const Text("43545896789", style: TextStyle(color: Colors.black26),)),
                Positioned(
                    top: 3,
                    left: 12,
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        decoration: const BoxDecoration(color: Colors.white),
                        child: Text(internalization.noIdentification, style: const TextStyle(color:  Color(0xff6C4F92)),))),
              ]),
              Align(
                alignment: Alignment.center,
                child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 40, vertical: 15),
                      child: Text(internalization.updatePassword.toUpperCase(),
                          style: const TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700)),
                    ),
                    onPressed: () {
                      _showModalChangePassword(context);
                    }),
              ),
              const SizedBox(
                height: 10,
              ),
              const Divider(height: 30, color: Colors.black45),
              Text(internalization.youCanAlso,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      color: Color(0xff6C4F92),
                      fontFamily: 'Dongle',
                      fontSize: 52,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -2)),

              Align(
                alignment: Alignment.center,
                child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 15),
                      child: Text(internalization.activeForeignCurrency.toUpperCase(),
                          style: const  TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700)),
                    ),
                    onPressed: ()  => Navigator.pushNamed(context, 'options_subscription')),
              ),
              const SizedBox(
                height: 10,
              ),
              Center(child: Text(internalization.textProcessMoney, style: const TextStyle(color: Colors.black45))),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: MaterialButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          disabledColor: const Color(0xffc7a314),
                          focusColor: const Color(0xffc7a314),
                          splashColor: const Color(0xffc7a314),
                          highlightColor: const Color(0xffEDCF53),
                          elevation: 0,
                          color: const Color(0xffEDCF53),
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                            child: Text(internalization.activePartner.toUpperCase(),
                                style: const TextStyle(
                                    color: Color(0xff6C4F92),
                                    fontWeight: FontWeight.w700)),
                          ),
                          onPressed: () {

                          }),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Center(child: Text(internalization.textAllyCommercial, style: const TextStyle(color: Colors.black45))),
                  ],
                ),
              ),
            
              const SizedBox(height: 150)
            ],
          ),
        ),
      ),
    );
  }
}

_showModalChangePassword(context) {
  final internalization = S.of(context);
  final childContent = Form(
    autovalidateMode: AutovalidateMode.onUserInteraction,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(internalization.changePin,
            style: const TextStyle(
                color: Colors.black45,
                fontSize: 40,
                fontFamily: 'Dongle',
                height: 0.8),
            textAlign: TextAlign.left),
        Text(internalization.changeTextPin,
            style: const TextStyle(color: Colors.black45, fontSize: 16)),
        const SizedBox(
          height: 15,
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 8),
          width: 290,
          child: TextFormField(
            autocorrect: false,
            style: const TextStyle(color: Colors.grey),
            keyboardType: TextInputType.phone,
            obscureText: true,
            decoration: InputDecorations.generalInputDecoration(
                hinText: '****',
                labelText: internalization.Pin,
                prefixIcon: Icons.password,
                colorInput: Colors.black45),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 8),
          width: 290,
          child: TextFormField(
            autocorrect: false,
            style: const TextStyle(color: Colors.grey),
            keyboardType: TextInputType.phone,
            obscureText: true,
            decoration: InputDecorations.generalInputDecoration(
                hinText: '****',
                labelText: internalization.confirmTextPin,
                prefixIcon: Icons.password,
                colorInput: Colors.black45,
                colorError: const Color(0xFFC21839)),
          ),
        ),
        const SizedBox(height: 10),
        Align(
          alignment: Alignment.center,
          child: MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              disabledColor: const Color(0xffc7a314),
              focusColor: const Color(0xffc7a314),
              splashColor: const Color(0xffc7a314),
              highlightColor: const Color(0xffEDCF53),
              elevation: 0,
              color: const Color(0xffEDCF53),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                child: Text(internalization.btnChangePIN.toUpperCase(),
                    style: const TextStyle(
                        color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
              ),
              onPressed: () {}),
        ),
      ],
    ),
  );
  dialogModalBuilder(context, childContent);
}

_showModalChangeEmail(context) {
  final internalization = S.of(context);
  final childContent = Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(internalization.changeEmail,
          style: const TextStyle(
              color: Colors.black45,
              fontSize: 40,
              fontFamily: 'Dongle',
              height: 0.8),
          textAlign: TextAlign.left),
      Container(
        margin: const EdgeInsets.symmetric(vertical: 8),
        width: 290,
        child: TextFormField(
          autocorrect: false,
          style: const TextStyle(color: Colors.grey),
          keyboardType: TextInputType.text,
          decoration: InputDecorations.generalInputDecoration(
              hinText: 'micorreo@xisfo.co',
              labelText: internalization.email,
              prefixIcon: Icons.email,
              colorInput: Colors.black45,
              colorError: const Color(0xFFC21839)),
        ),
      ),
      const SizedBox(height: 10),
      Align(
        alignment: Alignment.center,
        child: MaterialButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            disabledColor: const Color(0xffc7a314),
            focusColor: const Color(0xffc7a314),
            splashColor: const Color(0xffc7a314),
            highlightColor: const Color(0xffEDCF53),
            elevation: 0,
            color: const Color(0xffEDCF53),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
              child: Text(internalization.changeUpdate.toUpperCase(),
                  style: const TextStyle(
                      color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
            ),
            onPressed: () {}),
      ),
    ],
  );
  dialogModalBuilder(context, childContent);
}
