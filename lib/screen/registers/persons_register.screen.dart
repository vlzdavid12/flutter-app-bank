import 'package:flutter/material.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';

import 'step/person_natural/accountDetails.screen.dart';
import 'step/person_natural/autenticationData.screen.dart';
import 'step/person_natural/endRegisterXisfo.screen.dart';

class PersonsRegisterScreen extends StatefulWidget {
  @override
  State<PersonsRegisterScreen> createState() => _PersonsRegisterScreenState();
}

class _PersonsRegisterScreenState extends State<PersonsRegisterScreen> {
  PageController pageController = PageController();
  late int pageChanged;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    changeStatusLight();
    pageChanged = 0;
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final internalization = S.of(context);
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          toolbarHeight: 55,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Container(
              padding: const EdgeInsets.only(top: 5),
              height: 48, child: Image.asset('assets/images/logo-purple.png')),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Container(
                    width: 550,
                    padding: const EdgeInsets.all(15),
                    child: Column(children: [
                      SizedBox(
                          width: double.infinity,
                          height: size.height / 1.3,
                          child: PageView(
                              physics: const NeverScrollableScrollPhysics(),
                              pageSnapping: true,
                              controller: pageController,
                              onPageChanged: (index) {
                                setState(() {
                                  pageChanged = index;
                                });
                              },
                              children: [
                                StepOne(pageController, pageChanged, internalization),
                                StepTwo(pageController, pageChanged, internalization),
                                StepThree(size: size, internalization: internalization),
                              ])),
                    Container(
                        padding: const EdgeInsets.only(bottom: 10),
                        width: double.infinity,
                        child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Icon(Icons.arrow_back_ios_new,
                                    size: 14, color: Colors.black45),
                                Text(internalization.back,
                                    style: const TextStyle(
                                        color: Colors.black45,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14))
                              ],
                            ))),
                      _Dots(3, pageChanged)
                    ])))));
  }
}



class _Dots extends StatelessWidget {
  final int totalSlides;
  final int pageChanged;

  const _Dots(this.totalSlides, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: SizedBox(
        width: double.infinity,
        height: 25,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(totalSlides, (i) => _Dot(i, pageChanged)),
        ),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;
  final int pageChanged;
  late Color color;
  late double widthBox;

  _Dot(this.index, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    if (pageChanged >= index - 0.5 && pageChanged < index + 0.5) {
      color = const Color(0xff6C4F92);
      widthBox = 8;
    } else {
      color = Colors.grey;
      widthBox = 5;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: 40,
      height: widthBox,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          color: color, //Colors.grey
          borderRadius: BorderRadius.circular(10)),
    );
  }
}

