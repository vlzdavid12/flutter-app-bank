import "package:flutter/material.dart";
import "package:lottie/lottie.dart";
import "package:metamap_plugin_flutter/metamap_plugin_flutter.dart";
import "package:xisfo_app/widgets/btnGroup.widget.dart";
import "package:xisfo_app/widgets/header.widget.dart";

import '../../../../generated/l10n.dart';

class StepTwo extends StatelessWidget {

  PageController pageController;
  S internalization;
  int pageChanged;


  void showMetaMapFlow() {
    final metaData = {"fixedLanguage": "es"};
    MetaMapFlutter.showMetaMapFlow(
        "6356ae14a74bbc001cd5bf62", "63570aac5c6bdb001c725a74", metaData);
    MetaMapFlutter.resultCompleter.future.then((result) => print(result));
  }

  StepTwo(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: const EdgeInsets.all(0),
      color: Colors.transparent,
      child: Column(
        children: [
          HeaderFormWidget(
            stepNumber: '02',
            title: internalization.authData,
            subTitle:
            'Verificamos que tus datos sean legitimos',
          ),
          const SizedBox(height: 20),
          const Text('Verifiquemos tu identidad'),
          Expanded(
              flex: 1,
              child: Column(
                children: [
                  Lottie.asset('assets/images/persons/check_register.json', width: 220),
                  const SizedBox(height: 5),
                  MaterialButton(
                    minWidth: 140,
                    height: 50,
                    shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          width: 0.3,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    highlightColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    focusColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    elevation: 0,
                    color: Colors.transparent,
                    onPressed: showMetaMapFlow,
                    child: Text(internalization.nowVerificar,
                        style: const TextStyle(
                            color: Colors.black45,
                            fontWeight: FontWeight.w700)),
                  ),
                ],
              )),
          BtnGroupWidget(
              pageController: pageController,
              pageChanged: pageChanged, internalization: internalization,),
          const SizedBox(height: 25),
        ],
      ),
    );
  }
}