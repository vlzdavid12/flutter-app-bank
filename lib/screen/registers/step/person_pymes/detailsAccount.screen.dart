import "package:flutter/material.dart";
import "package:xisfo_app/generated/l10n.dart";
import "package:xisfo_app/ui/input_decorations.ui.dart";
import "package:xisfo_app/widgets/btnGroup.widget.dart";
import "package:xisfo_app/widgets/header.widget.dart";

class StepOne extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepOne(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    final internalization =  S.of(context);
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(0),
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            HeaderFormWidget(
              stepNumber: '01',
              title: 'Cuenta',
              subTitle: internalization.detailsAccount,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.phone,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '000 000 0000',
                  labelText: 'Número telefónico',
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              maxLength: 4,
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.phone,
              obscureText: true,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '****',
                  labelText: 'Crea tu PIN',
                  prefixIcon: Icons.password,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.phone,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'Razón social',
                  labelText: 'Nombre legal - Empresa',
                  prefixIcon: Icons.account_circle,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'Cómo se llama la empresa',
                  labelText: 'Nombre Comercial',
                  prefixIcon: Icons.business,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'micorreo@xisfo.co',
                  labelText: 'Correo electrónico',
                  prefixIcon: Icons.email,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'No.',
                  labelText: 'Nit Empresa',
                  prefixIcon: Icons.storefront_sharp,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,)
          ],
        ),
      ),
    );
  }
}
