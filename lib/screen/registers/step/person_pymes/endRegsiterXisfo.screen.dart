import "package:flutter/material.dart";
import "package:flutter_html/flutter_html.dart";
import "package:xisfo_app/generated/l10n.dart";
import "package:xisfo_app/ui/contract_term_wallet.ui.dart";
import "package:xisfo_app/widgets/form/checkbox_input.widgets.dart";
import "package:xisfo_app/widgets/header.widget.dart";

class StepThree extends StatelessWidget {
  const StepThree({
    Key? key,
    required this.size,
    required this.internalization
  }) : super(key: key);

  final Size size;
  final S internalization;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        children: [
          HeaderFormWidget(
            stepNumber: '03',
            title: internalization.welcomeXisfo,
            subTitle: internalization.welcomeXisfo,
          ),
          Column(
            children: [
              SizedBox(
                height: size.height / 2.6,
                child: SingleChildScrollView(
                    child: Html(data: TermContract.contractWallet())),
              ),
              CheckBoxInput(lisTitles: [ internalization.acceptTermCondition ]),
            ],
          ),
          MaterialButton(
              minWidth: 320,
              height: 50,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              disabledColor: const Color(0xffc7a314),
              focusColor: const Color(0xffc7a314),
              splashColor: const Color(0xffc7a314),
              highlightColor: const Color(0xffEDCF53),
              elevation: 0,
              color: const Color(0xffEDCF53),
              child: Text(internalization.send.toUpperCase(),
                  style: const TextStyle(
                      color: Color(0xff6C4F92),
                      fontWeight: FontWeight.w700)),
              onPressed: () {}),
        ],
      ),
    );
  }
}