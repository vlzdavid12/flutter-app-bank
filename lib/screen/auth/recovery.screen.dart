import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';

import '../../bloc/auth/step/step_bloc.dart';

class RecoveryPass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final internalization = S.of(context);
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(20),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FadeIn(
              duration: const Duration(milliseconds: 2000),
              child: SizedBox(
                  width: 150,
                  child: Image.asset('assets/images/logo-purple.png')),
            ),
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              width: 380,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    internalization.forgotPassword,
                    style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 35,
                        fontFamily: 'Dongle',
                        height: 1.0),
                    textAlign: TextAlign.left,
                  ),
                  FadeIn(
                      duration: const Duration(milliseconds: 2000),
                      child: const Text('🔒',
                          style: TextStyle(fontSize: 25, height: 0.4))),
                ],
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            SizedBox(
              width: 380,
              child: Text(internalization.textRecovery,
                  textAlign: TextAlign.left,
                  style:
                      const TextStyle(height: 1.0, color: Color(0xFFABA6B2))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 380,
              child: TextFormField(
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: 'micorreo@xisfo.co',
                    labelText: internalization.email,
                    prefixIcon: Icons.email,
                    colorInput: Colors.black45,
                    colorError: const Color(0xFFC21839)),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                disabledColor: const Color(0xffa18bc7),
                focusColor: const Color(0xff4d2d79),
                splashColor: const Color(0xff53298a),
                highlightColor: const Color(0xff6C4F92),
                elevation: 0,
                color: const Color(0xff6C4F92),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
                  child: Text(internalization.send.toUpperCase(),
                      style: const TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700)),
                ),
                onPressed: () {}),
            const SizedBox(
              height: 20,
            ),
            MaterialButton(
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                elevation: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(Icons.keyboard_arrow_left_rounded,
                        color: Color(0xff6C4F92)),
                    Text(
                      internalization.back,
                      style: const TextStyle(color: Color(0xff6C4F92)),
                    ),
                  ],
                ),
                onPressed: () {
                  context.read<StepBloc>().add(const SetChangeStepEvent(newStep: true));
                  Navigator.pop(context);
                })
          ],
        ),
      ),
    ));
  }
}
