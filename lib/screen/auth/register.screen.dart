import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';
import 'package:xisfo_app/widgets/widgets.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _SliderShow());
  }
}

class _SliderShow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Slideshow(
        bulletPrimary: 9,
        bulletSecondary: 6,
        colorPrimary: const Color(0xffEDCF53),
        colorSecondary: Colors.white,
        slides: [
          _SliderPymes(size: size),
          _SlideIndex(size: size),
          _SliderPersons(size: size)
        ]);
  }
}

class _SliderPersons extends StatelessWidget {
  const _SliderPersons({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final internalization = S.of(context);
    return Stack(alignment: Alignment.center, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FadeIn(
              duration: const Duration(milliseconds: 2000),
              child: Image.asset('assets/images/logo-purple.png', width: 110)),
          const SizedBox(height: 8),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.welcome.toUpperCase(),
                style: const TextStyle(
                    color: Color(0xff6C4F92),
                    fontSize: 45,
                    fontFamily: 'Dongle',
                    height: 0.8)),
          ),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.registerAs,
                style: const TextStyle(height: 1.0, color: Color(0xFFABA6B2))),
          ),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.xisfoPersons.toUpperCase(),
                style: const TextStyle(
                    fontSize: 16, height: 1.5, color: Color(0xFFABA6B2))),
          ),
          const SizedBox(height: 11),
          SizedBox(
            width: 340,
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                FadeIn(
                  duration: const Duration(milliseconds: 2000),
                  child: SizedBox(
                      width: size.width / 1.5,
                      child: FadeIn(
                          duration: const Duration(milliseconds: 2000),
                          child: Lottie.asset(
                            'assets/images/persons/person_3.json',
                            fit: BoxFit.fill,
                          ))),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          SizedBox(
            width: 180,
            child: FadeInUp(
              duration: const Duration(milliseconds: 1500),
              child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  disabledColor: const Color(0xffc7a314),
                  focusColor: const Color(0xffc7a314),
                  splashColor: const Color(0xffc7a314),
                  highlightColor: const Color(0xffEDCF53),
                  elevation: 0,
                  color: const Color(0xffEDCF53),
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(internalization.registerHere.toUpperCase(),
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff6C4F92),
                                  fontWeight: FontWeight.w700)),
                        ]),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, 'persons_register');
                  }),
            ),
          ),
        ],
      ),
      PopupNote(
          textTitle: internalization.textPersonNatureRegister),
    ]);
  }
}

class _SliderPymes extends StatelessWidget {
  const _SliderPymes({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final internalization = S.of(context);
    return Stack(alignment: Alignment.center, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FadeIn(
              duration: const Duration(milliseconds: 2000),
              child: Image.asset('assets/images/logo-purple.png', width: 110)),
          const SizedBox(height: 8),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.welcome.toUpperCase(),
                style: const TextStyle(
                    color: Color(0xff6C4F92),
                    fontSize: 45,
                    fontFamily: 'Dongle',
                    height: 0.8)),
          ),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.textRegister,
                style: const TextStyle(height: 1.0, color: Color(0xFFABA6B2))),
          ),
          FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Text(internalization.xisfoPymes.toUpperCase(),
                style: const TextStyle(
                    fontSize: 16, height: 1.4, color: Color(0xFFABA6B2))),
          ),
          const SizedBox(height: 2),
          SizedBox(
            width: 340,
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    width: size.width / 1.3,
                    child: FadeIn(
                        duration: const Duration(milliseconds: 2000),
                        child: Lottie.asset(
                          'assets/images/persons/person_2.json',
                          width: 300,
                          fit: BoxFit.fill,
                        ))),
                FadeInUp(
                  duration: const Duration(milliseconds: 1500),
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      disabledColor: const Color(0xffc7a314),
                      focusColor: const Color(0xffc7a314),
                      splashColor: const Color(0xffc7a314),
                      highlightColor: const Color(0xffEDCF53),
                      elevation: 0,
                      color: const Color(0xffEDCF53),
                      child: Container(
                        width: 150,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(internalization.registerHere.toUpperCase(),
                                  style: const TextStyle(
                                      fontSize: 12,
                                      color: Color(0xff6C4F92),
                                      fontWeight: FontWeight.w700)),
                            ]),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, 'pymes_register');
                      }),
                ),
              ],
            ),
          )
        ],
      ),
      PopupNote(
          textTitle: internalization.textBusinessRegister),
    ]);
  }
}

class _SlideIndex extends StatelessWidget {
  const _SlideIndex({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final internalization = S.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FadeIn(
            duration: const Duration(milliseconds: 3000),
            child: Image.asset('assets/images/logo-white.png', width: 110)),
        const SizedBox(height: 10),
        FadeIn(
          duration: const Duration(milliseconds: 3000),
          child: SizedBox(
            width: 300,
            child: Text(internalization.textSegmentsPersons.toUpperCase(),
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.white,
                    fontFamily: 'Dongle',
                    fontSize: 28,
                    height: 0.8)),
          ),
        ),
        SizedBox(
          width: 290,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  SizedBox(
                    height: 60.0,
                    width: 80.0,
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.navigate_before,
                            size: 90, color: Colors.white)),
                  ),
                  Expanded(
                      flex: 1,
                      child: SizedBox(
                        height: 319,
                      )),
                  SizedBox(
                    height: 60.0,
                    width: 80.0,
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.navigate_next,
                            size: 90, color: Colors.white)),
                  ),
                ],
              ),
              Positioned(
                  top: 55,
                  left: 2,
                  child: Text(internalization.pymes.toUpperCase(),
                      style: const TextStyle(
                          color: Color(0xffEDCF53), fontSize: 12))),
              Positioned(
                  top: 55,
                  right: 0,
                  child: Text(internalization.persons.toUpperCase(),
                      style: const TextStyle(
                          color: Color(0xffEDCF53), fontSize: 12))),
              Positioned(
                top: -10,
                left: -15,
                child: FadeIn(
                  duration: const Duration(milliseconds: 3000),
                  child: Lottie.asset(
                    'assets/images/persons/person_1.json',
                    fit: BoxFit.fitHeight,
                    height: 380,
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Column(
                  children: [
                    Text(internalization.textSlideMore,
                        style:
                        const TextStyle(color: Color(0xffEDCF53), height: 1.0)),
                  ],
                )
              ),
            ],
          ),
        ),
        Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            width: double.infinity,
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(Icons.arrow_back_ios_new,
                        size: 14, color: Color(0xffEDCF53)),
                    Text(internalization.back,
                        style: const TextStyle(
                            color: Color(0xffEDCF53),
                            fontWeight: FontWeight.w700,
                            fontSize: 14))
                  ],
                ))),
      ],
    );
  }
}
