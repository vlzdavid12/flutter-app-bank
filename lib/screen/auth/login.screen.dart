import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xisfo_app/bloc/auth/step/step_bloc.dart';
import 'package:xisfo_app/bloc/password/password_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';
import 'package:xisfo_app/widgets/widgets.dart';

import '../../services/local_notification.service.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    changeStatusDark();
    return Scaffold(
      backgroundColor: const Color(0xff6C4F92),
      body: _LoginGeneral(),
    );
  }
}

class _LoginGeneral extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final stepState = context.watch<StepBloc>().state.step;
    final GlobalKey<FormState> formKeyLogin = GlobalKey<FormState>();

    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
        Center(
        child: ElevatedButton(
        child: const Text('Show Notification'),
        onPressed: () {
          NotificationService()
              .showNotification(title: 'Xisfo', body: 'Tienes una nueva trasacción ☺️.');
        },
      )),

            Center(
              child: stepState ? ScreenAnimationOne() : ScreenAnimationTwo(),
            ),
            const SizedBox(
              height: 30,
            ),
            Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: formKeyLogin,
                child: stepState ? _LoginScreenOne() : _LoginScreenTwo())
          ],
        ),
      ),
    );
  }
}

class _LoginScreenOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);

    bool validPhone = false;
    return Column(
      children: [
        SizedBox(
          width: 290,
          child: TextFormField(
              cursorColor: Colors.white,
              autocorrect: false,
              style: const TextStyle(color: Colors.white),
              keyboardType: TextInputType.phone,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '000 000 0000',
                  labelText: internalization.numberMobil,
                  prefixIcon: Icons.phone_android,
                  colorInput: Colors.white),
              validator: (String? value) {
                if (value == null || value.trim().isEmpty) {
                  return internalization.enterCallNumber;
                }

                String pattern = "[0-9 ]{10}";
                RegExp regExp = RegExp(pattern);
                if (regExp.hasMatch(value)) {
                  validPhone = true;
                  return null;
                } else {
                  return internalization.mobilNotValidate;
                }
              }),
        ),
        const SizedBox(
          height: 25,
        ),
        MaterialButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            disabledColor: const Color(0xffc7a314),
            focusColor: const Color(0xffc7a314),
            splashColor: const Color(0xffc7a314),
            highlightColor: const Color(0xffEDCF53),
            elevation: 0,
            color: const Color(0xffEDCF53),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
              child: Text(internalization.getInto.toUpperCase(),
                  style: const TextStyle(
                      color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
            ),
            onPressed: () {
                if(validPhone){
                  context.read<StepBloc>().add(const SetChangeStepEvent(newStep: false));
                }
            }),
        const SizedBox(height: 30),
        Text(internalization.newFintechOur,
            style: const TextStyle(color: Colors.white)),
        MaterialButton(
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            elevation: 0,
            child: Text(
              internalization.createNewAccount,
              style: const TextStyle(color: Color(0xffEDCF53)),
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'register');
            })
      ],
    );
  }
}

class _LoginScreenTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final visiblePass = context.watch<PasswordBloc>().state.visiblePass;
    return Column(
      children: [
        SizedBox(
          width: 290,
          child: Stack(children: [
            TextFormField(
              enableSuggestions: false,
              maxLength: 4,
              cursorColor: Colors.white,
              autocorrect: false,
              style: const TextStyle(color: Colors.white),
              keyboardType: TextInputType.phone,
              obscureText: visiblePass ? true : false,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '****',
                  labelText: internalization.password,
                  prefixIcon: Icons.password,
                  colorInput: Colors.white),
              validator: (String? value) {
                if (value == null || value.length < 4) {
                  return internalization.enterPinPassword;
                }
                return null;
              },
            ),
            Positioned(
                top: 8,
                right: 8,
                child: visiblePass
                    ? IconButton(
                        onPressed: () {
                          context.read<PasswordBloc>().add(
                             const SetPasswordChangeEvent(newVisiblePass: false));
                        },
                        icon: Image.asset(
                            'assets/images/icons/eye_block.icon.png',
                            width: 35))
                    : IconButton(
                        onPressed: () {
                          context.read<PasswordBloc>().add(
                             const  SetPasswordChangeEvent(newVisiblePass: true));
                        },
                        icon: Image.asset('assets/images/icons/eye.icon.png',
                            width: 35)))
          ]),
        ),
        const SizedBox(
          height: 25,
        ),
        MaterialButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            disabledColor: const Color(0xffc7a314),
            focusColor: const Color(0xffc7a314),
            splashColor: const Color(0xffc7a314),
            highlightColor: const Color(0xffEDCF53),
            elevation: 0,
            color: const Color(0xffEDCF53),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
              child: Text(internalization.getInto.toUpperCase(),
                  style: const TextStyle(
                      color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
            ),
            onPressed: () {
              /* if (loginForm.formKey.currentState!.validate()) {
                Navigator.pushNamed(context, 'dashboard');
              }*/
              Navigator.pushNamed(context, 'dashboard');
            }),
        const SizedBox(height: 30),
 
        MaterialButton(
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            elevation: 0,
            child: Text(
              internalization.forgotPin,
              style: const TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'recovery');
            })
      ],
    );
  }
}
