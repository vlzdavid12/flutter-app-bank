import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 290,
              child: Text(
                internalization.welcomeDashboard("Cafer"),
                style: const TextStyle(
                    color: Color(0xff6C4F92),
                    fontSize: 42,
                    fontFamily: 'Dongle',
                    letterSpacing: -1,
                    fontWeight: FontWeight.w700,
                    height: 0.8),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            _TotalWallet(),
            const SizedBox(
              height: 40,
            ),
            Container(
              alignment:  Alignment.center,
              child: Wrap(
                spacing: 5.0, // gap between adjacent chips
                runSpacing: 15.0,
                children: [
                  _BtnService(
                    colorPrimary: const Color(0xFF524795),
                    colorSecondary: const Color(0xFF4C3DAB),
                    colorDisable: const Color(0xFF796EBD),
                    textLabel: internalization.ask,
                    image: Image.asset('assets/images/icons/request.icon.png'),
                    navigate: 'request',
                  ),

                  _BtnService(
                    colorPrimary: const Color(0xff6C4F92),
                    colorSecondary: const Color(0xff54387c),
                    colorDisable: const Color(0xff9377bd),
                    textLabel: internalization.sendTwo,
                    image: Image.asset('assets/images/icons/send.icon.png'),
                    navigate: 'send',
                  ),

                  _BtnService(
                    colorPrimary: const Color(0xff8E67E0),
                    colorSecondary: const Color(0xff6C4F92),
                    colorDisable: const Color(0xff9377bd),
                    textLabel: internalization.payment,
                    image: Image.asset('assets/images/icons/payment.icon.png'),
                    navigate: ''
                  ),

                  _BtnService(
                    colorPrimary: const Color(0xffCC9FEF),
                    colorSecondary: const Color(0xff7e589f),
                    colorDisable: const Color(0xffa285b9),
                    textLabel: internalization.withDraw,
                    image: Image.asset('assets/images/icons/withdraw.ico.png'),
                    navigate: 'withdraw',
                  ),

                ],
              ),
            ),
          const SizedBox(height: 100),
          ],
        ),
      ),
    ));
  }
}

class _BtnService extends StatelessWidget {

  final Color colorPrimary;
  final Color colorSecondary;
  final Color colorDisable;
  final String textLabel;
  final Image image;
  final String navigate;


  const _BtnService({required this.colorPrimary, required this.colorSecondary, required this.textLabel, required this.colorDisable, required this.image, required this.navigate});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 165,
      height: 60,
      child: MaterialButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50)),
          disabledColor: colorDisable,
          focusColor: colorPrimary,
          splashColor: colorSecondary,
          highlightColor: colorPrimary,
          elevation: 0,
          color: colorPrimary,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 40,
                  padding: const EdgeInsets.all(6),
                  decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(50),
                    color: Colors.white
                  ),
                  child: image,
                ),
                const SizedBox(width: 15),
                Expanded(child: Text(textLabel, style: const TextStyle(color: Colors.white))),
              ],
            ),
          onPressed: () {
            Navigator.pushNamed(context, navigate);
          }),
    );
  }
}

class _TotalWallet extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return Stack(
      children: [
        FadeInLeft(
          child: Container(
            width: 150,
            height: 180,
            decoration: const BoxDecoration(
                color: Color(0xFFEDCF53),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: null,
          ),
        ),
        FadeInDown(
          child: Container(
            margin: const EdgeInsets.only(top: 55, left: 10),
            width: double.infinity,
            padding:
                const EdgeInsets.only(top: 25, left: 10, right: 20, bottom: 10),
            decoration: BoxDecoration(
                color: const Color(0xff6C4F92),
                borderRadius: BorderRadius.circular(25)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                   Container(
                     padding: const EdgeInsets.all(20),
                      width: 70,
                      child: Image.asset('assets/images/icons/wallet.icon.png')),
                  SizedBox(
                    width: 180,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:  [
                        Text(internalization.available,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 35,
                                fontFamily: 'Dongle',
                                height: 0.9)),
                        SizedBox(
                          width: 180,
                          child: Flex(direction: Axis.horizontal, children: const [
                            Expanded(
                              flex: 1,
                              child:
                              Text("\$ 673.412,66",
                                  softWrap: false,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 41,
                                      fontFamily: 'Dongle',
                                      height: 0.8))
                            ),
                          ]),
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
        )
      ],
    );
  }
}
