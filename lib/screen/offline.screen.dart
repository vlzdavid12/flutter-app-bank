import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';

class OfflineScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    changeStatusDark();
    return Scaffold(
      backgroundColor: const Color(0xff6C4F92),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment:  MainAxisAlignment.center,
          children: [
            FadeInDown(duration: const Duration(milliseconds: 1500),  child: const Icon(Icons.wifi_off_rounded, color: Colors.white, size: 200,)),
            Center( child: Text(internalization.notRed, style: const TextStyle(color: Colors.white, fontSize: 20)))
          ],
        ),
      ),
    );
  }
}
