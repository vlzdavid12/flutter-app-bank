import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:xisfo_app/ui/contract_legal_natural_person.dart';

import '../../../../generated/l10n.dart';
import '../../../../widgets/form/checkbox_input.widgets.dart';
import '../../../../widgets/btnGroup.widget.dart';
import '../../../../widgets/header.widget.dart';

class StepFive extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepFive(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            HeaderFormWidget(
              stepNumber: '05',
              title: internalization.endStatementFound,
              subTitle: internalization.welcomeXisfo,
            ),
            Column(
              children: [
                SizedBox(
                  height: size.height / 2.4,
                  child: SingleChildScrollView(
                      child: Html(data: TermContractLegal.contractDeclarationFound())),
                ),
                CheckBoxInput(lisTitles: [
                  internalization.declarationOrigenFound
                ]),
                CheckBoxInput(lisTitles:[
                  internalization.leyPoliData
                ]),
              ],
            ),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization:  internalization,),
          ],
        ),
      ),
    );
  }
}