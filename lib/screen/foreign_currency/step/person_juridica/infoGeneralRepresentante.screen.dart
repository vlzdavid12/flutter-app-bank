import 'package:flutter/material.dart';

import '../../../../generated/l10n.dart';
import '../../../../ui/input_decorations.ui.dart';
import '../../../../widgets/form/date_picker_input.widgets.dart';
import '../../../../widgets/form/dropdown_input.widgets.dart';
import '../../../../widgets/form/radio_input.widgets.dart';
import '../../../../widgets/btnGroup.widget.dart';
import '../../../../widgets/header.widget.dart';

class StepTwo extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  TextEditingController dateInputDate = TextEditingController();
  TextEditingController dropDownSelect = TextEditingController();


  StepTwo(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(0),
        color: Colors.transparent,
        child: Column(
          children: [
            HeaderFormWidget(
              stepNumber: '02',
              title: internalization.infoGeneral,
              subTitle: internalization.representLegal,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '',
                  labelText: 'Nombres',
                  prefixIcon: Icons.account_circle,
                  colorInput: Colors.grey),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '',
                  labelText: 'Apellidos',
                  prefixIcon: Icons.account_circle,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            DopDownInput(list: [''], labelDocument: 'Tipo de documento.', dropDownSelect: dropDownSelect, prefixIcon: Icons.file_copy_rounded,),
            DatePickerInput(textLabel: 'Fecha de expedición', dataInputDate: dateInputDate),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '0123456789',
                  labelText: 'No. de identificación',
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey),
            ),
            DopDownInput(list: [''], labelDocument: 'Lugar de expedición', dropDownSelect: dropDownSelect, prefixIcon: Icons.location_on,),
            Row(
              children: [
                const Expanded(
                  flex: 1,
                  child: Text('TIENE VINCULOS COMERCIALES CON EL ESTADO',
                      style: TextStyle(fontSize: 10)),
                ),
                Expanded(
                    flex: 1,
                    child: RadioInput(
                      listRadio: ['Si', 'No'],
                    )),
              ],
            ),
            const SizedBox(height: 25),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,),
            const SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}
