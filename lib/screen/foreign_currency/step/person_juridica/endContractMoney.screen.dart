import "package:flutter/material.dart";
import "package:flutter_html/flutter_html.dart";
import "package:metamap_plugin_flutter/metamap_plugin_flutter.dart";

import '../../../../generated/l10n.dart';
import '../../../../ui/contract_legal_natural_person.dart';
import '../../../../widgets/form/checkbox_input.widgets.dart';
import '../../../../widgets/header.widget.dart';

class StepSix extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepSix(this.pageController, this.pageChanged, this.internalization);

  void showMetaMapSignature() {
    final metaData = {"fixedLanguage": "es"};
    MetaMapFlutter.showMetaMapFlow(
        "6356ae14a74bbc001cd5bf62", "63570aca792e51001c0db382", metaData);
    MetaMapFlutter.resultCompleter.future.then((result) => print("resp:  $result"));
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            HeaderFormWidget(
              stepNumber: '06',
              title: internalization.endMoneyInternational,
              subTitle: internalization.welcomeXisfo,
            ),
            Column(
              children: [
                SizedBox(
                  height: size.height / 2.8,
                  child: SingleChildScrollView(
                      child: Html(data: TermContractLegal.contractLegalNaturePerson())),
                ),
                CheckBoxInput(lisTitles:  [
                  'Al hacer click aquí acepto el contrato de moneda extrangera.'
                ]),
              ],
            ),
            MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                disabledColor: const Color(0xffc7a314),
                focusColor: const Color(0xffc7a314),
                splashColor: const Color(0xffc7a314),
                highlightColor: const Color(0xffEDCF53),
                elevation: 0,
                color: const Color(0xffEDCF53),
                onPressed:showMetaMapSignature,
                child: Container(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                  child: Text(internalization.signContract.toUpperCase(),
                      style: const TextStyle(
                          color: Color(0xff6C4F92),
                          fontWeight: FontWeight.w700)),
                )),
          ],
        ),
      ),
    );
  }
}