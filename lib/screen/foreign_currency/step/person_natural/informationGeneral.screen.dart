import "package:flutter/material.dart";
import 'package:xisfo_app/widgets/btnGroup.widget.dart';

import "../../../../generated/l10n.dart";
import "../../../../ui/input_decorations.ui.dart";
import "../../../../widgets/form/date_picker_input.widgets.dart";
import "../../../../widgets/form/dropdown_input.widgets.dart";
import "../../../../widgets/header.widget.dart";

class StepOne extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  TextEditingController dateInputDate = TextEditingController();
  TextEditingController dropDownSelect = TextEditingController();

  StepOne(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(0),
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            HeaderFormWidget(
              stepNumber: '01',
              title: internalization.infoGeneral,
              subTitle: internalization.startDetailAccount,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '',
                  labelText: 'Nombres',
                  prefixIcon: Icons.account_circle,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '',
                  labelText: 'Apellidos',
                  prefixIcon: Icons.account_circle,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            DopDownInput(list: [''], labelDocument: "Tipo de documento", dropDownSelect: dropDownSelect, prefixIcon: Icons.description,),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '0123456789',
                  labelText: 'No. de identicación',
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)
              ),
            ),
            DopDownInput(list: [''], labelDocument: 'Lugar de expedición', dropDownSelect: dropDownSelect, prefixIcon: Icons.location_on,),
            DatePickerInput(
              textLabel: 'Fecha de nacimiento',
              dataInputDate: dateInputDate,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'Cra. 45 No. 90 - 89',
                  labelText: 'Dirección de domicilio',
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey),
            ),
            DopDownInput(list: [''], labelDocument: 'País', dropDownSelect: dropDownSelect, prefixIcon: Icons.public,),
            DopDownInput(list: [''], labelDocument: 'Ciudad', dropDownSelect: dropDownSelect, prefixIcon: Icons.satellite_alt,),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.phone,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '000 000 0000',
                  labelText: 'Número Celular',
                  prefixIcon: Icons.phone,
                  colorInput: Colors.grey),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'micorreo@xisfo.co',
                  labelText: 'Correo electrónico',
                  prefixIcon: Icons.email,
                  colorInput: Colors.grey),
            ),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,),
          ],
        ),
      ),
    );
  }
}
