import 'package:flutter/material.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';
import 'package:xisfo_app/screen/foreign_currency/step/person_natural/endExportMoneyContract.screen.dart';
import 'package:xisfo_app/screen/foreign_currency/step/person_natural/endRegisterFound.screen.dart';
import 'package:xisfo_app/screen/foreign_currency/step/person_natural/informacionGenerlaRepresentante.screen.dart';
import 'package:xisfo_app/screen/foreign_currency/step/person_natural/informationGeneral.screen.dart';

class PersonNaturalScreen extends StatefulWidget {
  @override
  State<PersonNaturalScreen> createState() => _PersonNaturalScreenState();
}

class _PersonNaturalScreenState extends State<PersonNaturalScreen> {
  PageController pageController = PageController();
  late int pageChanged = 0;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    changeStatusLight();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final internalization = S.of(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        elevation: 0,
        title:Container(
            padding: const EdgeInsets.only(top: 5),
            height: 48, child: Image.asset('assets/images/logo-purple.png')),
      ),
      body: Scaffold(
          body: SingleChildScrollView(
              child: Center(
                  child: Container(
                      padding: const EdgeInsets.all(15),
                      width: 550,
                      child: Column(
                        children: [
                        
                          SizedBox(
                              width: double.infinity,
                              height: size.height / 1.3,
                              child: PageView(
                                  physics:
                                      const NeverScrollableScrollPhysics(),
                                  pageSnapping: true,
                                  controller: pageController,
                                  onPageChanged: (index) {
                                    setState(() {
                                      pageChanged = index;
                                    });
                                  },
                                  children: [
                                    StepOne(pageController, pageChanged, internalization),
                                    StepTwo(pageController, pageChanged, internalization),
                                    StepThree(pageController, pageChanged, internalization),
                                    StepFour(pageController, pageChanged, internalization)
                                  ])),
                          _Dots(4, pageChanged)
                        ],
                      ))))),
    );
  }
}

// End Step

class _Dots extends StatelessWidget {
  final int totalSlides;
  final int pageChanged;

  const _Dots(this.totalSlides, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: SizedBox(
        width: double.infinity,
        height: 25,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(totalSlides, (i) => _Dot(i, pageChanged)),
        ),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;
  final int pageChanged;
  late Color color;
  late double widthBox;

  _Dot(this.index, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    if (pageChanged >= index - 0.5 && pageChanged < index + 0.5) {
      color = const Color(0xff6C4F92);
      widthBox = 8;
    } else {
      color = Colors.grey;
      widthBox = 5;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: 40,
      height: widthBox,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          color: color, //Colors.grey
          borderRadius: BorderRadius.circular(10)),
    );
  }
}

