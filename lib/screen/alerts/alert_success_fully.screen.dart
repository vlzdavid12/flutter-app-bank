import 'package:animate_do/animate_do.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

class AlertSuccessFullyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final dateNow = DateTime.now();
    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Swing(
            duration: const Duration(milliseconds: 1000),
            child: Image.asset(
              'assets/images/icons/check.png',
              width: 130,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            internalization.solicitudExitosa,
            style: const TextStyle(fontSize: 24),
          ),
          const SizedBox(height: 20),
          Stack(
            children:[
              DottedBorder(
                padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
                borderType: BorderType.RRect,
                radius: const Radius.circular(10),
                dashPattern: const [4, 4],
                strokeWidth: 2,
                color: const Color(0xff6C4F92),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text("\$ 3.016.900",
                        style: TextStyle(
                            fontSize: 64,
                            fontFamily: 'Dongle',
                            fontWeight: FontWeight.w700,
                            color: Color(0xff6C4F92),
                            height: 1.0)),
                    Text(internalization.paymentHave(32134875689),
                        style: const TextStyle(fontSize: 16)),
                    const SizedBox(height: 20),
                    Table(
                      border: const TableBorder(verticalInside: BorderSide.none),
                      columnWidths: const <int, TableColumnWidth>{
                        0: FixedColumnWidth(140),
                        1: FixedColumnWidth(140),
                      },
                      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                      children: <TableRow>[
                        TableRow(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: const Text("Mensaje"),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: const Text("texto de solicitud"),
                            ),
                          ],
                        ),
                        TableRow(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: const Text("Código de ref."),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: const Text("F-908987"),
                            ),
                          ],
                        ),
                        TableRow(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: const Text("Horá."),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: Text(DateFormat('mm:ss').format(dateNow)),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                  ],
                )),
              Positioned(
                top: 230,
                right: 10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Material(
                      type: MaterialType.transparency,
                      child: Ink(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: const Color(0xff6C4F92), width: 2),
                            color: const Color(0xff6C4F92),
                            borderRadius: BorderRadius.circular(50.0)),
                        //<-- SEE HERE
                        child: InkWell(
                          borderRadius: BorderRadius.circular(100.0),
                          onTap: () {
                            Navigator.pushNamed(context, 'preview_screen_invoice');
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Icon(
                              Icons.download,
                              size: 35.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),

                  ],
                ),
              ),
              const SizedBox(height: 290),
          ]),
          const SizedBox(height: 100),
        ],
      ),
    ));
  }
}
