import 'package:flutter/material.dart';
import 'package:xisfo_app/generated/l10n.dart';

class SuccessFullyRegisterScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final internalization =  S.of(context);
    return Scaffold(
      backgroundColor: const Color(0xff6C4F92),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/images/icons/success_register.png', width: 160.0,),
              Center(child: Text(internalization.textSuccessFully, style: const TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.center)),
              const SizedBox(height: 30),
              MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  disabledColor: const Color(0xffc7a314),
                  focusColor: const Color(0xffc7a314),
                  splashColor: const Color(0xffc7a314),
                  highlightColor: const Color(0xffEDCF53),
                  elevation: 0,
                  color: const Color(0xffEDCF53),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 15),
                    child: Text(internalization.back,
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700)),
                  ),
                  onPressed: () {
                      Navigator.pushNamed(context, 'login');
                  }),
            ],
          )
        ),
      ),
    );
  }
}
