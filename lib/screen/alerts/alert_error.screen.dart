import 'package:animate_do/animate_do.dart';
import 'package:dotted_border/dotted_border.dart';
import "package:flutter/material.dart";
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

class AlertErrorScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return  LayoutApp(
        children: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Swing(
                duration: const Duration(milliseconds: 1000),
                child: Image.asset(
                  'assets/images/icons/close_error.png',
                  width: 130,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                internalization.processRejected,
                style: const TextStyle(fontSize: 24),
              ),
              const SizedBox(height: 20),
              DottedBorder(
                  padding:
                  const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
                  borderType: BorderType.RRect,
                  radius: const Radius.circular(10),
                  dashPattern: const [4, 4],
                  strokeWidth: 2,
                  color: const Color(0xff6C4F92),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(internalization.processRejectedText, textAlign: TextAlign.center, style: const TextStyle(fontSize: 16))
                    ])
              )
            ],
          ),
        )
    );
  }
}
