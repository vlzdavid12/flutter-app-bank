import 'package:flutter/material.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';
import 'package:xisfo_app/widgets/form/dropdown_input.widgets.dart';
import 'package:xisfo_app/widgets/form/upload_input.widgets.dart';

class UploadSupport extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final international = S.of(context);
    TextEditingController dropDownSelect = TextEditingController();

    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(15),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(international.legalPaymentSupport("Cafer"),
                style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Dongle',
                    fontSize: 42,
                    height: 0.8)),
            const SizedBox(height: 20),
            Text(international.selectPlatform),
            DopDownInput(
              list: [''],
              labelDocument: "Tipo Segmento",
              dropDownSelect: dropDownSelect,
              prefixIcon: Icons.rotate_90_degrees_ccw,
            ),
            const SizedBox(height: 10),
            DopDownInput(
              list: ['Lolita096'],
              labelDocument: 'Master',
              dropDownSelect: dropDownSelect,
              prefixIcon: Icons.settings_suggest,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '3103409890',
                  labelText: international.walletBonus,
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)),
            ),
            const SizedBox(height: 10),
            Text(international.paymentApply,
                style: const TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w700,
                    fontSize: 24)),
            const SizedBox(height: 10),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '#FAC-0001234',
                  labelText: international.numberFacture,
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)),
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: '#FAC-0001234',
                  labelText: international.mountDollar,
                  prefixIcon: Icons.numbers,
                  colorInput: Colors.grey,
                  colorError: const Color(0xFFC21839)),
            ),
            const SizedBox(height: 10),
            Text(international.addArchive,
                style: const TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w700,
                    fontSize: 24)),
            const SizedBox(height: 10),
            UploadInput(),
            const SizedBox(height: 10),
            Center(
              child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  disabledColor: const Color(0xffc7a314),
                  focusColor: const Color(0xffc7a314),
                  splashColor: const Color(0xffc7a314),
                  highlightColor: const Color(0xffEDCF53),
                  elevation: 0,
                  color: const Color(0xffEDCF53),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 80, vertical: 15),
                    child: Text(international.uploadSupport,
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700)),
                  ),
                  onPressed: () {}),
            ),
            const SizedBox(height: 120),
          ],
        ),
      ),
    ));
  }
}
