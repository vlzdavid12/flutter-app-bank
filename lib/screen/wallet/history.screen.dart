import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/widgets/modal.widgets.dart';
import 'package:xisfo_app/widgets/widgets.dart';

class HistoryScreen extends StatelessWidget {
  Random random = Random();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final internalization =  S.of(context);
    final now = DateTime.now();

    return LayoutApp(
      children: Column(
        children: [
          Align(
            alignment: Alignment.center,
            child: MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                disabledColor: const Color(0xffc7a314),
                focusColor: const Color(0xffc7a314),
                splashColor: const Color(0xffc7a314),
                highlightColor: const Color(0xffEDCF53),
                elevation: 0,
                color: const Color(0xffEDCF53),
                child: Container(
                  width: size.width / 1.2,
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:  [
                      const Icon(Icons.search, color: Color(0xff6C4F92)),
                      const SizedBox(width: 5),
                      Text(
                        internalization.filter.toUpperCase(),
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700),
                      )
                    ],
                  ),
                ),
                onPressed: () {
                  _showFilter(context);
                }),
          ),
          const SizedBox(
            height: 5,
          ),
          SizedBox(
            width: double.infinity,
            height: size.height / 1.5,
            child: ListApp(
                items: List<ListItem>.generate(
              1000,
              (i) => i % 6 == 0
                  ? HeadingItem(DateFormat('yyyy-MM-dd').format(now))
                  : BodyItem(
                      'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                      DateFormat('yyyy-MM-dd hh:mm:ss').format(now),
                      '30.300.000.00',
                      Image.asset(
                        './assets/images/list_icons/icon${random.nextInt(4) + 1}.png',
                        width: 40,
                      )),
            )),
          )
        ],
      ),
    );
  }
}

class ListApp extends StatelessWidget {
  final List<ListItem> items;

  const ListApp({Key? key, required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: items.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final item = items[index];

        return ListTile(
          title: item.bodyItem(context),
        );
      },
    );
  }
}

class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);

  @override
  Widget bodyItem(BuildContext context) {
    return Text(
      heading,
      style: const TextStyle(
          color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20),
    );
  }
}

class BodyItem implements ListItem {
  final String title;
  final String fecha;
  final String total;
  final Widget icon;

  BodyItem(this.title, this.fecha, this.total, this.icon);

  @override
  Widget bodyItem(BuildContext context) => Row(
        children: [
          icon,
          const SizedBox(width: 15),
         Expanded(
              child: GestureDetector(
                child:  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flex(direction: Axis.horizontal, children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        title,
                        style: const TextStyle(fontSize: 12, height: 1.5),
                        softWrap: false,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis, // new
                      ),
                    ),
                  ]),
                  const SizedBox(height: 4),
                  Text(fecha, style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w700, height: 1.6)),
                ],
              ),
                onTap: () {
                 _showAddModal(context);
                }
            ),
          ),
          const SizedBox(width: 3),
          Text('\$ $total',
              style:
                  const TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
        ],
      );
}

_showFilter(context) {
  TextEditingController dropDownSelect = TextEditingController();
  final international = S.of(context);
  final childContent = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(international.search,
          style: const TextStyle(
              color: Colors.black45, fontSize: 22, fontWeight: FontWeight.w700),
          textAlign: TextAlign.left),
      const SizedBox(height: 5),
      Container(
          margin: const EdgeInsets.symmetric(vertical: 2),
          width: double.infinity,
          child: DopDownInput(list: [''], labelDocument: 'Acción', dropDownSelect: dropDownSelect, prefixIcon: Icons.transfer_within_a_station ,)),
      Container(
          margin: const EdgeInsets.symmetric(vertical: 2),
          width: double.infinity,
          child: DopDownInput(list: [''], labelDocument: 'Desde', dropDownSelect: dropDownSelect, prefixIcon: Icons.my_location,)),
      Container(
          margin: const EdgeInsets.symmetric(vertical: 2),
          width: double.infinity,
          child: DopDownInput(list: [''], labelDocument: 'Hasta', dropDownSelect:  dropDownSelect, prefixIcon: Icons.arrow_forward,)),
      MaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          disabledColor: const Color(0xffa18bc7),
          focusColor: const Color(0xff4d2d79),
          splashColor: const Color(0xff53298a),
          highlightColor: const Color(0xff6C4F92),
          elevation: 0,
          color: const Color(0xff6C4F92),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Text(international.filter.toUpperCase(),
              style:
                  const TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
          ),
          onPressed: () {}),
    ],
  );
  dialogModalBuilder(context, childContent);
}

abstract class ListItem {
  /// The title line to show in a list item.
  Widget bodyItem(BuildContext context);
}

Future<void> _showAddModal(BuildContext context) async {
  Random random = Random();
  final now = DateTime.now();
  final internalization = S.of(context);

  final childContent =  Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
      Image.asset(
      './assets/images/list_icons/icon${random.nextInt(4) + 1}.png',
    width: 80,
      ),
          const SizedBox(height: 20),
          Text(internalization.withDraw, style: const TextStyle(fontSize: 18, height: 2.0)),
          Text('\$ 30.300.0000.00', style: const TextStyle(fontSize: 24, color: Color(0xff6C4F92), fontWeight: FontWeight.w700)),
          const SizedBox(height: 10),
          Text('Lorem Ipsum is simply dummy text of the printing and typesetting industry', style: const TextStyle(fontSize: 14)),
          const SizedBox(height: 10),
          Text(  DateFormat('yyyy-MM-dd hh:mm:ss').format(now)),
          const SizedBox(height: 20),
          Material(
            type: MaterialType.transparency,
            child: Ink(
              decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color(0xff6C4F92), width: 2),
                  color: const Color(0xff6C4F92),
                  borderRadius: BorderRadius.circular(50.0)),
              //<-- SEE HERE
              child: InkWell(
                borderRadius: BorderRadius.circular(100.0),
                onTap: () {
                  Navigator.pushNamed(context, 'preview_screen_invoice');
                },
                child: const Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Icon(
                    Icons.download,
                    size: 35.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ) );
  await dialogModalBuilder(context, childContent);
}
