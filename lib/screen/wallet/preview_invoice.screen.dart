import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

class PreviewInvoiceScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PdfPreview(
      build: (format) => _generatePdf(format, "Pdf"),
    );
  }
}

String svgRaw =
    '''<?xml version="1.0" encoding="UTF-8"?><svg id="a" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 812.1 373"><text transform="translate(379.2 356.3)" style="fill:#ecce53; font-family:RimouskiSb-Regular, Rimouski; font-size:65px; letter-spacing:.1em;"><tspan x="0" y="0">Fintech</tspan></text><g><g><path d="M124.1,78.4c5.4,0,10.1,1.9,14,5.8,3.9,3.9,5.8,8.7,5.8,14.4s-.3,4.4-.9,6.7c-.6,2.4-1.7,4.7-3.4,7.1l-40.8,60.6,47.5,71.3c1.7,2.4,2.8,4.6,3.4,6.7,.6,2.1,.9,4.5,.9,7.1,0,5.7-1.9,10.4-5.8,14.2-3.9,3.8-8.6,5.7-14,5.7s-8-.8-10.6-2.5c-2.6-1.6-5.2-4.5-7.8-8.5l-37.2-59.9-36.5,59.9c-2.6,4-5.5,6.9-8.7,8.5-3.2,1.7-6.6,2.5-10.1,2.5-5.2,0-9.8-1.9-13.8-5.7-4-3.8-6-8.5-6-14.2s1.3-8.5,3.9-12.8l48.6-72.7L11.4,112.4c-1.7-2.4-2.8-4.7-3.4-7.1-.6-2.4-.9-4.6-.9-6.7,0-5.7,1.9-10.5,5.8-14.4,3.9-3.9,8.6-5.8,14-5.8s5.8,.8,9,2.3c3.2,1.5,6,4.1,8.3,7.6l31.2,50.4,31.2-50c2.4-3.5,5.1-6.1,8.2-7.8,3.1-1.7,6.1-2.5,9.2-2.5" style="fill:#6d4f95;"/><path d="M211.7,257.5c0,6.4-1.8,11.4-5.5,15.1-3.7,3.7-8.7,5.5-15.1,5.5s-10.5-1.8-14.4-5.5c-3.9-3.7-5.8-8.7-5.8-15.1V105.7c0-5.9,1.9-10.8,5.8-14.5,3.9-3.8,8.7-5.7,14.4-5.7s11.4,1.9,15.1,5.7c3.7,3.8,5.5,8.6,5.5,14.5v151.8Z" style="fill:#6d4f95;"/><path d="M301.4,153.2c9,3.5,16.6,7.1,22.9,10.6,6.3,3.5,11.5,7.3,15.8,11.3,5.4,5.4,9.3,11.6,11.7,18.6,2.4,7,3.5,15.5,3.5,25.7s-1.4,14.9-4.3,22c-2.8,7.1-6.9,13.4-12.2,19-5.3,5.6-11.7,10.1-19.1,13.5-7.5,3.4-15.9,5.1-25.4,5.1s-16.4-1.4-24.3-4.1c-7.9-2.7-14.6-6.2-20-10.5-3.3-2.6-6.1-5.5-8.3-8.9-2.2-3.3-3.4-7.1-3.4-11.4s1.7-9.2,5.1-13.3c3.4-4.1,7.6-6.2,12.6-6.2s8.7,1.4,13.5,4.3c3.6,2.1,7.3,3.9,11.3,5.3,4,1.4,8.5,2.1,13.5,2.1,7.1,0,12.3-1.8,15.6-5.5,3.3-3.7,5-8.2,5-13.6s-.9-9.2-2.7-11.9c-1.8-2.7-4-5-6.6-6.9-5.4-4-11.4-7.1-17.9-9.2-6.5-2.1-12.8-5-19-8.5-8.3-4.7-14.8-10.8-19.5-18.1-4.7-7.3-7.1-16.8-7.1-28.4s1.4-16.3,4.3-23.2c2.8-7,6.8-12.9,11.9-17.9,5.1-5,11.1-8.8,17.9-11.5,6.9-2.7,14.4-4.1,22.7-4.1s16.4,1.5,24.5,4.6c8,3.1,14.2,7.7,18.4,13.8,2.8,4,4.3,9.1,4.3,15.2s-1.5,9.3-4.4,12.9c-3,3.7-7.5,5.5-13.6,5.5s-4.9-.5-7.6-1.6c-2.7-1.1-4.8-2.5-6.2-4.4-4-5.2-9.8-7.8-17.4-7.8s-7.1,1.4-10.1,4.3c-3,2.8-4.4,7-4.4,12.4s1.1,8.8,3.4,11.5c2.2,2.7,7.5,5.7,15.8,9" style="fill:#6d4f95;"/><path d="M422.3,120.9v136.5c0,6.4-1.8,11.4-5.5,15.1-3.7,3.7-8.7,5.5-15.1,5.5s-10.5-1.8-14.4-5.5c-3.9-3.7-5.8-8.7-5.8-15.1V120.9c-5.4-.7-9.6-2.8-12.6-6.4-3-3.5-4.4-8.3-4.4-14.2s1.5-9.6,4.4-13.3c3-3.7,7.1-5.9,12.6-6.6v-18.1c0-10.9,1.5-20.3,4.4-28.2,3-7.9,7.1-14.4,12.4-19.5,5.3-5.1,11.5-8.8,18.6-11.2,7.1-2.4,14.8-3.5,23-3.5s13.7,1.7,17.7,5.1c4,3.4,6,8,6,13.7s-1.5,11.1-4.4,14.7c-3,3.7-7.2,5.7-12.6,6.2-3.3,.2-6.4,.7-9.2,1.4-2.8,.7-5.4,1.9-7.8,3.6-2.4,1.7-4.2,3.9-5.5,6.7-1.3,2.8-1.9,6.6-1.9,11.4v17.4h12.8c6.1,0,11,1.9,14.7,5.8,3.7,3.9,5.5,8.7,5.5,14.4s-1.8,11.4-5.5,15.1c-3.7,3.7-8.6,5.5-14.7,5.5h-12.8Z" style="fill:#6d4f95;"/><path d="M560.6,73.8c14.4,0,28,2.8,40.6,8.3,12.6,5.6,23.7,13.1,33.2,22.5,9.5,9.5,16.9,20.5,22.3,33.2,5.4,12.6,8.2,26.2,8.2,40.6s-2.7,28-8.2,40.6c-5.4,12.6-12.9,23.6-22.3,33-9.5,9.3-20.5,16.7-33.2,22.2-12.6,5.4-26.2,8.2-40.6,8.2s-28-2.7-40.6-8.2c-12.6-5.4-23.7-12.8-33.2-22.2-9.5-9.3-16.9-20.3-22.3-33-5.4-12.6-8.2-26.2-8.2-40.6s2.7-28,8.2-40.6c5.4-12.6,12.9-23.7,22.3-33.2,9.5-9.5,20.5-17,33.2-22.5,12.6-5.6,26.2-8.3,40.6-8.3m0,40.8c-9,0-17.3,1.7-25,5-7.7,3.3-14.4,7.9-20,13.6-5.7,5.8-10.2,12.6-13.5,20.4-3.3,7.8-5,16.1-5,24.8s1.7,17,5,24.6c3.3,7.7,7.8,14.4,13.5,20,5.7,5.7,12.4,10.2,20,13.5,7.7,3.3,16,5,25,5s17-1.6,24.7-5c7.7-3.3,14.4-7.8,20.2-13.5,5.8-5.7,10.3-12.4,13.7-20,3.3-7.7,5-15.9,5-24.6s-1.7-17-5-24.8c-3.3-7.8-7.9-14.6-13.7-20.4-5.8-5.8-12.5-10.3-20.2-13.6-7.7-3.3-15.9-5-24.7-5" style="fill:#6d4f95;"/></g><circle cx="191.3" cy="45.5" r="24.7" style="fill:#6d4f95;"/></g><g><path d="M754.1,63c3.3-3.3,13.5-13.4,33.2-13.4s24.8-11.1,24.8-24.8-2.8-13-7.3-17.5C800.3,2.8,794.1,0,787.3,0,773.6,0,762.5,11.1,762.5,24.8c0,19.6-10.1,29.9-13.4,33.2-3.3,3.3-13.5,13.4-33.2,13.4s-24.8,11.1-24.8,24.8,2.8,13,7.3,17.5c4.5,4.5,10.7,7.3,17.5,7.3,13.7,0,24.8-11.1,24.8-24.8,0-19.6,10.1-29.9,13.4-33.2" style="fill:#efd14e;"/><path d="M805.1,114c-9.2,9.2-24.2,9.2-33.5,0-9.2-9.2-13-32.9-6.2-39.7,6.8-6.8,30.5-3,39.7,6.2s9.2,24.2,0,33.5Z" style="fill:#6d5296;"/><path d="M698.1,6.9c9.2-9.2,24.2-9.2,33.5,0s13,32.9,6.2,39.7c-6.8,6.8-30.5,3-39.7-6.2-9.2-9.2-9.2-24.2,0-33.5Z" style="fill:#6d5296;"/></g><text transform="translate(686.2 323.4)" style="fill:#6d4f95; font-family:MyriadPro-Regular, Myriad Pro; font-size:54px;"><tspan x="0" y="0">®</tspan></text></svg>''';



Future<Uint8List> _generatePdf(PdfPageFormat format, String title) async {
  final pdf = pw.Document(version: PdfVersion.pdf_1_5, compress: true);
  final svgImage = pw.SvgImage(svg: svgRaw);

  pdf.addPage(
    pw.Page(
      pageTheme: pw.PageTheme(
          pageFormat: format.copyWith(
            marginBottom: 90,
            marginLeft: 90,
            marginRight: 90,
            marginTop: 90,
          ),
          orientation: pw.PageOrientation.portrait),
      build: (context) {
        return pw.Column(
            crossAxisAlignment: pw.CrossAxisAlignment.center,
            mainAxisAlignment: pw.MainAxisAlignment.center,
            children: [
              pw.Container(
                margin: const pw.EdgeInsets.symmetric(vertical: 20),
                width: 220,
                child: pw.Center(
                  child: svgImage,
                ),
              ),
              pw.Expanded(
                  child: pw.Column(children: [
                pw.Center(
                  child: pw.Text('Pagaste',
                      style: const pw.TextStyle(
                        fontSize: 28,
                      )),
                ),
                pw.SizedBox(height: 10),
                pw.Text('\$3.016.900',
                    style: const pw.TextStyle(
                      fontSize: 40,
                    )),
                pw.SizedBox(height: 10),
                pw.Text('Empresa de Energía',
                    style: const pw.TextStyle(
                      fontSize: 28,
                    )),
                pw.SizedBox(height: 30),
                pw.Divider(
                    borderStyle:
                        const pw.BorderStyle(pattern: <int>[5, 7], phase: 4)),
                pw.SizedBox(height: 30),
                pw.Table(
                  border:
                      const pw.TableBorder(verticalInside: pw.BorderSide.none),
                  columnWidths: const <int, pw.TableColumnWidth>{
                    0: pw.FixedColumnWidth(140),
                    1: pw.FixedColumnWidth(140),
                  },
                  defaultVerticalAlignment:
                      pw.TableCellVerticalAlignment.middle,
                  children: <pw.TableRow>[
                    pw.TableRow(
                      children: [
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("Mensaje",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("texto de solicitud",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                      ],
                    ),
                    pw.TableRow(
                      children: [
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("Código de ref.",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("F-908987",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                      ],
                    ),
                    pw.TableRow(
                      children: [
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("Horá.",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          child: pw.Text("12/02/2023",
                              style: const pw.TextStyle(fontSize: 18)),
                        ),
                      ],
                    ),
                  ],
                ),
              ])),
              pw.Divider(
                  borderStyle:
                      const pw.BorderStyle(pattern: <int>[5, 7], phase: 4)),
              pw.Text('www.xisfo.co', style: const pw.TextStyle(fontSize: 18)),
            ]);
      },
    ),
  );

  return pdf.save();
}
