import 'package:flutter/material.dart';

class BtnMenu{
  String title;
  Widget icon;
  Color color;
  String actionLink;

  BtnMenu({required this.title, required this.icon, this.color = Colors.purple, required this.actionLink});
}
