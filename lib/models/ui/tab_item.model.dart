import "package:flutter/material.dart";

class TabItem{
  String title;
  Widget icon;
  int action;

  TabItem({required this.title, required this.icon, required this.action});
}